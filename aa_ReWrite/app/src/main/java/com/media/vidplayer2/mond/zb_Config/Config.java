package com.media.vidplayer2.mond.zb_Config;

import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.api.services.drive.model.File;
import com.media.vidplayer2.mond.R;
import com.media.vidplayer2.mond.aa_MainActivity.MainActivity_Start;
import com.media.vidplayer2.mond.zb_a_chat.FireStoreChat.AbstractChat;
import com.media.vidplayer2.mond.zb_a_chat.FireStoreChat.Chat;
import com.media.vidplayer2.mond.zb_a_chat.FireStoreChat.ChatHolder;
import com.media.vidplayer2.mond.zc_VideoFileParser.VideoFileParser;
import com.media.vidplayer2.mond.zz_TEST.TestCase;
import com.media.vidplayer2.mond.zz_TEST.aa_Recycler.BooVariable;
import com.media.vidplayer2.mond.zz_TEST.aa_Recycler.DirectoryFav;
import com.tapadoo.alerter.Alerter;


import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Semaphore;

import static android.os.Environment.getExternalStorageDirectory;
import static com.firebase.ui.auth.AuthUI.getApplicationContext;
/*

https://www.loginworks.com/blogs/get-crash-logs-android-applications/
 */

// TIME_DELAY_INSIDE_THREAD_START
// TIME_DELAY_INSIDE_THREAD_START
public class Config {


    public static List<DirectoryFav> directories=null;

    public static  int token = 0;

    //public static TestCase testCase=null;
    public static  boolean THREAD_STOPPER = false;
    ///Start screen

    public static long TIME_DELAY_START_SCREEN = 1000;
    public static long TIME_DELAY_START_SCREEN_STEP = 20;

    public static Boolean FIRST_TIME_RUN = true;
  //  public static long TIME_DELAY_START_SCREEN_STEP = 20;

    public static Boolean TEST             = false;

    public static Boolean DUPLI_TEST             = true;
    public  static int SEL_TAB = 1;

    public  static int HIDE_TAB = 2;


    public static Boolean IS_BACK             = false;


    public  static  Boolean TIME_TEST =false;
    public  static int TEST_DELY_IN_THREAD = 150;
    public  static int TIME_START = 0;

    public  static String LogoutMessage = "welcome";
    //////////////////////////////////////////////////////////

    public  static String SMS_MESSAGE;
    public  static String SIGNATURE_HASH_TOKEN;
    public  static int OTP;
//////////////////////////////////////////////////////////////////////////////
    public static HashMap<String, Integer> mapUpload = new HashMap<>();
    public static Semaphore semaphoreUpload = new Semaphore(1);

    public  static  void deleteHashUpload(String key)
    {

        Config.mapUpload.remove(key);

    }


    public  static  Boolean KeyExistUpload(String key) throws InterruptedException {



       Boolean boolen = Config.mapUpload.containsKey(key);


        return boolen;

    }
////////////////////////////////////////////////////////////////////////

    public static HashMap<String, Integer> mapDownload = new HashMap<>();
    public static Semaphore semaphoreDownload = new Semaphore(1);

    public  static  void deleteHashDownload(String key)
    {

        Config.mapDownload.remove(key);

    }




    public  static  Boolean KeyExistDownload(String key) throws InterruptedException {



        Boolean boolen = Config.mapDownload.containsKey(key);


        return boolen;

    }
/////////////////////////////////////////////////////////////////////////////////////////



    public static HashMap<String, Integer> profileUpload = new HashMap<>();
    public static Semaphore semaphoreprofileUpload = new Semaphore(1);

    public  static  void deleteHashprofileUpload(String key)
    {

        Config.profileUpload.remove(key);

    }

    public  static  Boolean KeyExistprofileUpload(String key) throws InterruptedException {



        Boolean boolen = Config.profileUpload.containsKey(key);


        return boolen;

    }

    ///////////////////////////////////////
    public static double roundTwo(double val)
    {


        return (double)Math.round(val * 100.0) / 100.00;
    }

    public static double toMB(double val)
    {


        return (double)val/(1024*1024);
    }
    public static double roundFour(double val)
    {


        return (double)Math.round(val * 10000.0) / 10000.00;
    }

/////////////////////////////////////////////////////////////////////////////////


    public static File file_parm=null;
    public static Boolean file_id=false;


public static  String USER_NAME = null;

    ///////////////////////////////////////////


    public static long TIME_DELAY_INSIDE_THREAD_START =0;
   public static long TIME_DELAY_INSIDE_THREAD_HOME =0;
;
    //public  static long TIME_DELAY_FLASH_SCREEN = 1000;
    public static int STRING_TRIM             = 735;



    public static Boolean CHAT_TEST             = true;
    static String                                               name_file=  "com.leak.solver.Mond.";


    public static  double brightnessSpeed =20.05;
    public static float SOUND_RANGE =.05f;

    public static BooVariable bv=null;

    public static boolean login =false;


    public static  void task_download(AbstractChat chat)
    {
        new  DownloadFile(chat.getFileSize(),chat.getFilename()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, chat.getMimage_chat(),chat.getFilename());

    }

    private static class DownloadFile extends AsyncTask<String, String, String> {

        private ProgressDialog progressDialog;
        private String fileName;
        private String folder;
        private  long filesize;
        private boolean isDownloaded;

        public int Token ;

        private int position=0;

        public DownloadFile(long filesize,String fileName) {
            this.filesize = filesize;
            //   Config.directories.set(position-1,new DirectoryFav(fileName, filesize,Long.parseLong(progress[0])  ));
            Config.directories.add(new DirectoryFav(fileName, filesize, filesize) );
             this.fileName=fileName;
            position = Config.directories.size();
        }

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Token = Config.token++;
            // this.progressDialog = new ProgressDialog(firestoreChatActivity);
            // this.progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            // this.progressDialog.setCancelable(false);
            // this.progressDialog.show();



            //Toasty.success(firestoreChatActivity1, "download queue to see the progress!"+Token+"position"+position, Toast.LENGTH_SHORT, true).show();
            //  firestoreChatActivity1=null;

        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url)
        {



            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection connection = url.openConnection();
                connection.connect();
                // getting file length
               // int lengthOfFile = connection.getContentLength();
                long lengthOfFile =filesize;

                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                String timestamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());

                //Extract file name from URL
                fileName = f_url[0].substring(f_url[0].lastIndexOf('/') + 1, f_url[0].length());

                //Append timestamp to file name
                fileName = timestamp + "_" + fileName;

                fileName = f_url[1];


                folder = getExternalStorageDirectory() +
                        java.io.File.separator + "MondInbox/";
                java.io.File directory = new java.io.File(folder);

                if (!directory.exists()) {
                    directory.mkdirs();
                }

                // Output stream to write file
                OutputStream output = new FileOutputStream(folder + fileName);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1)
                {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called

                    long result = (int) ((total * 100) / lengthOfFile);

                     if(result%5==0)
                    publishProgress(Long.toString(result) );

                    if(Config.DUPLI_TEST)
                     Log.d("DOWNLOAD_TEST", "Progress: " + (int) ((total * 100) / lengthOfFile)+"::"+lengthOfFile+"::"+total);

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();
                return "Downloaded at: " + folder + fileName;

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }




            /*
            Log.d("DOWNLOAD_TEST_C"+Token, "Progress: for ##--"+"##");
            long i=0;
            boolean flag=true;
            while(flag){
                publishProgress( Long.toString(i));

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if(Config.DUPLI_TEST)
                    Log.d("DOWNLOAD_TEST_C"+Token, "Progress: for ##--"+"##"+ i++);




            }
*/



            return "Something went wrong";
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            // progressDialog.setProgress(Integer.parseInt(progress[0]));

            if(Config.DUPLI_TEST)
                Log.d("DOWNLOAD_TEST_C"+Token, "Position: for ##--"+"##" +position);

               Config.directories.set(position-1,new DirectoryFav(fileName+" (Download)", filesize,  Long.parseLong(progress[0]) ));
               Config.bv.setBoo(Config.bv.isBoo());
        }


        @Override
        protected void onPostExecute(String message) {
            // dismiss the dialog after the file was downloaded
//            this.progressDialog.dismiss();

            if(Config.DUPLI_TEST)
                Log.d("DOWNLOAD_TEST_C"+Token, "Position: for ##--"+"##"+position);

            Config.directories.set(position-1,new DirectoryFav(fileName+" (Download)", filesize,  Long.parseLong("100") ));
            Config.bv.setBoo(Config.bv.isBoo());








            // Display File path after downloading
            //   Toast.makeText(firestoreChatActivity,                    message, Toast.LENGTH_LONG).show();
        }
    }

    public static String getAppTaskState(Context context    )
    {

        ActivityManager activityManager =
                (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);

        StringBuilder stringBuilder=new StringBuilder();
        int totalNumberOfTasks=activityManager.getRunningTasks(10).size();//Returns total number of tasks - stacks
        stringBuilder.append("\nTotal Number of Tasks: "+totalNumberOfTasks+"\n\n");

        List<ActivityManager.RunningTaskInfo> taskInfo =activityManager.getRunningTasks(10);//returns List of RunningTaskInfo - corresponding to tasks - stacks

        for(ActivityManager.RunningTaskInfo info:taskInfo){
            stringBuilder.append("Task Id: "+info.id+", Number of Activities : "+info.numActivities+"\n");//Number of Activities in task - stack

            // Display the root Activity of task-stack
            stringBuilder.append("TopActivity: "+info.topActivity.getClassName().
                    toString().replace(name_file,"")+"\n");

            // Display the top Activity of task-stack
            stringBuilder.append("BaseActivity:"+info.baseActivity.getClassName().
                    toString().replace(name_file,"")+"\n");
            stringBuilder.append("\n\n");
        }
        return stringBuilder.toString();
    }





}
