package com.media.vidplayer2.mond.zf_space_share.spaceList.ab_bottomsheet;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.media.vidplayer2.mond.R;
import com.media.vidplayer2.mond.zb_Config.Config;
import com.media.vidplayer2.mond.zf_space_share.downloadSpace.DownloadSpaceActivity;
import com.media.vidplayer2.mond.zf_space_share.downloadSpace.FileManagerDownloadSpace;
import com.media.vidplayer2.mond.zf_space_share.spaceList.ListSpaceShareActivity;

import java.util.ArrayList;


public class BottomSheet extends BottomSheetDialog {


    private Context context;

    private static final String[] SCOPES = {DriveScopes.DRIVE};




    File file;

    ListSpaceShareActivity listSpaceShareActivity;

    public BottomSheet(Context context){
        super(context);
        this.context=context;

        listSpaceShareActivity =(ListSpaceShareActivity)context;
    }


    public void SetBottomSheet(File file){


        this.file=file;

    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = getLayoutInflater().inflate(R.layout.ag_bottom_sheet, null);
        setContentView(view);

        ArrayList<Item> items=new ArrayList<>();
        items.add( new Item(R.drawable.aa_crown, "Delete") );
        items.add( new Item(R.drawable.aa_crown, "Download") );
        items.add( new Item(R.drawable.aa_crown, "Rename") );
        items.add( new Item(R.drawable.aa_crown, "Send to Friend") );



        ItemAdapter adapter = new ItemAdapter( this.context, items );

        //ListView for the items
       ListView listView = (ListView) view.findViewById(R.id.list_items);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

               // Intent it = null;

                switch(position){
                    case 0:
                           listSpaceShareActivity.mDriveServiceHelper.delete_file_Case(file.getId());

                        break;
                    case 1:
                        //spaceShareChatUploadActivity.mDriveServiceHelper.download_file_Case(file);


                        Config.file_parm=file;
                        Config.file_id =false;

                        Intent i = new Intent(context, DownloadSpaceActivity.class);

                        context.startActivity(i);




                        break;
                    case 2:

                        break;
                    case 3:

                        break;
                    case 4:

                        break;
                    case 5:

                        break;
                    case 6:

                        break;
                }

              //  context.startActivity(it);
            }
        });

        //GridView for the items
        /*GridView gridView = (GridView) view.findViewById(R.id.grid_items);
        gridView.setAdapter( adapter );
        gridView.setNumColumns(2);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent it = null;

                switch(position){
                    case 0:
                        it = new Intent(Intent.ACTION_VIEW);
                        it.setData(Uri.parse("http://www.whatsapp.com"));
                        break;
                    case 1:
                        it = new Intent(Intent.ACTION_VIEW);
                        it.setData(Uri.parse("http://www.facebook.com"));
                        break;
                    case 2:
                        it = new Intent(Intent.ACTION_VIEW);
                        it.setData(Uri.parse("http://plus.google.com"));
                        break;
                    case 3:
                        it = new Intent(Intent.ACTION_VIEW);
                        it.setData(Uri.parse("http://www.twitter.com"));
                        break;
                    case 4:
                        it = new Intent(Intent.ACTION_VIEW);
                        it.setData(Uri.parse("http://www.youtube.com"));
                        break;
                    case 5:
                        it = new Intent(Intent.ACTION_VIEW);
                        it.setData(Uri.parse("http://www.instagram.com"));
                        break;
                    case 6:
                        it = new Intent(Intent.ACTION_VIEW);
                        it.setData(Uri.parse("http://www.stackoverflow.com"));
                        break;
                }

                context.startActivity(it);
            }
        });*/

    }






}
