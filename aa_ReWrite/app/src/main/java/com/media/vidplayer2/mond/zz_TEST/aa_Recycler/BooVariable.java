package com.media.vidplayer2.mond.zz_TEST.aa_Recycler;

import android.util.Log;

import com.media.vidplayer2.mond.zb_Config.Config;

import java.util.Random;

public class BooVariable {
    private boolean boo = false;
    private ChangeListener listener;

    public boolean isBoo() {
        return boo;
    }

    public void setBoo(boolean boo) {
        this.boo = boo;

        if(Config.DUPLI_TEST)
            Log.d("DRIVE_UPLOAD_TEST", "setBoo: "+this.boo+new Random().nextInt()%100);
        if (listener != null) listener.onChange();
    }

    public ChangeListener getListener() {
        return listener;
    }

    public void setListener(ChangeListener listener) {
        this.listener = listener;
    }

    public interface ChangeListener {
        void onChange();
    }
}