package com.media.vidplayer2.mond.za_global;

import android.app.Activity;
import android.app.Application;
import android.util.Log;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;


import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.media.vidplayer2.mond.AppSignatureHelper;
import com.media.vidplayer2.mond.aa_MainActivity.MainActivity_Start;
import com.media.vidplayer2.mond.ab_home_screen.HomeActivity;
import com.media.vidplayer2.mond.ac_Selector_Reg_or_Login.LoginActivity1;
import com.media.vidplayer2.mond.zb_Config.Config;
import com.media.vidplayer2.mond.zc_VideoFileParser.VideoFileParser;
import com.media.vidplayer2.mond.zz_TEST.aa_Recycler.BooVariable;
import com.media.vidplayer2.mond.zz_TEST.aa_Recycler.DirectoryFav;
import com.squareup.leakcanary.AndroidExcludedRefs;
import com.squareup.leakcanary.ExcludedRefs;
import com.squareup.leakcanary.LeakCanary;
import com.tapadoo.alerter.Alerter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class MyApplication extends Application   implements LifecycleObserver
{







    @Override
    public void onCreate()
    {
        super.onCreate();
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);

        AppSignatureHelper appSignatureHelper = new AppSignatureHelper(this);
     // Config.SIGNATURE_HASH_TOKEN = appSignatureHelper.getAppSignatures().get(0);

        Config.SIGNATURE_HASH_TOKEN = appSignatureHelper.getAppSignatures().get(0);

        if(Config.DUPLI_TEST)
            Log.d("SMS_TEST", "App Restarted");



        if (LeakCanary.isInAnalyzerProcess(this))
        {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }

        ExcludedRefs excludedRefs = AndroidExcludedRefs.createAppDefaults()
                .instanceField("android.view.inputmethod.InputMethodManager", "sInstance")
                .instanceField("android.view.inputmethod.InputMethodManager", "mLastSrvView")
                .instanceField("com.android.internal.policy.PhoneWindow$DecorView", "mContext")
                .instanceField("android.support.v7.widget.SearchView$SearchAutoComplete", "mContext")
                .build();



         LeakCanary.install(this);


        FirebaseFirestore firestore = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setTimestampsInSnapshotsEnabled(true)
                .build();
        firestore.setFirestoreSettings(settings);

        Config.directories = null;
        //Config.directories.clear();
        Config.token = 0;
        Config.directories =  new ArrayList< DirectoryFav>();

        Config.directories.clear();

        Config.bv = new BooVariable();

      



    }


    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {

        if(Config.TEST)
        Log.d("MyApp", "App in background from Appication");

        Thread_KilerCommon();
        VideoFileParser.THREAD_STOPPER=false;
        MainActivity_Start.mainActivityStartS =null;
        HomeActivity.homeActivityS=null;
        LoginActivity1.loginActivity1S =null;
        Config.TIME_START = 0;


        set_online_status(false);



    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded()
    {
        if(Config.TEST)
        Log.d("MyApp", "App in foreground from Appication");
        set_online_status(true);

    }


    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    private void onAppDestroy()
    {
        if(Config.TEST)
            Log.d("MyAppD", "Destroyed");

    }


    public void Thread_KilerCommon()
    {




        if(Config.TEST)
            Log.d("_#_TT_TEST", " Inside T test");





         ///Threads used in MainActivity_Start
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

        if(MainActivity_Start.mainActivityStartS !=null)
        {
            //Thread for making random delay at first time running
            if(MainActivity_Start.mainActivityStartS._fileManager_mainActivity.firstTimeThread!=null)
            {

                if(MainActivity_Start.mainActivityStartS._fileManager_mainActivity.firstTimeThread.isAlive())
                {
                    VideoFileParser.THREAD_STOPPER=true;
                }
                while (MainActivity_Start.mainActivityStartS._fileManager_mainActivity.firstTimeThread.isAlive());

            }
        }


/////////////////////////////////////////////////////////////////////////////////////////////////////////////





        if(VideoFileParser.getInstance().threadForStorageReadDirectoryI !=null)
        {

            if(VideoFileParser.getInstance().threadForStorageReadDirectoryI.isAlive())
            {
                VideoFileParser.THREAD_STOPPER=true;
            }



            while (VideoFileParser.getInstance().threadForStorageReadDirectoryI.isAlive())
            {

                // if(Config.TEST)
                //    Log.d("_#_TT_TEST", " threadForStorageReadDirectoryI"+firstTimeThread.isAlive()+"::"+THREAD_STOPPER);
            }


        }


        if(VideoFileParser.getInstance().threadForStorageReadDirectoryE !=null)
        {

            if(VideoFileParser.getInstance().threadForStorageReadDirectoryE.isAlive())
            {
                VideoFileParser.THREAD_STOPPER=true;
            }



            while (VideoFileParser.getInstance().threadForStorageReadDirectoryE.isAlive())
            {
            }


        }




////////////////////////////////////// Home  activity thread /////////////////////////////////////

 //Fav frag thread






     if(HomeActivity.homeActivityS!=null)
     {

         if(HomeActivity.homeActivityS.fileManager_homeActivity.folderFileManager.threadForReadingFilesFromDir !=null)
         {

             if(HomeActivity.homeActivityS.fileManager_homeActivity.folderFileManager.threadForReadingFilesFromDir.isAlive())
             {
                 VideoFileParser.THREAD_STOPPER=true;
             }



             while (HomeActivity.homeActivityS.fileManager_homeActivity.folderFileManager.threadForReadingFilesFromDir.isAlive());



         }

         HomeActivity.homeActivityS.fileManager_homeActivity.folderFileManager.threadForReadingFilesFromDir =null;
     }




        if(MainActivity_Start.mainActivityStartS !=null)
        MainActivity_Start.mainActivityStartS._fileManager_mainActivity.firstTimeThread=null;



        VideoFileParser.THREAD_STOPPER=false;
        VideoFileParser.getInstance().threadForStorageReadDirectoryI =null;
        VideoFileParser.getInstance().threadForStorageReadDirectoryE =null;





    }













    void set_online_status(Boolean online)
    {


        if(  FirebaseAuth.getInstance().getCurrentUser()!=null)
        {
            String current_id = FirebaseAuth.getInstance().getCurrentUser().getUid();

            HashMap<String, Object> UserStat = new HashMap<>();



            if(online){

                UserStat.put("online", true);

            }
            else
            {
                UserStat.put("online", false);

            }

            UserStat.put("time_stamp",  FieldValue.serverTimestamp());


            FirebaseFirestore.getInstance().collection("UsersList").document(current_id).update(UserStat).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {



                }
            });

        }




    }







}

