package com.media.vidplayer2.mond.ab_home_screen.aa_tabs.ac_folderview;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.media.vidplayer2.mond.ab_home_screen.HomeActivity;
import com.media.vidplayer2.mond.ab_home_screen.aa_tabs.ac_folderview.aa_Recycler.Directory_folder;
import com.media.vidplayer2.mond.ab_home_screen.aa_tabs.ac_folderview.aa_Recycler.RecyclerAdapter_folder;
import com.media.vidplayer2.mond.zb_Config.Config;
import com.media.vidplayer2.mond.zc_VideoFileParser.VideoFileParser;

import java.io.File;
import java.io.FileFilter;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

import static android.os.Build.ID;

public class FolderFileManager
{
    public ThreadForReadingFilesFromDir threadForReadingFilesFromDir =null;
    Context context=null;

    Handler Folder_Frag_directory_handler = null;
    public RecyclerView Folder_Frag_recyclerView =null;
    public RecyclerAdapter_folder Folder_Frag_recyclerAdapter =null;
    public SwipeRefreshLayout Folder_Frag_mSwipeRefreshLayout =null;
    public boolean FOLDER_FRAG_READY_TO_UPDATE_FOLDER = false;


    public FolderFileManager(Context context) {
        this.context = context;
        Folder_Frag_directory_handler = new Handler(Looper.myLooper());
    }


    public void MakeNewFilesFromDirectoryReading( File path,CyclicBarrier barrier)
    {
        threadForReadingFilesFromDir = new ThreadForReadingFilesFromDir(path,barrier) ;
    }

    public void getfolder_Content(File dir) throws InterruptedException
    {


        final int[] count__folder = {0};

        VideoFileParser.getInstance().total_folder.clear();

        File[] directories = dir.listFiles(new FileFilter()
        {



            @Override
            public boolean accept(File pathname)
            {

                if(Config.TEST){

                    Log.d("_#_FILE_SEARCH_1", "onCreate: MyApplication make mem A :: " +pathname);

                }

                Boolean flag_video = false;

                if (
                        pathname.getName().endsWith(".3gp")
                                || pathname.getName().endsWith(".avi")
                                || pathname.getName().endsWith(".flv")
                                || pathname.getName().endsWith(".m4v")
                                || pathname.getName().endsWith(".mkv")
                                || pathname.getName().endsWith(".mov")

                                || pathname.getName().endsWith(".mp4")
                                || pathname.getName().endsWith(".mpeg")
                                || pathname.getName().endsWith(".mpg")
                                || pathname.getName().endsWith(".mts")
                                || pathname.getName().endsWith(".vob")



                                || pathname.getName().endsWith(".webm")
                                || pathname.getName().endsWith(".wmv")


                )

                {


                    if(Config.TEST){

                        Log.d("_#_FILE_SEARCH_2", "onCreate: MyApplication make mem B :: " +pathname);

                    }


                    flag_video =true;
                    count__folder[0]++;



                    // Get length of file in bytes
                    long fileSizeInBytes = pathname.length();
                    // Convert the bytes to Kilobytes (1 KB = 1024 Bytes)
                    long fileSizeInKB = fileSizeInBytes / 1024;
                    //  Convert the KB to MegaBytes (1 MB = 1024 KBytes)
                    long fileSizeInMB = fileSizeInKB / 1024;




                    String string_path = pathname.toString();
                    String[] parts = string_path.split("/");



                    Directory_folder directory_folder = new Directory_folder(string_path,fileSizeInMB+ "MB",parts[parts.length - 1]);


                    VideoFileParser.getInstance().total_folder.add(directory_folder);


                }




                return flag_video ;
            }
        });





    }


    public void ReadDirByThread_folder(File path)
    {
        //  Toast.makeText(getApplicationContext(), "Your Message", Toast.LENGTH_LONG).show();





        if (Config.TEST)
            Log.d("DONE_FOLDER", "Done Folder -1");





        if (Config.TEST)
        {
            for (int x = 0; x < VideoFileParser.getInstance().total_folder.size(); x++)
            {
                Directory_folder directory_folder = VideoFileParser.getInstance().total_folder.get(x);
                Log.d("DONE_FOLDER",directory_folder.file_path+":**:"+directory_folder.size+"::"+directory_folder.file_name);

            }

            Log.d("DONE_FOLDER", VideoFileParser.getInstance().total_folder.size()+"");


        }

        Runnable barrierAction = new Runnable() { public void run()
        {




            Folder_Frag_updateUIRecycler();


            if (Config.TEST)
                Log.d("DONE_FOLDER", "Done Folder 1");




            if (Config.TEST)
            {
                for (int x = 0; x <  VideoFileParser.getInstance().total_folder.size(); x++)
                {
                    Directory_folder directory_folder = VideoFileParser.getInstance().total_folder.get(x);
                    Log.d("DONE_FOLDER",directory_folder.file_path+":**:"+directory_folder.size+"::"+directory_folder.file_name);

                }

                Log.d("DONE_FOLDER", VideoFileParser.getInstance().total_folder.size()+"");


            }





        }};


        CyclicBarrier barrier = new CyclicBarrier(1, barrierAction);
        if (Config.TEST)
            Log.d("DONE_FOLDER", "Done Folder 0");

        ((HomeActivity)context).fileManager_homeActivity.folderFileManager.MakeNewFilesFromDirectoryReading(path,barrier);
        ((HomeActivity)context).fileManager_homeActivity.folderFileManager.threadForReadingFilesFromDir.start();

    }




    public void Folder_Frag_updateUIRecycler()
    {



        Runnable r = new Runnable()
        {
            @Override
            public void run() {




                if (Config.TEST)
                {
                    for (int x = 0; x < VideoFileParser.getInstance().total_folder.size(); x++)
                    {
                        Directory_folder directory_folder = VideoFileParser.getInstance().total_folder.get(x);
                        Log.d("_AFRAG_FD_F",directory_folder.file_path+":**:"+directory_folder.size+"::"+directory_folder.file_name);

                    }

                    Log.d("_AFRAG_FD_F", VideoFileParser.getInstance().total_folder.size()+"");


                }




                if (Config.TIME_TEST)
                {
                    if(VideoFileParser.getInstance().total_folder==null)
                    {
                        Log.d("_AFRAG_FD_F",  "null at MainActivity_Start.mainActivityStartS._fileManager_mainActivity.total_SD_MEM");
                    }
                    else
                    {
                        Log.d("_AFRAG_FD_F",  "not null total_SD_MEM");

                    }








                }




                Folder_Frag_recyclerAdapter.setRecyclerAdapter(VideoFileParser.getInstance().total_folder, context);
                Folder_Frag_recyclerAdapter.notifyItemRangeChanged(0, VideoFileParser.getInstance().total_folder.size());
                Folder_Frag_recyclerAdapter.notifyDataSetChanged();









            }
        };


        if (Config.TEST)
            Config.TIME_START =0;

        ///Block here
        while (!FOLDER_FRAG_READY_TO_UPDATE_FOLDER) {


            if (Config.TEST)
                Log.d("_AFRAG_STUCK", FOLDER_FRAG_READY_TO_UPDATE_FOLDER+"::"+Config.TIME_START++);
        }


        if (Config.TEST)
            Log.d("_#YESCASE_YY", "7");

        if (!VideoFileParser.THREAD_STOPPER)
        {
            Folder_Frag_directory_handler.post(r);
        }
        else
        {
            Folder_Frag_directory_handler.removeCallbacksAndMessages(null);
        }


        if (Config.TEST)
            Log.d("DONE_FOLDER", "Done Folder 2");


    }


    public  class ThreadForReadingFilesFromDir extends Thread {

        File dir;

        private CyclicBarrier barrier;
        public ThreadForReadingFilesFromDir(File dir, CyclicBarrier barrier) {
            this.barrier=barrier;
            this.dir =dir;

        }

        /*
         * Defines the code to run for this task.
         */
        @Override
        public void run()
        {
            // Moves the current Thread into the background
            //  android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);



            if(!VideoFileParser.THREAD_STOPPER ) {
                try {
                   getfolder_Content(dir);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            try {
                this.barrier.await();
            } catch (BrokenBarrierException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Thread #" + ID + " passed the barrier.");

        }

    }

}
