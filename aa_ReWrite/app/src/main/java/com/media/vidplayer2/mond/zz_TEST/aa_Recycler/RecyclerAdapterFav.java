package com.media.vidplayer2.mond.zz_TEST.aa_Recycler;

import android.app.Application;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.media.vidplayer2.mond.R;
import com.media.vidplayer2.mond.ab_home_screen.HomeActivity;
import com.media.vidplayer2.mond.zb_Config.Config;
import com.media.vidplayer2.mond.zc_VideoFileParser.VideoFileParser;
import com.media.vidplayer2.mond.zz_TEST.TestCase;


import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RecyclerAdapterFav extends RecyclerView.Adapter<ViewHolderFav> {


    private Context context;








    public RecyclerAdapterFav( Context context)
    {


        this.context = context;






        if (Config.TEST)
        {
            Log.d("ITEMS_RECYCLER",  "1");

        }






    }

    public void setRecyclerAdapter(List<DirectoryFav> directories, Context context)
    {
        Config.directories = directories;
        this.context = context;

        if (Config.TEST)
        {
            Log.d("ITEMS_RECYCLER",  "2");

        }

    }

    @NonNull
    @Override
    public ViewHolderFav onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {


        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.zz_c_activity_queue_slice_recycler_item, viewGroup, false);

        if (Config.TEST)
        {
            Log.d("ITEMS_RECYCLER",  "3");

        }

        return new ViewHolderFav(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderFav viewHolderFav, final int i)
    {


        DirectoryFav directoryFav = Config.directories.get(i);



        if (Config.DUPLI_TEST)
        {
            Log.d("ITEMS_RECYCLER_CASE",  directoryFav.name+"::"+Long.toString(directoryFav.progress)+"::"+Long.toString(directoryFav.size));

        }

        viewHolderFav.name.setText(directoryFav.name);
        viewHolderFav.file_progress.setText(Long.toString(directoryFav.progress)+"%");

        viewHolderFav.file_size.setText(Long.toString(directoryFav.size));





    }





    @Override
    public int getItemCount() {



        return Config.directories.size();
    }


}
