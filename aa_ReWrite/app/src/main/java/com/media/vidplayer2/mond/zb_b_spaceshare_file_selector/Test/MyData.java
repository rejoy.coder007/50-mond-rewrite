package com.media.vidplayer2.mond.zb_b_spaceshare_file_selector.Test;

public class MyData {
    private int id;
    private String title;

    public MyData(int id, String title) {
        this.id = id;
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }
}