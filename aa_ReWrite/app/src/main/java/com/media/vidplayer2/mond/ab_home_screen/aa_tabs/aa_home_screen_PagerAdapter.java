package com.media.vidplayer2.mond.ab_home_screen.aa_tabs;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;


import com.google.firebase.firestore.ListenerRegistration;
import com.media.vidplayer2.mond.ab_home_screen.aa_tabs.aa_ChatFragments.FriendListFragmentF;
import com.media.vidplayer2.mond.ab_home_screen.aa_tabs.ab_favorites.FavFragment;
import com.media.vidplayer2.mond.ab_home_screen.aa_tabs.ac_folderview.FolderFragment;
import com.media.vidplayer2.mond.zb_Config.Config;

import java.util.ArrayList;

public class aa_home_screen_PagerAdapter extends FragmentPagerAdapter
{

    public static SparseArray<Fragment> registeredFragments = new SparseArray<>();

    public static final String                                                          ARG_PAGE = "ARG_PAGE";
    final int                                                                                   PAGE_COUNT =3;
    private String tabTitles[] = new String[] { "SpaceShare", "Favorite", "FolderView" };
    private Context context;

    public aa_home_screen_PagerAdapter(FragmentManager fm, Context context)
    {
        //super(fm);
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.context = context;

        registeredFragments.put(0, null);
        registeredFragments.put(1, null);
        registeredFragments.put(2, null);

    }

    @Override
    public int getCount()
    {

        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position)
    {
        Fragment fragment=null;


        switch (position)
        {
            case 0:
            {
                fragment = new FriendListFragmentF();
                Bundle args = new Bundle();
                args.putInt(ARG_PAGE, 0);
                fragment.setArguments(args);
                break;
            }
           case 1:
            {
                fragment = new FavFragment();
                Bundle args = new Bundle();
                args.putInt(ARG_PAGE, 1);
                fragment.setArguments(args);
                break;
            }

            case 2:
            {
                fragment = new FolderFragment();
                Bundle args = new Bundle();
                args.putInt(ARG_PAGE, 2);
                fragment.setArguments(args);
                break;
            }

       }

        if(Config.TEST)
        {
            Log.d("_AFRAG_FD_NEW", "getItem:   : HomeActivity  "+position );

        }

        return fragment;

    }

    @Override
    public CharSequence getPageTitle(int position)
    {
      return tabTitles[position];
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position)
    {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
       // registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
      //  registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }


}
