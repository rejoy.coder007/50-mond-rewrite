package com.media.vidplayer2.mond.zg_contactlist;
        /*

        754-3010


Local

(541) 754-3010


Domestic

+1-541-754-3010


International

1-541-754-3010


Dialed in the US

001-541-754-3010


Dialed from Germany

191 541 754 3010


Dialed from France

Now consider a German phone number. Although a German phone number consists of an area code and an extension like a US number, the format is different. Here is the same German phone number in a variety of formats:

636-48018


Local

(089) / 636-48018


Domestic

+49-89-636-48018


International

19-49-89-636-48018


Dialed from France

         */

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.MetadataChanges;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.WriteBatch;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.media.vidplayer2.mond.R;
import com.media.vidplayer2.mond.zb_Config.Config;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ContactActivity extends AppCompatActivity {

    String name = null, phoneNumber = null;
    Cursor phones=null;
    private final int PICK_CONTACT = 5;
    ListenerRegistration UsersDetailsListner;
    @BindView(R.id.ContactId)
    public TextView contactMessage;


    @BindView(R.id.ph_add)
    public LinearLayout ph_add;

    @BindView(R.id.ccp_1)
    public com.rilixtech.CountryCodePicker countryCodePicker;

    @BindView(R.id.mob_no_1)
    EditText mob_no_1;


    @BindView(R.id.name_c)
    TextView name_c;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ak_activity_contact);

        ButterKnife.bind(this);
        Intent intent = new Intent(Intent.ACTION_PICK,
                ContactsContract.Contacts.CONTENT_URI);
        intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
        startActivityForResult(intent, PICK_CONTACT);

    }

    @OnClick(R.id.add_frd)
    public void settings_image_btn_change_status(View view)
    {


        CheckUser();





    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == PICK_CONTACT && resultCode == RESULT_OK)
        {
            Uri contactData = data.getData();

             phones = getContentResolver()
                    .query(contactData,
                            null,
                            null,
                            null,
                            null);


            while (phones.moveToNext())
            {
                name = phones.getString(phones.getColumnIndex(
                        ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                phoneNumber = phones.getString(phones.getColumnIndex(
                        ContactsContract.CommonDataKinds.Phone.NUMBER));


                if (Config.DUPLI_TEST)
                {
                    String[] S = new String[6];
                    S[0] = "044 668 18 00";
                    S[1] = "+9144 668 18 00";
                    S[2] = "+91-44 -668 -18-00";
                    S[3] = "+91(44) -(668) -18-00";
                    S[4] = "+1000 -(668) -18-00";
                    S[5] = "+0000 -(668) -18-00111";


                    String[] S1 = new String[12];
                    S1[0] = "+13028580567";
                    S1[1] = "+919074671981";
                    S1[2] = "09074671981";
                    S1[3] = "+436703091189";
                    S1[4] = "+036703091189";
                    S1[5] = "+12044103294";
                    S1[6] = "754-3010";
                    S1[7] = "(541) 754-3010";
                    S1[8] = "+1-541-754-3010";
                    S1[9] = "1-541-754-3010";
                    S1[10] = "001-541-754-3010";
                    S1[11] = "191 541 754 3010";

/*
                    for (int i = 0; i < S.length; i++) {
                        if (Config.DUPLI_TEST)
                            Log.d("_PH_NO", i + "::" + getRefinedNumber(S[i],true) + "::" + getRefinedNumber(S[i]) + "::" + getRefinedNumber(S[i]) + "::" + getRefinedNumber(S[i]) + "::" + getRefinedNumber(S[i]) + "::" + getRefinedNumber(S[i]));


                    }

                    for (int i = 0; i < S1.length; i++) {


                        if (Config.DUPLI_TEST)
                            Log.d("_PH_NO", i + "::" + phoneNumber + "::" + getRefinedNumber(getCountry(S1[i])) + ":-----------:" + getRefinedNumber(getNumber(S1[i])));


                    }
*/
                }




                if (Config.DUPLI_TEST)
                    Log.d("_PH_NO", "name :: " + name + "phone :: " + phoneNumber);


            }



            if (Config.DUPLI_TEST)
                Log.d("_PH_NO_C", "name :: " + name + "phone :: " + phoneNumber+"::"+getCountryX(getRefinedNumber(phoneNumber,true))+"::"+phoneNumber.charAt(0));


            if(getCountryX(getRefinedNumber(phoneNumber,true))==null ||phoneNumber.charAt(0) !='+')
            {

                mob_no_1.setText(getRefinedNumber(phoneNumber,false));
                name_c.setText(name);
            }
            else {

                countryCodePicker.setCountryForPhoneCode(getCountryX(getRefinedNumber(phoneNumber,true)));
                mob_no_1.setText(getNumber(getRefinedNumber(phoneNumber,true)));
                name_c.setText(name);

            }




/*
            if( getRefinedNumber(phoneNumber.toString())!=null )
            {
                mob_no_1.setText(phoneNumber.toString());
            }
            else {

            }
*/
            phones.close();
            ph_add.setVisibility(View.VISIBLE);

        }

        super.onActivityResult(  requestCode,   resultCode,   data);
    }


    public void CheckUser() {

        /*
        @BindView(R.id.ccp_1)
        public com.rilixtech.CountryCodePicker countryCodePicker;

        @BindView(R.id.mob_no_1)
        EditText mob_no_1;
*/

       ;



        countryCodePicker.registerPhoneNumberTextView(mob_no_1);



            String current_uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
            if (Config.DUPLI_TEST)
                Log.d("_PH_NO", "Number Searched "+countryCodePicker.getFullNumberWithPlus() );


            FirebaseFirestore.getInstance().collection("UsersList").whereEqualTo("mob",countryCodePicker.getFullNumberWithPlus()).get()
                    .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                        @Override
                        public void onSuccess(QuerySnapshot queryDocumentSnapshots)
                        {
                            if (Config.DUPLI_TEST)
                                Log.d("_PH_NO", "Failed "+queryDocumentSnapshots.size() );


                            if (queryDocumentSnapshots.size() > 0)
                            {



                                UsersData object=queryDocumentSnapshots.getDocuments().get(0).toObject(UsersData.class);


                                if (Config.DUPLI_TEST)
                                    Log.d("_PH_NO", "Get Number "+object.getMob() );

                                FirebaseFirestore.getInstance().collection("Friends").document(current_uid).collection("MyFriends").document(object.getMob()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                        if (task.isSuccessful()) {
                                            DocumentSnapshot documentSnapshot = task.getResult();
                                            if (documentSnapshot.exists())
                                            {


                                                contactMessage.setText("User is already your friend");
                                            }
                                            else
                                            {




                                                HashMap<String, Object> FrduserMap_1 = new HashMap<>();


                                                FrduserMap_1.put("id", object.getId());
                                                FrduserMap_1.put("block",  false);
                                                FrduserMap_1.put("time_stamp",   FieldValue.serverTimestamp());
                                                FrduserMap_1.put("time_stampN",   FieldValue.serverTimestamp());
                                                HashMap<String, Object> FrduserMap_2 = new HashMap<>();


                                                FrduserMap_2.put("id",current_uid);
                                                FrduserMap_2.put("block",  false);
                                                FrduserMap_2.put("time_stamp",   FieldValue.serverTimestamp());
                                                FrduserMap_2.put("time_stampN",   FieldValue.serverTimestamp());
                                                WriteBatch batch =  FirebaseFirestore.getInstance().batch();





                                                DocumentReference FriendListReference_1  =  FirebaseFirestore.getInstance().collection("Friends").document(current_uid).collection("MyFriends").document(object.getId());
                                                DocumentReference FriendListReference_2  =  FirebaseFirestore.getInstance().collection("Friends").document(object.getId()).collection("MyFriends").document(current_uid);

                                              //  DocumentReference ChatReference_1  =  FirebaseFirestore.getInstance().collection("Chats").document(current_uid).collection("MyChats").document(object.getId());
                                              //  DocumentReference ChatReference_2  =  FirebaseFirestore.getInstance().collection("Chats").document(object.getId()).collection("MyCats").document(current_uid);

                                                batch.set(FriendListReference_1, FrduserMap_1);
                                                batch.set(FriendListReference_2, FrduserMap_2);



                                                batch.commit().addOnSuccessListener(new OnSuccessListener<Void>() {
                                                    @Override
                                                    public void onSuccess(Void aVoid) {

                                                        onBackPressed();

                                                    }


                                                }).addOnFailureListener(new OnFailureListener() {
                                                    @Override
                                                    public void onFailure(@NonNull Exception e) {



                                                        contactMessage.setText("Connection Problem");

                                                    }
                                                });




                                            }
                                        }
                                        else
                                        {
                                            contactMessage.setText("Connection Problem");
                                        }
                                    }
                                });














                                contactMessage.setText("please wait .. ");


                            }
                            else
                            {



                                contactMessage.setText("This contact has no Mond Account. Please Make sure you have Entered <Country Code><Mobile number> format properly");



                            }


                            // ...
                        }
                    });










    }
    public String getRefinedNumber(String no,Boolean needplus  )
    {


        // only keep digits
        String phoneNumber = no.replaceAll("[^0-9]", "");
// trim leading zeroes

        if(needplus)
        phoneNumber =  phoneNumber.replaceFirst("^0*(.*)","$1");


        if(needplus)

        return '+'+phoneNumber;
        else
            return phoneNumber;

    }


     String getCountry(String no)
    {
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        Phonenumber.PhoneNumber numberProto=null;



        try {
            numberProto = phoneUtil.parse(no, "");

          //  System.out.println("Country code: " + numberProto.getCountryCode());
            //This prints "Country code: 91"
        } catch (NumberParseException e) {
          //  System.err.println("NumberParseException was thrown: " + e.toString());

           // if(Config.DUPLI_TEST)
             //   Log.d("_PH_NO", no+"***"+e);

        }

        if(numberProto==null)
            return  "Invalid number";

        return Integer.toString(numberProto.getCountryCode());
    }

    Integer getCountryX(String no)
    {
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        Phonenumber.PhoneNumber numberProto=null;



        try {
            numberProto = phoneUtil.parse(no, "");

            //  System.out.println("Country code: " + numberProto.getCountryCode());
            //This prints "Country code: 91"
        } catch (NumberParseException e) {
            //  System.err.println("NumberParseException was thrown: " + e.toString());

            // if(Config.DUPLI_TEST)
            //   Log.d("_PH_NO", no+"***"+e);

        }

        if(numberProto==null)
            return  0;

        return  numberProto.getCountryCode() ;
    }

    String getNumber(String no)
    {
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        Phonenumber.PhoneNumber numberProto=null;
        try {
            numberProto = phoneUtil.parse(no, "");


            //  System.out.println("Country code: " + numberProto.getCountryCode());
            //This prints "Country code: 91"
        } catch (NumberParseException e) {
            //  System.err.println("NumberParseException was thrown: " + e.toString());

            //if(Config.DUPLI_TEST)
            //    Log.d("_PH_NO", no+"***"+e);
        }



        if(numberProto==null)
            return  "Invalid number";


        String no_o = no.replaceAll(Integer.toString(numberProto.getCountryCode()), "");

        String Code = Integer.toString(numberProto.getCountryCode());

        if (Config.DUPLI_TEST)
           Log.d("_PH_NO_1",  no+"::"+Code+"::"+no.substring(Code.length()+1));


        return  no.substring(Code.length()+1);
    }


    @Override
    protected void onStop() {

        super.onStop();  // Always call the superclass method first


      //  UsersDetailsListner.remove();
    }


}
