package com.media.vidplayer2.mond.ab_home_screen.aa_tabs.ab_favorites.aa_Recycler;

public class DirectoryFav {

    public String dir_name;

    public DirectoryFav(String dir_name, String path, String video_count) {
        this.dir_name = dir_name;
        this.path = path;
        this.video_count = video_count;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String path;
    public String getDir_name() {
        return dir_name;
    }

    public void setDir_name(String dir_name) {
        this.dir_name = dir_name;
    }

    public String getVideo_count() {
        return video_count;
    }

    public void setVideo_count(String video_count) {
        this.video_count = video_count;
    }

    public String video_count;
}
