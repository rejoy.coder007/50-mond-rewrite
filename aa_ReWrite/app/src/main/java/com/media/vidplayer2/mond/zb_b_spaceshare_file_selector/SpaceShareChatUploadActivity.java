package com.media.vidplayer2.mond.zb_b_spaceshare_file_selector;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.media.vidplayer2.mond.R;
import com.media.vidplayer2.mond.ad_Profile.FileManagerProfile;
import com.media.vidplayer2.mond.ad_Profile.MyProfileActivity;
import com.media.vidplayer2.mond.ad_Profile.statusActvity;

import com.media.vidplayer2.mond.zb_Config.Config;
import com.media.vidplayer2.mond.zb_b_spaceshare_file_selector.aa_Recycler.Directory_folder;
import com.media.vidplayer2.mond.zb_b_spaceshare_file_selector.aa_Recycler.RecyclerAdapter_UploadSpace;
import com.media.vidplayer2.mond.ze_LeakManager.IMMLeaks;
import com.media.vidplayer2.mond.zf_space_share.spaceList.FileListSpaceShareActivity;
import com.media.vidplayer2.mond.zf_space_share.spaceList.ListSpaceShareActivity;


import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SpaceShareChatUploadActivity extends AppCompatActivity {
    public Handler mainHandler;
    public FileListSpaceShareUploadActivity mDriveServiceHelper=null;
    public GoogleAccountCredential credential;
    public GoogleSignInAccount acct;

    public Thread thread;
    @BindView(R.id.contact_recycleView_folder_spacelist_uploadspace)
    public RecyclerView Folder_Frag_recyclerView;

    public List<Directory_folder> total_folder =null;
    public RecyclerAdapter_UploadSpace Folder_Frag_recyclerAdapter=null;


    public String current_email;
    public String other_user_email;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.zzzz_activity_space_share_chat_upload);



        ButterKnife.bind(this);

          mainHandler = new Handler( getMainLooper());








        credential =
                GoogleAccountCredential.usingOAuth2(
                        this, Collections.singleton(DriveScopes.DRIVE_FILE));

        acct = GoogleSignIn.getLastSignedInAccount(SpaceShareChatUploadActivity.this);

        credential.setSelectedAccount(acct.getAccount());


        Drive googleDriveService =
                new Drive.Builder(
                        AndroidHttp.newCompatibleTransport(),
                        new GsonFactory(),
                        credential)
                        .setApplicationName("Drive API Migration")
                        .build();

        // The DriveServiceHelper encapsulates all REST API and SAF functionality.
        // Its instantiation is required before handling any onClick actions.


        mDriveServiceHelper = new FileListSpaceShareUploadActivity(googleDriveService, SpaceShareChatUploadActivity.this);





        //  MainActivity_Start.mainActivityStartS._fileManager_mainActivity.total_folder.clear();

        Folder_Frag_recyclerView.setHasFixedSize(true);
        Folder_Frag_recyclerAdapter = new RecyclerAdapter_UploadSpace(  mDriveServiceHelper.list_of_files, SpaceShareChatUploadActivity.this,SpaceShareChatUploadActivity.this);


        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(SpaceShareChatUploadActivity.this);
        Folder_Frag_recyclerView.setLayoutManager(linearLayoutManager);
        Folder_Frag_recyclerView.setAdapter( Folder_Frag_recyclerAdapter);



        IMMLeaks.fixFocusedViewLeak(getApplication());


        other_user_email = getIntent().getStringExtra("other_user_email");



        current_email = getIntent().getStringExtra("current_email");


      //  thread.start();

    }

    public void goback(File file)
    {


    }





    @Override
    public void onResume(){
        super.onResume();
        // put your code here...

        //  mDriveServiceHelper.query();
       mDriveServiceHelper.ListFilesDrive_A();
    }



}
