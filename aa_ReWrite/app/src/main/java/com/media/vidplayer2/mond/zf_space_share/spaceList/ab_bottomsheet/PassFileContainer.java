package com.media.vidplayer2.mond.zf_space_share.spaceList.ab_bottomsheet;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.api.services.drive.model.File;

import java.io.Serializable;


public class PassFileContainer implements Serializable {

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public PassFileContainer(File file) {
        super();
        this.file = file;
    }

    File file;


}