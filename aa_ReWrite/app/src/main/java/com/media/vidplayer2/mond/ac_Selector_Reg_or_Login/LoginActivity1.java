package com.media.vidplayer2.mond.ac_Selector_Reg_or_Login;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.CredentialRequest;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.media.vidplayer2.mond.R;
import com.media.vidplayer2.mond.zb_Config.Config;
import com.media.vidplayer2.mond.ze_LeakManager.IMMLeaks;


import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.rilixtech.CountryCodePicker;

/*
client ID
 941044327915-lr4k0ld5abprl0avof6t7dubhvc76e9i.apps.googleusercontent.com

  abEVDlC2rphWthh6Ybk08Ya1

  +15555215554

 */
public class LoginActivity1 extends AppCompatActivity
{

    public static final int                                                          RC_SIGN_IN = 8;
    public static final int                                             PERMISSION_REQUEST_CODE = 1;
    public static final int                RC_REQUEST_PERMISSION_SUCCESS_CONTINUE_FILE_CREATION = 3;
    public static final int                                                        RESOLVE_HINT = 4;
    public static  final  int MY_PERMISSIONS_REQUEST_SEND_SMS = 101;

    private GoogleApiClient                                                       credentialsClient;
    private CredentialRequest                                                    credentialsRequest;

    public SignInButton                                                                       login;

    LoginActivity1 loginActivity1 =null;
    public  static LoginActivity1 loginActivity1S =null;

    @BindView(R.id.logout)
    public Button logout;

    @BindView(R.id.btn_layout)
    public LinearLayout linearLayout;


    @BindView(R.id.text_id)
    public TextView text;



    @BindView(R.id.mob_no)
    public EditText mob_no;


    @BindView(R.id.code)
    public TextView code;


    @BindView(R.id.progress_circular)
    public ProgressBar progressBar;




    @BindView(R.id.phno_ver)
    public LinearLayout phno_ver;

    @BindView(R.id.ccp)
    public CountryCodePicker ccp;




    public  String[] paths;
    public ArrayAdapter<String>adapter;
    public FirebaseAuth                                                                 mAuth=null;
    public FirebaseFirestore                                                       mFirestore=null;
    public GoogleSignInClient                                           mGoogleSignInClient = null;
    public GoogleSignInOptions gso=null;
    public GoogleApiClient mGoogleApiClient=null;
    public FileManagerLoginAcitivity1 fileManagerLoginAcitivity1 =null;
    private static final AtomicInteger SAFE_ID = new AtomicInteger(10);





    @OnClick(R.id.get_code)
    public void get_Code(View view)
    {


        ccp.registerPhoneNumberTextView(mob_no);

        if(Config.DUPLI_TEST)
            Log.d("SMS_CASE_0",  "Start permission"+ccp.getNumber().toString());




        if(isValidMobile(ccp.getNumber().toString()))
        {


            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.SEND_SMS)
                    != PackageManager.PERMISSION_GRANTED)
            {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.SEND_SMS))
                {
                }
                else
                {
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.SEND_SMS},
                            MY_PERMISSIONS_REQUEST_SEND_SMS);
                }
            }
            else {
                fileManagerLoginAcitivity1.send_sms();
            }



        }
        else
        {
            Toast toast=Toast.makeText(getApplicationContext(),"Please enter a valid number",Toast.LENGTH_SHORT);

            toast.show();
        }





    }


    @OnClick(R.id.verify_code)
    public void verify_code(View view)
    {
        // Firebase sign out

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.SEND_SMS)
                == PackageManager.PERMISSION_GRANTED)
        {
            fileManagerLoginAcitivity1.verify_sms();

        }


        else
        {
            Toast toast=Toast.makeText(getApplicationContext(),"request code first",Toast.LENGTH_SHORT);

            toast.show();
        }








    }

    public static int getRandomNumberInRange(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    public static boolean isValidMobile(String phone) {
        boolean check = false;
        if (!Pattern.matches("[a-zA-Z]+", phone)) {
            if (phone.length() < 9 || phone.length() > 13) {
                // if(phone.length() != 10) {
                check = false;
                // txtPhone.setError("Not Valid Number");
            } else {
                check = android.util.Patterns.PHONE.matcher(phone).matches();
            }
        } else {
            check = false;
        }
        return check;
    }



    @OnClick(R.id.logout)
    public void logout(View view)
    {
        // Firebase sign out

        if(mAuth!=null)
        mAuth.signOut();


       if(GoogleSignIn.getLastSignedInAccount(LoginActivity1.this) != null)
       {
           Config.LogoutMessage = "Please sign in again";
           mGoogleSignInClient.signOut().addOnCompleteListener(this,
                   task -> fileManagerLoginAcitivity1.UIAfterLogout());
       }




    }











    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ad_activity_login);


        ButterKnife.bind(this);




        login = findViewById(R.id.login);

        fileManagerLoginAcitivity1 = new FileManagerLoginAcitivity1(LoginActivity1.this);


        mAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();

         gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, null)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();






       //  phno_ver.setVisibility(View.GONE);




        login.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {













                progressBar.setVisibility(View.VISIBLE);
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);











                }



        });



        IMMLeaks.fixFocusedViewLeak(getApplication());

        loginActivity1 =this;
        LoginActivity1.loginActivity1S =this;

    }


    @Override
    public void onResume() {
        super.onResume();
        //  unbinder= ButterKnife.bind(this);

        LoginActivity1.loginActivity1S = loginActivity1;



    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        LoginActivity1.loginActivity1S = null;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        fileManagerLoginAcitivity1.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults)
    {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        fileManagerLoginAcitivity1.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }





























}