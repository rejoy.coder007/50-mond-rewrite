package com.media.vidplayer2.mond.ab_home_screen.aa_tabs.ac_folderview.ab_bottomsheet;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;


import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.media.vidplayer2.mond.R;
import com.media.vidplayer2.mond.zb_Config.Config;

import com.media.vidplayer2.mond.zf_space_share.uploadSpace.UploadSpaceActivity;

import java.util.ArrayList;

public class BottomSheet extends BottomSheetDialog {

    private Context context;

    private String  path;
    private String  name;

    public BottomSheet(Context context,String path,String name){
        super(context);
        this.path =path;
        this.name =name;
        this.context = context;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = getLayoutInflater().inflate(R.layout.ag_bottom_sheet, null);
        setContentView(view);

        ArrayList<Item> items=new ArrayList<>();
        items.add( new Item(R.drawable.aa_crown, "Send to Space Share") );
        items.add( new Item(R.drawable.aa_crown, "Video Picker") );
        items.add( new Item(R.drawable.aa_crown, "Drag and Drop") );
        items.add(new Item(R.drawable.aa_crown, "Lock"));
        items.add( new Item(R.drawable.aa_crown, "Rename") );


        ItemAdapter adapter = new ItemAdapter( this.context, items );

        //ListView for the items
       ListView listView = (ListView) view.findViewById(R.id.list_items);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

               // Intent it = null;

                switch(position){
                    case 0:

                          if(Config.DUPLI_TEST)
                          {
                              Log.d("SPACE_SHARE", "onItemClick: ");
                          }

                        Intent i = new Intent(context, UploadSpaceActivity.class);
                        i.putExtra("path", path);
                        i.putExtra("name", name);
                        context.startActivity(i);

                        break;
                    case 1:

                        break;
                    case 2:

                        break;
                    case 3:

                        break;
                    case 4:

                        break;
                    case 5:

                        break;
                    case 6:

                        break;
                }

              //  context.startActivity(it);
            }
        });

        //GridView for the items
        /*GridView gridView = (GridView) view.findViewById(R.id.grid_items);
        gridView.setAdapter( adapter );
        gridView.setNumColumns(2);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent it = null;

                switch(position){
                    case 0:
                        it = new Intent(Intent.ACTION_VIEW);
                        it.setData(Uri.parse("http://www.whatsapp.com"));
                        break;
                    case 1:
                        it = new Intent(Intent.ACTION_VIEW);
                        it.setData(Uri.parse("http://www.facebook.com"));
                        break;
                    case 2:
                        it = new Intent(Intent.ACTION_VIEW);
                        it.setData(Uri.parse("http://plus.google.com"));
                        break;
                    case 3:
                        it = new Intent(Intent.ACTION_VIEW);
                        it.setData(Uri.parse("http://www.twitter.com"));
                        break;
                    case 4:
                        it = new Intent(Intent.ACTION_VIEW);
                        it.setData(Uri.parse("http://www.youtube.com"));
                        break;
                    case 5:
                        it = new Intent(Intent.ACTION_VIEW);
                        it.setData(Uri.parse("http://www.instagram.com"));
                        break;
                    case 6:
                        it = new Intent(Intent.ACTION_VIEW);
                        it.setData(Uri.parse("http://www.stackoverflow.com"));
                        break;
                }

                context.startActivity(it);
            }
        });*/

    }
}
