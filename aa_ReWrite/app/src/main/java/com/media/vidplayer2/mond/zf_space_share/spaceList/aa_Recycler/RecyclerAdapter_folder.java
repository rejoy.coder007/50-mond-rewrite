package com.media.vidplayer2.mond.zf_space_share.spaceList.aa_Recycler;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.request.RequestOptions;
import com.google.api.services.drive.model.About;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.media.vidplayer2.mond.R;


import com.media.vidplayer2.mond.zb_Config.Config;
import com.media.vidplayer2.mond.zf_space_share.spaceList.ListSpaceShareActivity;
import com.google.api.services.drive.model.File;
import com.media.vidplayer2.mond.zf_space_share.uploadSpace.GoogleDriveFileHolder;


import java.util.ArrayList;
import java.util.List;

import com.media.vidplayer2.mond.zc_Glide.FutureStudioAppGlideModule;

public class RecyclerAdapter_folder extends RecyclerView.Adapter<ViewHolder_folder>
{

    private List<GoogleDriveFileHolder> directories =new ArrayList<>();
    private Context context;
    private ListSpaceShareActivity listSpaceShareActivity;



    private FirebaseUser current_user=null;



    public RecyclerAdapter_folder(List<GoogleDriveFileHolder> directories, Context context)
    {
        this.directories = directories;
        this.context = context;

        listSpaceShareActivity =   ((ListSpaceShareActivity) context);
    }

    public void setRecyclerAdapter(List<GoogleDriveFileHolder> directories, Context context)
    {
        this.directories = directories;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder_folder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {


        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.ai_acvity_folder_list_drive_slice_recycler_item, viewGroup, false);


        return new ViewHolder_folder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder_folder viewHolderFolder, final int i)
    {






        GoogleDriveFileHolder googleDriveFileHolder = listSpaceShareActivity.mDriveServiceHelper.list_of_files.get(i);


        File file  = googleDriveFileHolder.getFile();
        About about = googleDriveFileHolder.getAbout();

        if(i>0)
        {
            if(Config.DUPLI_TEST)
            {
                // Log.d("RECYCLER_CONTENT_1", directory.file_path+"::"+viewHolderFolder.file_name.getText()+"::"+string_path+"::"+directories.size());
            }

            //viewHolderFolder.file_name.setText( string_path.substring(0, string_path.lastIndexOf(".")));
            viewHolderFolder.file_name.setText( i+"_"+ file.getName());




            long interval = 1 * 200;

            RequestOptions options = new RequestOptions();
            options.fitCenter();


            FutureStudioAppGlideModule futureStudioAppGlideModule = new FutureStudioAppGlideModule();



            //  futureStudioAppGlideModule.add_image_to_view(context,viewHolderFolder,Uri.fromFile( new File( directory.file_path ) ));


            futureStudioAppGlideModule.showThumbnail(context,viewHolderFolder.thumbnail, file.getWebContentLink());

            viewHolderFolder.slice.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {

                    current_user=FirebaseAuth.getInstance().getCurrentUser();

                    if(current_user==null)
                    {


                    }
                    else
                    {
                        listSpaceShareActivity.mDriveServiceHelper.dialog.SetBottomSheet(file);
                        listSpaceShareActivity.mDriveServiceHelper.dialog .show();
                    }









                    return true;
                }
            });


        }
        else
        {

            double size=0;
            for(  int j=1;j<directories.size();j++)
            {

                size =size+directories.get(j).getFile().getSize();
            }

               double perc = Config.roundFour((double)about.getStorageQuota().getUsage()/(double)about.getStorageQuota().getLimit());
            double percM = Config.roundFour((double)size/(double)about.getStorageQuota().getLimit());
              String quota = "Mond Usage - "+ Config.roundFour(Config.toMB(size))+"MB ("+percM+"%"+")\n"+"Drive Limit - "+ Config.roundFour(Config.toMB(about.getStorageQuota().getLimit()))+"MB(100%)\n"+"Drive Usage -"+ Config.roundFour(Config.toMB(about.getStorageQuota().getUsage()))+"MB ("+perc+"%"+")";
              viewHolderFolder.file_name.setText(quota);
              viewHolderFolder.thumbnail.setVisibility(View.GONE);
        }







    }


    public static class VideoPlayerThread extends Thread {
        @Override
        public void run()
        {

            if(Config.TEST)
                Log.d("_####_CALL_VIDEO", " Inside VideoPlayerThread Thread :  aa_activity_StartUp A");





            if(Config.TEST)
                Log.d("_###_REDIR_FIRST_SCREEN", "Video_redirect()B1 : aa_activity_StartUp");








            if(Config.TEST)
                Log.d("_###_REDIR_FIRST_SCREEN", "Video_redirect()B2 : aa_activity_StartUp");


            ///////////////////////////////////////////////////////////////////////////////////////////



            if(Config.TEST)
                Log.d("_####_CALL_VIDEO", " Inside VideoPlayerThread Thread :  aa_activity_StartUp B");


        }

    }


    @Override
    public int getItemCount()
    {



        return directories.size();
    }
}
