package com.media.vidplayer2.mond.ab_home_screen.aa_tabs.ab_favorites;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.media.vidplayer2.mond.R;
import com.media.vidplayer2.mond.ab_home_screen.HomeActivity;
import com.media.vidplayer2.mond.ab_home_screen.aa_tabs.ab_favorites.aa_Recycler.RecyclerAdapterFav;
import com.media.vidplayer2.mond.zb_Config.Config;
import com.media.vidplayer2.mond.zc_VideoFileParser.VideoFileParser;


public class FavFragment extends Fragment
{
    public static final String                          ARG_PAGE = "ARG_PAGE";
    private int                                                         mPage;
    HomeActivity                                           homeActivity =null;

    public FavFragment()
    {

    }


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt(ARG_PAGE);
        homeActivity = ((HomeActivity) getActivity());

        if (Config.TEST)
        {
            Log.d("_AFRAG_FV_CRE_", "OnCreate Fav fragment HomeActivity");

        }


    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View rootView = null;

        homeActivity.fileManager_homeActivity.favFileManager.FAV_FRAG_READY_TO_UPDATE =false;
       // homeActivity.fileManager_homeActivity.DummyFAVFolder = new ArrayList<DirectoryFav>();

        rootView = inflater.inflate(R.layout.ac_b_fragment_favourite, container, false);

        homeActivity.fileManager_homeActivity.favFileManager.Fav_Frag_recyclerView =  rootView.findViewById(R.id.contact_recycleView);
        homeActivity.fileManager_homeActivity.favFileManager.Fav_Frag_recyclerView.setHasFixedSize(true);
        homeActivity.fileManager_homeActivity.favFileManager.Fav_Frag_recyclerAdapter = new RecyclerAdapterFav(VideoFileParser.getInstance().total_SD_MEM, getContext());


        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        homeActivity.fileManager_homeActivity.favFileManager.Fav_Frag_recyclerView.setLayoutManager(linearLayoutManager);
        homeActivity.fileManager_homeActivity.favFileManager.Fav_Frag_recyclerView.setAdapter( homeActivity.fileManager_homeActivity.favFileManager.Fav_Frag_recyclerAdapter);




        // SwipeRefreshLayout
        homeActivity.fileManager_homeActivity.favFileManager.Fav_Frag_mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
        homeActivity.fileManager_homeActivity.favFileManager.Fav_Frag_mSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        //   StartThread_dir_update(false);




        homeActivity.fileManager_homeActivity.favFileManager.Fav_Frag_mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh()
            {
                // Your recyclerview reload logic function will be here!!!

                if (Config.TEST)
                {
                    Log.d("_AFRAG_FV_RFRSH", "onRefresh fav fragment  : homeActivity ");

                }

               // Config.FIRST_TIME_RUN=false;



               // homeActivity.fileManager_homeActivity.ReadDirFromHomeOnceVisitedOrFirstTimeVisited();

                homeActivity.fileManager_homeActivity.favFileManager.Fav_Frag_mSwipeRefreshLayout.setRefreshing(false);


            }
        });


        if (Config.TEST)
        {
            Log.d("_AFRAG_FV_VIEW_", " onCreateView fav fragment  : aa_activity_home_screen");

        }









        return rootView;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {


        //Only once needed
        homeActivity.fileManager_homeActivity.favFileManager.FAV_FRAG_READY_TO_UPDATE =true;

         if(Config.TEST)
            Log.d("_AFRAG_FV_VIEW_CTED", " After Fragment fav View Creation --  homeActivity " );



    }




    @Override
    public void onStop() {

        if(Config.TEST)
        {
            Log.d("_AFRAG_FV_STP_", "Stop executed from fv folder -- home activity");

        }


      super.onStop();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if(Config.TEST)
        {
            Log.d("_AFRAG_FV_DEST_", "Destroy executed from fv folder -- home activity");

        }



        homeActivity.fileManager_homeActivity.favFileManager.Fav_Frag_recyclerView =null;
        homeActivity.fileManager_homeActivity.favFileManager.Fav_Frag_recyclerAdapter =null;
        homeActivity.fileManager_homeActivity.favFileManager.Fav_Frag_mSwipeRefreshLayout =null;



    }





}
