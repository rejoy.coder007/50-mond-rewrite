package com.media.vidplayer2.mond.zb_a_chat.FireStoreChat;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NavUtils;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.MetadataChanges;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.WriteBatch;
import com.media.vidplayer2.mond.R;
import com.media.vidplayer2.mond.ad_Profile.FileManagerProfile;
import com.media.vidplayer2.mond.ad_Profile.MyProfileActivity;
import com.media.vidplayer2.mond.ad_Profile.statusActvity;
import com.media.vidplayer2.mond.zb_Config.Config;
import com.media.vidplayer2.mond.zb_a_chat.GetTimeAgo;
import com.media.vidplayer2.mond.zb_b_spaceshare_file_selector.SpaceShareChatUploadActivity;
import com.media.vidplayer2.mond.zb_b_spaceshare_file_selector.Test.MainActivity;
import com.media.vidplayer2.mond.zc_Glide.GlideApp;
import com.media.vidplayer2.mond.zf_space_share.downloadSpace.FileManagerDownloadSpace;


import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class FirestoreChatActivity extends AppCompatActivity
        implements FirebaseAuth.AuthStateListener {
    private static final String TAG = "FirestoreChatActivity";

public int GET_FILE_FOR_SHARE_IN_CHAT = 1;




    private CollectionReference SenderChatCollection;
    private   CollectionReference ReceiverChatCollection;


    private DocumentReference SenderChatCollectionD;
    private   DocumentReference ReceiverChatCollectionD;



    Query SenderChatQuery;
    Query ReceiverChatQuery;
    /** Get the last 50 chat messages ordered by timestamp . */

    /*
    private     Query SenderChatQuery =
            SenderChatCollection.orderBy("timestamp", Query.Direction.DESCENDING).limit(50);

            */

    static {
        FirebaseFirestore.setLoggingEnabled(true);
    }

    RecyclerView mRecyclerView;
    Button mSendButton ;


    //   @BindView(R.id.messageEdit)

    EditText mMessageEdit ;


    TextView mEmptyListMessage  ;




    String other_user_id;
    String current_id;
    String other_user_name;

    String current_name ;
    String current_image ;
    String other_user_image;


    public String current_email;
    public String other_user_email;

    /*



           mChatToolbar = (Toolbar) findViewById(R.id.chat_app_bar);
     */


      TextView mTitleView;


      TextView mLastSeenView;


      ImageView mProfileImage;

    @BindView(R.id.chat_app_bar)
      Toolbar mChatToolbar;





    private FirebaseAuth mAuth;

    private FirebaseFirestore mFirestore;
    ListenerRegistration UsersList_registration_CurrentList;


    @OnClick(R.id.chat_add_btn_upload)
    public void chat_add_btn_upload(View view)
    {



      //  Intent status_intent = new Intent(FirestoreChatActivity.this, SpaceShareChatUploadActivity.class);
      //  status_intent.putExtra("status_value", status_value);
        //   Log.d("#STOP_DET", "inside status Button ::" +CLOSING);
     ///   startActivity(status_intent);

        Intent i = new Intent(FirestoreChatActivity.this, SpaceShareChatUploadActivity.class);
        i.putExtra("current_email", current_email);
        i.putExtra("other_user_email", other_user_email);
        startActivityForResult(i, GET_FILE_FOR_SHARE_IN_CHAT);


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GET_FILE_FOR_SHARE_IN_CHAT) {
            if (resultCode == RESULT_OK) {

              ;
                String image = data.getStringExtra("image");
                long fileSize = data.getLongExtra("fileSize", 0);

                if(Config.DUPLI_TEST)
                    Log.d("DOWNLOAD_TEST", "size: "+fileSize);
                String filename = data.getStringExtra("filename");
                onSendClickI(image,fileSize,filename);
            }
        }
    }

    @Override
    public void onBackPressed() {

        HashMap<String, Object> FrduserMap_1 = new HashMap<>();
        HashMap<String, Object> FrduserMap_2 = new HashMap<>();



        FrduserMap_1.put("time_stamp",   FieldValue.serverTimestamp());
        FrduserMap_1.put("time_stampN",   FieldValue.serverTimestamp());

        FrduserMap_2.put("time_stamp",   FieldValue.serverTimestamp());

       Boolean updated_file=true;
        WriteBatch batch =  FirebaseFirestore.getInstance().batch();

       DocumentReference FriendListReference_1  =  FirebaseFirestore.getInstance().collection("Friends").document(current_id).collection("MyFriends").document(other_user_id);

       DocumentReference FriendListReference_2  =  FirebaseFirestore.getInstance().collection("Friends").document(other_user_id).collection("MyFriends").document(current_id);


        batch.update(FriendListReference_2, FrduserMap_2);
        batch.update(FriendListReference_1, FrduserMap_1);
        batch.commit();

/*
        batch.commit().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {



            }


        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {





            }
        });
*/








        super.onBackPressed();
        NavUtils.navigateUpFromSameTask(this);








    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.an_a_activity_chat);
        ButterKnife.bind(this);

        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setReverseLayout(true);
        manager.setStackFromEnd(true);




        setSupportActionBar(mChatToolbar);

        ActionBar actionBar = getSupportActionBar();

        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);

        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View action_bar_view = inflater.inflate(R.layout.an_d_chat_custom_bar, null);
        actionBar.setCustomView(action_bar_view);


        mTitleView = (TextView) action_bar_view.findViewById(R.id.custom_bar_title);
        mLastSeenView = (TextView) action_bar_view.findViewById(R.id.custom_bar_seen);
        mProfileImage = (ImageView) action_bar_view.findViewById(R.id.custom_bar_image);
///////////////////////////////////////////////////////////////////////////////////////////////
        other_user_id = getIntent().getStringExtra("other_user_id");
        other_user_name = getIntent().getStringExtra("other_user_name");
        other_user_image = getIntent().getStringExtra("other_user_image");
        other_user_email = getIntent().getStringExtra("other_user_email");



        current_id = getIntent().getStringExtra("current_id");
        current_name = getIntent().getStringExtra("current_name");
        current_image = getIntent().getStringExtra("current_image");
        current_email = getIntent().getStringExtra("current_email");


        if(Config.DUPLI_TEST)
            Log.d("CHAT_CASE", current_id+" : "+current_name+":"+current_image+":"+current_email);



        mAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();



        SenderChatCollection =
                FirebaseFirestore.getInstance().collection("Chats").document(current_id).collection(other_user_id);

        ReceiverChatCollection =
                FirebaseFirestore.getInstance().collection("Chats").document(other_user_id).collection(current_id);




        SenderChatQuery =
                SenderChatCollection.orderBy("timestamp", Query.Direction.DESCENDING);
        ReceiverChatQuery =
                SenderChatCollection.orderBy("timestamp", Query.Direction.DESCENDING);



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        //  @BindView(R.id.messagesList)
        mRecyclerView = (RecyclerView)findViewById(R.id.messagesList);


        RecyclerView.ItemAnimator animator = mRecyclerView.getItemAnimator();
        if (animator instanceof SimpleItemAnimator) {
            ((SimpleItemAnimator) animator).setSupportsChangeAnimations(false);
        }

        // @BindView(R.id.sendButton)
        mSendButton = (Button)findViewById(R.id.sendButton);


        //   @BindView(R.id.messageEdit)

        mMessageEdit = (EditText)findViewById(R.id.messageEdit);


        //mEmptyListMessage = (TextView)findViewById(R.id.emptyTextView);


        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(manager);

        mRecyclerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View view, int left, int top, int right, int bottom,
                                       int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if (bottom < oldBottom) {
                    mRecyclerView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mRecyclerView.smoothScrollToPosition(0);
                        }
                    }, 100);
                }
            }
        });



        mMessageEdit.setClickable(true);
        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSendClick();
            }
        });

        //  mMessageEdit.callOnClick();


        Map<String, Object> UserStat = new HashMap<>();
        UserStat.put("seen", true);

        //  if(zz_Config.CHAT_TEST)
        //   Log.d("CHAT_ACTIVITY", "onCreate: "+mCurrentUserId+"::"+other_user_id);

        mFirestore.collection("Chat").document(current_id).collection("MyFriends").document(other_user_id).set(UserStat);



    }

    @Override
    public void onStart() {
        super.onStart();

        UsersList_registration_CurrentList = mFirestore.collection("UsersList").document(other_user_id).addSnapshotListener(MetadataChanges.INCLUDE, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                @Nullable FirebaseFirestoreException e) {


                if (e != null) {
                    System.err.println("Listen failed:" + e);
                    return;
                }



                Boolean online = documentSnapshot.getBoolean("online");
                Timestamp time_stamp = documentSnapshot.getTimestamp("time_stamp");
                String image = documentSnapshot.getString("thumb_image");
                String name = documentSnapshot.getString("name");
                mTitleView.setText(name);

                String lastSeenTime="";


                if(online) {

                    mLastSeenView.setText("Online");


                    if(Config.CHAT_TEST)
                    {
                        Log.d("ONLINE_TEST", "1"+time_stamp+"::"+time_stamp.toDate().getTime()+"::"+lastSeenTime);
                    }



                } else {

                    GetTimeAgo getTimeAgo = new GetTimeAgo();







                    lastSeenTime = getTimeAgo.getTimeAgo(time_stamp.toDate().getTime(), FirestoreChatActivity.this);

                    mLastSeenView.setText(lastSeenTime);


                    if(Config.CHAT_TEST)
                    {
                        Log.d("ONLINE_TEST", "2"+time_stamp+"::"+time_stamp.toDate().getTime()+"::"+lastSeenTime);
                    }


                }




                if(!image.equals("default"))
                {


                    GlideApp.with(FirestoreChatActivity.this).load(image)
                            .into(mProfileImage);
                    /*
                    Picasso.get().load(image).networkPolicy(NetworkPolicy.OFFLINE).placeholder(R.drawable.default_avatar).into(mProfileImage, new Callback()
                    {

                        @Override
                        public void onSuccess() {

                            Log.d("#CONFIG_1", "image uploaded");
                            //  Toast.makeText(CloudSettingsActivity.this, "image uploaded", Toast.LENGTH_SHORT).show();


                        }

                        @Override
                        public void onError(Exception e) {

                            Picasso.get().load(image).placeholder(R.drawable.default_avatar).into(mProfileImage);


                        }
                    });
*/

                }


                // ...
            }
        });


        if (isSignedIn()) { attachRecyclerViewAdapter(); }
        FirebaseAuth.getInstance().addAuthStateListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        FirebaseAuth.getInstance().removeAuthStateListener(this);
        UsersList_registration_CurrentList.remove();

        mRecyclerView.setAdapter(null);
    }

    @Override
    public void onAuthStateChanged(@NonNull FirebaseAuth auth) {
        mSendButton.setEnabled(isSignedIn());
        mMessageEdit.setEnabled(isSignedIn());

        if (isSignedIn()) {
            attachRecyclerViewAdapter();
        } else {
            Toast.makeText(this, R.string.signing_in, Toast.LENGTH_SHORT).show();
            //  auth.signInAnonymously().addOnCompleteListener(new SignInResultNotifier(this));
        }
    }

    private boolean isSignedIn() {
        return FirebaseAuth.getInstance().getCurrentUser() != null;
    }

    private void attachRecyclerViewAdapter() {
        final RecyclerView.Adapter adapter = newAdapter();

        // Scroll to bottom on new messages
        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                mRecyclerView.smoothScrollToPosition(0);
            }
        });

        mRecyclerView.setAdapter(adapter);
    }


     /*


                    Intent profileintent = new Intent(spaceShareChatUploadActivity, SpaceShareChatUploadActivity.class);
                  //  profileintent.putExtra("otherprofile", true);
                  //  profileintent.putExtra("other_user_id", holder.other_user_id);



                    spaceShareChatUploadActivity.startActivity(profileintent);
      */



    public void onSendClick() {
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        String name = current_name;

        onAddMessage(new Chat(name, mMessageEdit.getText().toString(), uid, FieldValue.serverTimestamp().toString()));

        mMessageEdit.setText("");
    }

    public void onSendClickI(String Image,long fileSize,String filename) {
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        String name = current_name;
        mMessageEdit.setText(filename);
        onAddMessage(new Chat(name, mMessageEdit.getText().toString(), uid, FieldValue.serverTimestamp().toString(),Image,fileSize,filename));


    }

    @NonNull
    private RecyclerView.Adapter newAdapter() {
        FirestoreRecyclerOptions<Chat> options =
                new FirestoreRecyclerOptions.Builder<Chat>()
                        .setQuery(SenderChatQuery, Chat.class)
                        .setLifecycleOwner(this)
                        .build();

        return new FirestoreRecyclerAdapter<Chat, ChatHolder>(options) {
            @NonNull
            @Override
            public ChatHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


                ChatHolder chatHolder = new ChatHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.an_b_message, parent, false));
                chatHolder.firestoreChatActivity=FirestoreChatActivity.this;

                return chatHolder;
            }

            @Override
            protected void onBindViewHolder(@NonNull ChatHolder holder, int position, @NonNull Chat model) {
                holder.bind(model);
               // holder.firestoreChatActivity=FirestoreChatActivity.this;


            }

            @Override
            public void onDataChanged() {
                // If there are no chat messages, show a view that invites the user to add a an_b_message.
                //  mEmptyListMessage.setVisibility(getItemCount() == 0 ? View.VISIBLE : View.GONE);
            }
        };
    }


    private void onAddMessage(@NonNull Chat chat) {

        /*
        SenderChatCollection.add(chat).addOnFailureListener(this, new OnFailureListener() {


            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e(TAG, "Failed to write an_b_message", e);
            }
        });*/


        /*
        SenderChatCollection.add(chat).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
            @Override
            public void onSuccess(DocumentReference documentReference) {

                ReceiverChatCollection.add(chat).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {

                    }
                });


            }
        });
       */


        WriteBatch batch =  FirebaseFirestore.getInstance().batch();



        if(Config.CHAT_TEST)
            Log.d("SEND_MSG", "sendMessage:  1");

        Map<String, Object> chat_status_1 = new HashMap<>();
        chat_status_1.put("an_b_message",chat.getMessage());
        chat_status_1.put("name",chat.getName());
        chat_status_1.put("mTimestamp",   FieldValue.serverTimestamp());
        chat_status_1.put("uid",chat.getUid());
        chat_status_1.put("TimeStampStr",chat.getTimeStampStr());



        //batch.set(SenderChatCollectionD, chat_status_1);
        //  batch.set(ReceiverChatCollectionD, chat_status_1);


        batch.set(SenderChatCollection.document(), chat);
        batch.set(ReceiverChatCollection.document(), chat);

        batch.commit().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

                mMessageEdit.setText("");


                Toast.makeText(FirestoreChatActivity.this, "Your Message sent", Toast.LENGTH_SHORT).show();

                if(Config.CHAT_TEST)
                    Log.d("SEND_MSG", "sendMessage from batch:  ");


            }


        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {



                if(Config.CHAT_TEST)
                    Log.d("SEND_MSG", "sendMessage from batch :: "+ e.getMessage());

            }
        });





    }
}
