package com.media.vidplayer2.mond.ab_home_screen;


import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.viewpager.widget.ViewPager;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.common.api.Scope;
import com.google.android.material.snackbar.Snackbar;
import com.google.api.services.drive.DriveScopes;
import com.google.firebase.auth.FirebaseAuth;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.media.vidplayer2.mond.R;
import com.media.vidplayer2.mond.ab_home_screen.aa_tabs.aa_ChatFragments.FriendListFragmentF;
import com.media.vidplayer2.mond.ab_home_screen.aa_tabs.aa_home_screen_PagerAdapter;
import com.media.vidplayer2.mond.ab_home_screen.aa_tabs.ab_favorites.FavFileManager;
import com.media.vidplayer2.mond.ab_home_screen.aa_tabs.ac_folderview.FolderFileManager;

import com.media.vidplayer2.mond.ac_Selector_Reg_or_Login.NewLogin.LoginActivity;
import com.media.vidplayer2.mond.ad_Profile.MyProfileActivity;
import com.media.vidplayer2.mond.zb_Config.Config;
import com.media.vidplayer2.mond.zf_space_share.spaceList.ListSpaceShareActivity;
import com.media.vidplayer2.mond.zg_contactlist.ContactActivity;
import com.media.vidplayer2.mond.zh_friendlist.FriendListActvity;
import com.media.vidplayer2.mond.zz_TEST.TestCase;

import java.util.List;


public class FileManager_HomeActivity
{



    public final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 20;
    aa_home_screen_PagerAdapter aa_home_screen_pagerAdapter = null;
    Context context = null;

    public FavFileManager favFileManager=null;
    public FolderFileManager folderFileManager=null;

    public Thread Thread_Full_FOLDER =null;
    Handler directory_handler_folder =null;

    HomeActivity homeActivity;

    public FileManager_HomeActivity(Context context)
    {

        this.context = context;
        aa_home_screen_pagerAdapter = new aa_home_screen_PagerAdapter(((HomeActivity) context).getSupportFragmentManager(),
                context);

        homeActivity=(HomeActivity)this.context;



        favFileManager = new FavFileManager(context);
        folderFileManager = new FolderFileManager(context);


    }


    private void openSettingsDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(homeActivity);
        builder.setTitle("Required Permissions");
        builder.setMessage("This app require permission to use awesome feature. Grant them in app settings.");
        builder.setPositiveButton("Take Me To SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", homeActivity.getPackageName(), null);
                intent.setData(uri);
                homeActivity.startActivityForResult(intent, 101);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    private void  requestMultiplePermissions(){



        Dexter.withActivity(homeActivity)
                .withPermissions(
                       // Manifest.permission.READ_CONTACTS,
                      //  Manifest.permission.RECORD_AUDIO,
                        Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted())
                        {
                            Toast.makeText(homeActivity.getApplicationContext(), "Permission Granted", Toast.LENGTH_SHORT).show();

                            if(Config.FIRST_TIME_RUN) {
                                favFileManager.Fav_Frag_ReadDirByThread();
                                setFirstTime_permission();
                            }


                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            openSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(homeActivity.getApplicationContext(), "Some Error! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();





    }


    public void getPermission()
    {


        requestMultiplePermissions();

/*
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {



            if (Config.TEST)
            {
                Log.d("_HMyAppRESUME_PERRAE", "gotPermission --getPermission : HomeActivity -- A");
            }


             if(Config.FIRST_TIME_RUN)
                favFileManager.Fav_Frag_ReadDirByThread();

            if (Config.TEST) {
                Log.d("_HMyAppRESUME_PERRAE", "gotPermission --getPermission : HomeActivity -- B");
            }

        }
        else
        {


            if (ActivityCompat.shouldShowRequestPermissionRationale((HomeActivity) context, Manifest.permission.READ_EXTERNAL_STORAGE)) {


                permission_snack_message();
                if (Config.TEST) {
                    Log.d("_#NO_PERMISSION_PREV", "shouldShowRequestPermissionRationale --getPermission : aa_activity_home_scree");
                }

            }
            else
            {

                if (Config.TEST) {
                    Log.d("_#NO_PERMISSION_FIRST", "noPermission --getPermission : HomeActivity -- A");
                }

                ActivityCompat.requestPermissions((HomeActivity) context,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_SETTINGS},
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

                if (Config.TEST) {
                    Log.d("_#NO_PERMISSION_FIRST", "noPermission --getPermission : HomeActivity -- B");
                }


            }

        }
*/

    }

    public void permission_snack_message()
    {


        Snackbar.make(((HomeActivity) context).findViewById(android.R.id.content),
                "Needs permission to read  video",
                Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        ActivityCompat.requestPermissions((HomeActivity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

                        if (Config.TEST) {
                            Log.d("_#SNACK_BAR", "permission_snack_message : HomeActivity");
                        }

                    }
                }).show();
    }

    private void setFirstTime_permission() {


        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        // first time
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("Permission", true);
        editor.commit();
        boolean ranBefore = preferences.getBoolean("Permission", false);

        if (Config.TEST)
            Log.d("_#FIRST_FLAG", "isFirstTime shared pref : HomeActivity " + ranBefore);

    }


    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        /*

        switch (requestCode) {


            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:


                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //                    grantResult[0] means it will check for the first postion permission which is READ_EXTERNAL_STORAGE
                    //                    grantResult[1] means it will check for the Second postion permission which is CAMERA
                    Toast.makeText(context, "Permission Granted", Toast.LENGTH_SHORT).show();

                    if (Config.TEST) {
                        Log.d("_HMyAppRESUME_PERRE", "Permission by req : HomeActivity -- A");
                    }




                    setFirstTime_permission();
                   // ReadDirFromHomeOnceVisitedOrFirstTimeVisited();

                    if (Config.TEST) {
                        Log.d("_HMyAppRESUME_PERRE", "Permission by req : HomeActivity -- B");
                    }


                } else {
                    permission_snack_message();

                }

             break;





            case MY_PERMISSIONS_REQUEST_SEND_SMS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.



                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

        }

        */


    }




    public void window_toolbar_navbar_bg() {
        //
        Window window = ((HomeActivity) context).getWindow();
        Drawable background = ((HomeActivity) context).getResources().getDrawable(R.drawable.ad_toolbar_bg);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(((HomeActivity) context).getResources().getColor(android.R.color.transparent));
        window.setNavigationBarColor(((HomeActivity) context).getResources().getColor(android.R.color.transparent));
        window.setBackgroundDrawable(background);

        ((HomeActivity) context).setSupportActionBar(((HomeActivity) context).toolbar);
        ((HomeActivity) context).getSupportActionBar().setDisplayShowTitleEnabled(false);


    }


    public void enable_tab() {

        if (Config.TEST)
            Log.d("_A#ENABLE_TAB", "enable_tab  : HomeActivity A");

        ((HomeActivity) context).viewPager.setAdapter(aa_home_screen_pagerAdapter);
        ((HomeActivity) context).tabLayout.setupWithViewPager(((HomeActivity) context).viewPager);



        ((HomeActivity) context).viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {


                switch (position) {

                    case 0:

                        Config.SEL_TAB =position;
                        Config.HIDE_TAB=Config.HIDE_TAB;

                        if(FirebaseAuth.getInstance().getCurrentUser()==null)
                        {

                            GoToLoginActivity();

                        }
                        else
                         {
                            if (!GoogleSignIn.hasPermissions(
                                    GoogleSignIn.getLastSignedInAccount(homeActivity), new Scope(DriveScopes.DRIVE_FILE))) {
                                GoToLoginActivity();
                            }
                            else {

                                if(Config.DUPLI_TEST)
                                    Log.d("_ATAB_SEL_A", "1 ----"+Config.HIDE_TAB+"::"+Config.SEL_TAB );

/*
                                if(homeActivity.start_list)
                                {
                                    FriendListFragmentF friendListFragmentF =(FriendListFragmentF)aa_home_screen_PagerAdapter.registeredFragments.get(0);

                                    if(friendListFragmentF!=null)
                                    friendListFragmentF.setUpRecyclerView();
                                    homeActivity.start_list=false;

                                }
                                */

                                }
                        }
                        if(Config.DUPLI_TEST)
                            Log.d("_ATAB_SEL_A", "2"+Config.HIDE_TAB+"::"+Config.SEL_TAB );




                        break;

                    case 1:

                         Config.SEL_TAB =position;
                         Config.HIDE_TAB=2;



                        if(Config.DUPLI_TEST)
                            Log.d("_ATAB_SEL_A", "3"+Config.HIDE_TAB+"::"+Config.SEL_TAB );

                        FriendListFragmentF friendListFragmentF =(FriendListFragmentF)aa_home_screen_PagerAdapter.registeredFragments.get(0);

                     //   friendListFragmentF.stopFlist();


                        // homeActivity.friendListFileManagerF.stopFlist();
                        break;
                    case 2:
                        Config.SEL_TAB =position;
                        Config.HIDE_TAB=1;
                        if(Config.DUPLI_TEST)
                            Log.d("_ATAB_SEL_A", Config.HIDE_TAB+"::"+Config.SEL_TAB );
                      //  FriendListFragmentF friendListFragmentF1 =(FriendListFragmentF)aa_home_screen_PagerAdapter.registeredFragments.get(0);

                        //friendListFragmentF1.stopFlist();

                        break;


                }


                if (Config.TEST)
                    Log.d("_A#ENABLE_TAB", "enable_tab : HomeActivity B" + position);


            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        if (Config.TEST)
            Log.d("_A#ENABLE_TAB", "enable_tab : HomeActivity B");

        for (int i = 0; i < ((HomeActivity) context).tabLayout.getTabCount(); i++) {


            ViewGroup slidingTabStrip = (ViewGroup) ((HomeActivity) context).tabLayout.getChildAt(0);
            View tab = slidingTabStrip.getChildAt(i);

            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) tab.getLayoutParams();

            switch (i) {
                case 0:
                    layoutParams.weight = 1;
                    break;
                case 1:
                    layoutParams.weight = 1;
                    break;

                default:
                    layoutParams.weight = 0;
                    break;
            }


            tab.setLayoutParams(layoutParams);

        }


    }

    public void disable_tab(int hide_tab_no, int sel_tab_no)
    {

        View tab;
        LinearLayout.LayoutParams layoutParams;

        ViewGroup slidingTabStrip = (ViewGroup) ((HomeActivity) context).tabLayout.getChildAt(0);



        for(int i=0;i<3;i++)
        {
            if(hide_tab_no==i)
            {


                tab = slidingTabStrip.getChildAt(hide_tab_no);
                layoutParams = (LinearLayout.LayoutParams) tab.getLayoutParams();
                layoutParams.weight = 0;
                tab.setLayoutParams(layoutParams);
            }
            else{


                tab = slidingTabStrip.getChildAt(i);
                layoutParams = (LinearLayout.LayoutParams) tab.getLayoutParams();
                layoutParams.weight = 1;
                tab.setLayoutParams(layoutParams);

            }
        }






        ((HomeActivity) context).viewPager.setCurrentItem(sel_tab_no);

    }





    public void GoToLoginActivity(){

         Intent intent = new Intent((HomeActivity)context, LoginActivity.class);
         ((HomeActivity)context).startActivity(intent);



    }

    public void GoToSpaceShareListActivity(){

        Intent intent = new Intent((HomeActivity)context, ListSpaceShareActivity.class);
        ((HomeActivity)context).startActivity(intent);



    }

    public void SendtoSettingsActivity(){

        Intent intent = new Intent((HomeActivity)context, MyProfileActivity.class);
        ((HomeActivity)context).startActivity(intent);



    }

    public void SendtoFriendlistActivity(){

        if(Config.DUPLI_TEST)
            Log.d("FLIST", "Friend list 1");


      //  Intent intent = new Intent((HomeActivity)context, FriendListsAcitivty.class);
        Intent intent = new Intent((HomeActivity)context, FriendListActvity.class);
        ((HomeActivity)context).startActivity(intent);



    }
    public void SendtoQueueActivity(){

        if(Config.DUPLI_TEST)
            Log.d("FLIST", "Friend list 1");


        //  Intent intent = new Intent((HomeActivity)context, FriendListsAcitivty.class);
        Intent intent = new Intent((HomeActivity)context, TestCase.class);
        ((HomeActivity)context).startActivity(intent);



    }


    public void GoToAddFriendActivity(){

        Intent intent = new Intent((HomeActivity)context, ContactActivity.class);
        ((HomeActivity)context).startActivity(intent);



    }












}


























