package com.media.vidplayer2.mond.ab_home_screen.aa_tabs.ac_folderview;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;


import com.media.vidplayer2.mond.R;
import com.media.vidplayer2.mond.ab_home_screen.HomeActivity;
import com.media.vidplayer2.mond.ab_home_screen.aa_tabs.ac_folderview.aa_Recycler.Directory_folder;
import com.media.vidplayer2.mond.ab_home_screen.aa_tabs.ac_folderview.aa_Recycler.RecyclerAdapter_folder;
import com.media.vidplayer2.mond.zb_Config.Config;
import com.media.vidplayer2.mond.zc_VideoFileParser.VideoFileParser;


/**
 * A simple {@link Fragment} subclass.
 */
public class FolderFragment extends Fragment {


    public static final String                          ARG_PAGE = "ARG_PAGE";
    private int                                                         mPage;
    HomeActivity                                           homeActivity =null;



    public FolderFragment() {
        // Required empty public constructor


    }






    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt(ARG_PAGE);
        homeActivity = ((HomeActivity) getActivity());

        if (Config.TEST)
        {
            Log.d("_AFRAG_FD_CRE_", "OnCreate  Folder fragment HomeActivity");

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {


        View rootView = null;

        homeActivity.fileManager_homeActivity.folderFileManager.FOLDER_FRAG_READY_TO_UPDATE_FOLDER =false;



        //MainActivity_Start.mainActivityStartS._fileManager_mainActivity.total_folder = new ArrayList<Directory_folder>();




        rootView = inflater.inflate(R.layout.ac_d_fragment_folder, container, false);




      //  MainActivity_Start.mainActivityStartS._fileManager_mainActivity.total_folder.clear();

        homeActivity.fileManager_homeActivity.folderFileManager.Folder_Frag_recyclerView =  rootView.findViewById(R.id.contact_recycleView_folder);
        homeActivity.fileManager_homeActivity.folderFileManager.Folder_Frag_recyclerView.setHasFixedSize(true);
        homeActivity.fileManager_homeActivity.folderFileManager.Folder_Frag_recyclerAdapter = new RecyclerAdapter_folder( VideoFileParser.getInstance().total_folder, getContext());


        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        homeActivity.fileManager_homeActivity.folderFileManager.Folder_Frag_recyclerView.setLayoutManager(linearLayoutManager);
        homeActivity.fileManager_homeActivity.folderFileManager.Folder_Frag_recyclerView.setAdapter( homeActivity.fileManager_homeActivity.folderFileManager.Folder_Frag_recyclerAdapter);



        // SwipeRefreshLayout
        homeActivity.fileManager_homeActivity.folderFileManager.Folder_Frag_mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container_folder);
        homeActivity.fileManager_homeActivity.folderFileManager.Folder_Frag_mSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        //   StartThread_dir_update(false);


        homeActivity.fileManager_homeActivity.folderFileManager.Folder_Frag_mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh()
            {
                // Your recyclerview reload logic function will be here!!!

                if (Config.TEST)
                {
                    Log.d("_AFRAG_FD_RFRSH", "onRefresh fav fragment  : homeActivity ");

                }

                homeActivity.fileManager_homeActivity.folderFileManager.Folder_Frag_mSwipeRefreshLayout.setRefreshing(false);


            }
        });


        if (Config.TEST)
        {
            Log.d("_AFRAG_FD_VIEW_", " onCreateView folder fragment  : aa_activity_home_screen");

        }


        if (Config.TEST)
        {
            for (int x = 0; x <  VideoFileParser.getInstance().total_folder.size(); x++)
            {
                Directory_folder directory_folder = VideoFileParser.getInstance().total_folder.get(x);
                Log.d("_ZRECYCLER_CONTENT_F",directory_folder.file_path+":**:"+directory_folder.size+"::"+directory_folder.file_name);

            }

            Log.d("_ZRECYCLER_CONTENT_F", VideoFileParser.getInstance().total_folder.size()+"");


        }





        return rootView;


    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {


        //Only once needed
        homeActivity.fileManager_homeActivity.folderFileManager.FOLDER_FRAG_READY_TO_UPDATE_FOLDER =true;

        if(Config.TEST)
            Log.d("_AFRAG_FD_VIEW_CTED",  ":: "+homeActivity.fileManager_homeActivity.folderFileManager.FOLDER_FRAG_READY_TO_UPDATE_FOLDER+" :: Flag frag created view" );
        //


    }




    @Override
    public void onStop() {

        if(Config.TEST)
        {
            Log.d("_AFRAG_FD_STP_", "Stop executed from frgament folder -- home activity");

        }


        super.onStop();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();



        homeActivity.fileManager_homeActivity.folderFileManager.FOLDER_FRAG_READY_TO_UPDATE_FOLDER =false;

        if(Config.TEST)
        {
            Log.d("_AFRAG_FD_VIEW_DEST_",  ":: "+homeActivity.fileManager_homeActivity.folderFileManager.FOLDER_FRAG_READY_TO_UPDATE_FOLDER+" :: Flag frag view destroyed" );


        }
       // homeActivity.fileManager_homeActivity.Folder_Frag_recyclerView.setNestedScrollingEnabled(false);

        homeActivity.fileManager_homeActivity.folderFileManager.Folder_Frag_recyclerView =null;
        homeActivity.fileManager_homeActivity.folderFileManager.Folder_Frag_recyclerAdapter =null;
        homeActivity.fileManager_homeActivity.folderFileManager.Folder_Frag_mSwipeRefreshLayout =null;




    }










}
