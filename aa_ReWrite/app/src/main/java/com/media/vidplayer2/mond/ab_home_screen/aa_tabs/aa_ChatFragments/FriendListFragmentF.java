package com.media.vidplayer2.mond.ab_home_screen.aa_tabs.aa_ChatFragments;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.media.vidplayer2.mond.R;
import com.media.vidplayer2.mond.ab_home_screen.HomeActivity;
import com.media.vidplayer2.mond.ab_home_screen.aa_tabs.aa_home_screen_PagerAdapter;
import com.media.vidplayer2.mond.zb_Config.Config;

import com.media.vidplayer2.mond.zh_friendlist.ModelFriendlist;

import java.util.ArrayList;


public class FriendListFragmentF extends Fragment {





    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference FriendsList=null;
    public FriendListAdapterXX adapter;
    RecyclerView recyclerView;




    public ArrayList<ListenerRegistration> listenersUsers;
    public ArrayList<ListenerRegistration> listenersNewMessage;


    public FriendListFragmentF()
    {
        // Required empty public constructor
        //listenersUsers = new ArrayList<ListenerRegistration>();
    }


    public static final String                                                ARG_PAGE = "ARG_PAGE";
    private int                                                                               mPage;

    HomeActivity homeActivity;





    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt(ARG_PAGE);


        listenersUsers = new ArrayList<ListenerRegistration>();
        listenersNewMessage = new ArrayList<ListenerRegistration>();


        if (Config.DUPLI_TEST)
        {
            Log.d("_AFRAG_SC_CRE_", "onCreate space fragment  : aa_activity_home_screen__ " );

        }




    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {









        if (Config.DUPLI_TEST) {
           Log.d("_AFRAG_SC_VIEW_", " onCreateView space fragment  : aa_activity_home_screen__A " + mPage);

        }



        View rootView = inflater.inflate(R.layout.al_activity_friendlist, container, false);



       // ((HomeActivity)getActivity()).friendListFileManagerF.setUpVIEW(rootView);

        //((HomeActivity)getActivity()).friendListFileManagerF.setUpRecyclerView();
        recyclerView = rootView.findViewById(R.id.recycler_viewF);
        recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);











        if (Config.DUPLI_TEST) {
            Log.d("_AFRAG_SC_VIEW_", " onCreateView space fragment  : aa_activity_home_screen " );

        }



       // setUpVIEW(rootView);

       // setUpRecyclerView();




            setUpRecyclerView();





        return rootView;
    }


    public  void setUpRecyclerView() {
        //Query query = FriendsList.orderBy("priority", Query.Direction.DESCENDING);


        if(FirebaseAuth.getInstance().getCurrentUser()!=null)
        {
            FriendsList = db.collection("Friends").document(FirebaseAuth.getInstance().getUid()).collection("MyFriends");

            Query query = FriendsList.orderBy("time_stamp", Query.Direction.DESCENDING);
            FirestoreRecyclerOptions<ModelFriendlist> options = new FirestoreRecyclerOptions.Builder<ModelFriendlist>()
                    .setQuery(query, ModelFriendlist.class)
                    .build();

            adapter = new FriendListAdapterXX(options,this);

            recyclerView.setAdapter(adapter);
            adapter.startListening();

        }





    }

    public void stopFlist()
    {

        if(FirebaseAuth.getInstance().getCurrentUser()!=null) {

            if(adapter!=null)
            {

                adapter.stopListening();

                for (int i = 0; i < listenersUsers.size(); i++)
                    if (listenersUsers.get(i) != null)
                        listenersUsers.get(i).remove();


                listenersUsers.clear();


                for (int i = 0; i < listenersNewMessage.size(); i++)
                    if (listenersNewMessage.get(i) != null)
                        listenersNewMessage.get(i).remove();


                listenersNewMessage.clear();
                aa_home_screen_PagerAdapter.registeredFragments.clear();
                FriendsList=null;

               // adapter=null;
            }


        }

    }




    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {


        if(Config.DUPLI_TEST)
            Log.d("_AFRAG_SC_VIEW_CTED", " After Fragment Space View Creation --  homeActivity " );

    }

/*

    public  void setUpRecyclerView(  )
    {
        if(Config.DUPLI_TEST)
            Log.d("_ATAB_SEL_A", "3 a ----"+Config.HIDE_TAB+"::"+Config.SEL_TAB );

        if(FirebaseAuth.getInstance().getCurrentUser()!=null)
        {
             FriendsList = FirebaseFirestore.getInstance().collection("Friends").document(FirebaseAuth.getInstance().getUid()).collection("MyFriends");

            if(Config.DUPLI_TEST)
                Log.d("_ATAB_SEL_A", "3 b ----"+Config.HIDE_TAB+"::"+Config.SEL_TAB );


            Query query=null;
            FirestoreRecyclerOptions<ModelFriendlistF> options=null;


            query =  FriendsList.orderBy("time_stamp", Query.Direction.DESCENDING);;


            options = new FirestoreRecyclerOptions.Builder<ModelFriendlistF>()
                    .setQuery( query, ModelFriendlistF.class)
                    .build();



            ((HomeActivity)getActivity()).friendListFileManagerF.adapter = new FriendListAdapterF( options,homeActivity);


            ((HomeActivity)getActivity()).friendListFileManagerF.recyclerView.setAdapter( ((HomeActivity)getActivity()).friendListFileManagerF.adapter);
            ((HomeActivity)getActivity()).friendListFileManagerF.adapter.startListening();
            ((HomeActivity)getActivity()).friendListFileManagerF.adapter.notifyDataSetChanged();



        }








    }

    public void setUpVIEW(View rootView ){
        ((HomeActivity)getActivity()).friendListFileManagerF.recyclerView = rootView.findViewById(R.id.recycler_viewF);
        ((HomeActivity)getActivity()).friendListFileManagerF.recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(((HomeActivity)getActivity()));
        ((HomeActivity)getActivity()).friendListFileManagerF.recyclerView.setLayoutManager(layoutManager);
        if(Config.DUPLI_TEST)
            Log.d("_ATAB_SEL_A", "2 ----"+Config.HIDE_TAB+"::"+Config.SEL_TAB );

    }
*/

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if(Config.DUPLI_TEST)
        {
            Log.d("_AFRAG_SPC_DEST_", "Destroy executed from  Space -- home activity");

        }

        stopFlist();

    }


    @Override
    public void onStart() {
        super.onStart();



    }

    @Override
    public void onResume() {
        super.onResume();

    }

// Fragment is active




    @Override
    public void onStop() {
        super.onStop();


        if(Config.DUPLI_TEST)
        {
            Log.d("_AFRAG_SPC_STOP_", "onStop executed from  Space -- home activity");

        }


/*
        if(FirebaseAuth.getInstance().getCurrentUser()!=null) {

            adapter.stopListening();
            for (int i = 0; i < HomeActivity.homeActivityS.listenersUsers.size(); i++)
                if (HomeActivity.homeActivityS.listenersUsers.get(i) != null)
                    HomeActivity.homeActivityS.listenersUsers.get(i).remove();


            HomeActivity.homeActivityS.listenersUsers.clear();
        }
        */

    }


}
