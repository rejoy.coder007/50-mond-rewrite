package com.media.vidplayer2.mond.ab_home_screen.aa_tabs.aa_ChatFragments;

import android.content.Context;
import android.util.Log;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.media.vidplayer2.mond.R;
import com.media.vidplayer2.mond.ab_home_screen.HomeActivity;
import com.media.vidplayer2.mond.zb_Config.Config;

import java.util.ArrayList;

public class FriendListFileManagerF {

    public CollectionReference notebookRef=null;
    public FriendListAdapterF                                  adapter=null;
    public RecyclerView recyclerView=null;
    public ArrayList<ListenerRegistration> listenersUsers=null;
    public HomeActivity homeActivity=null;




    public FriendListFileManagerF(HomeActivity context) {
        this.homeActivity = (HomeActivity)context;
        listenersUsers = new ArrayList<ListenerRegistration>();
    }


    public  void setUpRecyclerView(  )
    {
        if(Config.DUPLI_TEST)
            Log.d("_ATAB_SEL_A", "3 a ----"+Config.HIDE_TAB+"::"+Config.SEL_TAB );

        if(FirebaseAuth.getInstance().getCurrentUser()!=null)
        {
            notebookRef = FirebaseFirestore.getInstance().collection("Friends").document(FirebaseAuth.getInstance().getUid()).collection("MyFriends");

            if(Config.DUPLI_TEST)
                Log.d("_ATAB_SEL_A", "3 b ----"+Config.HIDE_TAB+"::"+Config.SEL_TAB );


            Query query=null;
            FirestoreRecyclerOptions<ModelFriendlistF> options=null;


            query =  notebookRef.orderBy("time_stamp", Query.Direction.DESCENDING);;


            options = new FirestoreRecyclerOptions.Builder<ModelFriendlistF>()
                    .setQuery( query, ModelFriendlistF.class)
                    .build();



            adapter = new FriendListAdapterF( options,homeActivity);


            recyclerView.setAdapter( adapter);
            adapter.notifyDataSetChanged();



        }








    }

    public void setUpVIEW(View rootView ){
        recyclerView = rootView.findViewById(R.id.recycler_viewF);
        recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(homeActivity);
        recyclerView.setLayoutManager(layoutManager);
        if(Config.DUPLI_TEST)
            Log.d("_ATAB_SEL_A", "2 ----"+Config.HIDE_TAB+"::"+Config.SEL_TAB );

    }

    public void stopFlist()
    {

        if(FirebaseAuth.getInstance().getCurrentUser()!=null) {

            if(adapter!=null)
            {
                adapter.stopListening();
                for (int i = 0; i < homeActivity.friendListFileManagerF.listenersUsers.size(); i++)
                    if (homeActivity.friendListFileManagerF.listenersUsers.get(i) != null)
                        homeActivity.friendListFileManagerF.listenersUsers.get(i).remove();


                homeActivity.friendListFileManagerF.listenersUsers.clear();

            }


        }

    }

}
