package com.media.vidplayer2.mond.zh_friendlist;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.WriteBatch;
import com.media.vidplayer2.mond.R;

import java.util.ArrayList;
import java.util.HashMap;

public class FriendListActvity extends AppCompatActivity {
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
   // private CollectionReference notebookRef = db.collection("Notebook");
   private CollectionReference notebookRef = db.collection("Friends").document(FirebaseAuth.getInstance().getUid()).collection("MyFriends");
    public FriendListAdapter adapter;



    public ArrayList<ListenerRegistration> listenersUsers;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.al_activity_friendlist);
        listenersUsers = new ArrayList<ListenerRegistration>();


    }


    @Override
    protected void onStop()
    {

        super.onStop();  // Always call the superclass method first

        adapter.stopListening();
        for(int i=0;i<listenersUsers.size();i++)
            if(listenersUsers.get(i)!=null)
                listenersUsers.get(i).remove();


        listenersUsers.clear();





    }

    // friendListActvity.listenersUsers.set(position,listenerRegistration);

    private void setUpRecyclerView() {
        //Query query = notebookRef.orderBy("priority", Query.Direction.DESCENDING);
        Query query = notebookRef.orderBy("time_stamp", Query.Direction.DESCENDING);
        FirestoreRecyclerOptions<ModelFriendlist> options = new FirestoreRecyclerOptions.Builder<ModelFriendlist>()
                .setQuery(query, ModelFriendlist.class)
                .build();

        adapter = new FriendListAdapter(options,this);

        RecyclerView recyclerView = findViewById(R.id.recycler_viewF);
        recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);


/*
          RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy)
            {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = layoutManager.getChildCount();
                int  totalItemCount = layoutManager.getItemCount();
                   firstVisibleItemPosition = ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();
                   lasttVisibleItemPosition = ((LinearLayoutManager) layoutManager).findLastVisibleItemPosition();
                int  lastVisibleItem = firstVisibleItemPosition +visibleItemCount;
               /// Toast.makeText(RecyclerViewActivity5.this, "Visible Item Total:"+String.valueOf(visibleItemCount), Toast.LENGTH_SHORT).show();
            }
        };

        recyclerView.addOnScrollListener(onScrollListener);
        */
    }

    @Override
    protected void onStart() {
        super.onStart();

        setUpRecyclerView();
        adapter.startListening();
    }


}
