package com.media.vidplayer2.mond.zf_space_share.downloadSpace;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.api.client.googleapis.media.MediaHttpDownloader;
import com.google.api.client.googleapis.media.MediaHttpDownloaderProgressListener;
import com.google.api.client.googleapis.media.MediaHttpUploader;
import com.google.api.client.http.InputStreamContent;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.media.vidplayer2.mond.zb_Config.Config;
import com.media.vidplayer2.mond.zf_space_share.uploadSpace.GoogleDriveFileHolder;
import com.media.vidplayer2.mond.zf_space_share.uploadSpace.UploadSpaceActivity;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static android.os.Environment.getExternalStorageDirectory;

public class FileManagerDownloadSpace
{




    public Drive.Files.Get request =null;
    public Context application_context=null;
    final Runnable[] runnable = new Runnable[5];
    private  Integer tokenVal;
    private  String foldername ="MondPlayerDataFiles";
    class CustomProgressListener implements MediaHttpDownloaderProgressListener
    {

        File file=null;
        double first_numBytes =0,second_numBytes=0;
        public double first_time =0,second_time=0;
        double download_speed=0;


        int index_download=0;

        public CustomProgressListener(File file) {
            this.file = file;
        }


        public void FileProgress(MediaHttpDownloader downloader)
        {




            second_numBytes = downloader.getNumBytesDownloaded();
            second_time = getCurrentTimeInseconds() ;

            download_speed = toMB((second_numBytes - first_numBytes)/ (second_time - first_time));

            double percent = (((double) downloader.getNumBytesDownloaded() / (double) file.getSize()) * 100);


            double percentRoundOff = roundTwo(percent);
            double download_speedRoundOff = roundTwo(download_speed);


            if (Config.DUPLI_TEST)
                Log.d("DRIVE_DOWNLOAD_TEST", "_#download Progress :: " + file.getSize() + "::" + downloader.getNumBytesDownloaded() + "::" + percentRoundOff + "% :: " + download_speedRoundOff);


            runnable[4] =  new Runnable()
            {
                public void run()
                {
                    if(Config.DUPLI_TEST)
                        Log.d("DRIVE_DOWNLOAD_TEST", "Running 4");

                    downloadSpaceActivity.percentage_size.setText(percentRoundOff+"%   ("+ roundTwo(toMB(downloader.getNumBytesDownloaded()))+"/"+ roundTwo(toMB(file.getSize()))+" MB)");
                    downloadSpaceActivity.speed_download.setText(download_speedRoundOff+" MB/second");
                }
            };

            try {
                if(Config.KeyExistDownload(tokenVal.toString())) {
                    downloadSpaceActivity.handler.post(runnable[4] );
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }




            first_numBytes = second_numBytes;
            first_time = second_time;



        }

        public void progressChanged(MediaHttpDownloader downloader)
        {



            switch (downloader.getDownloadState())
            {




                case MEDIA_IN_PROGRESS:

                    FileProgress(downloader);
                break;


                case MEDIA_COMPLETE:
                    FileProgress(downloader);

                break;






            }
        }
    }




    private Executor mExecutor = Executors.newSingleThreadExecutor();
    private Drive mDriveService=null;
    public DownloadSpaceActivity downloadSpaceActivity =null;

    private Context context=null;






    public FileManagerDownloadSpace(Drive mDriveService, Context context,Integer tokenVal) {
        this.mDriveService = mDriveService;

        downloadSpaceActivity = (DownloadSpaceActivity) context;
        application_context = downloadSpaceActivity.getApplicationContext();
        this.tokenVal=tokenVal;
    }

    public Task<String> download_file_Case(File file,Boolean download_mode)
    {






        return Tasks.call(mExecutor, () ->
        {

            java.io.File  direct = new java.io.File(getExternalStorageDirectory().toString()+"/MondInbox");



            runnable[0] = new Runnable() {
                public void run() {

                    downloadSpaceActivity.download_mode_d.setVisibility(View.GONE);
                }

            };


            runnable[1] =  new Runnable()
            {
                public void run()
                {
                    // UI code goes here
                    downloadSpaceActivity.percentage_size.setText("Download....");
                    downloadSpaceActivity.speed_download.setText("Started....");
                    if(Config.DUPLI_TEST)
                        Log.d("DRIVE_DOWNLOAD_TEST", "Running 1");

                }
            };


            runnable[2] =  new Runnable()
            {
                public void run()
                {
                    downloadSpaceActivity.ViewCase1_prior();
                    downloadSpaceActivity.download_mode_d.setVisibility(View.GONE);
                    downloadSpaceActivity.text_id_space_d.setText("Download success");
                    downloadSpaceActivity.STATUS_TOKEN = 0;

                    if(Config.DUPLI_TEST)
                        Log.d("DRIVE_DOWNLOAD_TEST", "Running 2");

                }
            };


            runnable[3] =  new Runnable()
            {
                public void run()
                {
                    downloadSpaceActivity.ViewCase1_prior();
                    downloadSpaceActivity.download_mode_d.setVisibility(View.GONE);
                    downloadSpaceActivity.text_id_space_d.setText("Download Failed");
                    downloadSpaceActivity.STATUS_TOKEN = 0;

                    if(Config.DUPLI_TEST)
                        Log.d("DRIVE_DOWNLOAD_TEST", "Running 3");

                }
            };



            if(Config.KeyExistDownload(tokenVal.toString())) {
                downloadSpaceActivity.handler.post(runnable[0]);
            }


            if(!direct.exists())
            {

                Boolean success=direct.mkdir();



            }

            java.io.File folder = new java.io.File(getExternalStorageDirectory() +
                    java.io.File.separator + "MondInbox"+java.io.File.separator+file.getName() );


            FileOutputStream fout = new FileOutputStream(folder);


             request =  mDriveService.files().get(file.getId());

            if(Config.KeyExistDownload(tokenVal.toString())) {

                downloadSpaceActivity.handler.post(runnable[1]);
            }


            if(download_mode)
            {
                request.getMediaHttpDownloader().setDirectDownloadEnabled(true);
                request.getMediaHttpDownloader().setChunkSize(MediaHttpUploader.DEFAULT_CHUNK_SIZE);

            }

            else
            {
                request.getMediaHttpDownloader().setDirectDownloadEnabled(false);
                request.getMediaHttpDownloader().setChunkSize(MediaHttpUploader.MINIMUM_CHUNK_SIZE);
            }


            CustomProgressListener customProgressListener= new CustomProgressListener(file);

            customProgressListener.first_time=getCurrentTimeInseconds();
            customProgressListener.first_numBytes=0;
            request.getMediaHttpDownloader().setProgressListener(customProgressListener);
            request.executeMediaAndDownloadTo(fout);


            return file.getId();
        }).addOnSuccessListener(new OnSuccessListener<String>() {
            @Override
            public void onSuccess(String s) {

                if(Config.DUPLI_TEST)
                    Log.d("DRIVE_DOWNLOAD_TEST", "download Done :: ");

                try {
                    if(Config.KeyExistDownload(tokenVal.toString())) {

                        downloadSpaceActivity.handler.post(runnable[2]);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                if(Config.DUPLI_TEST)
                    Log.d("DRIVE_DOWNLOAD_TEST", "download Failed :: "+e);

                try {
                    if(Config.KeyExistDownload(tokenVal.toString())) {
                        downloadSpaceActivity.handler.post(runnable[3]);
                    }
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        });
    }

    public double getCurrentTimeInseconds()
    {

        long timeInMilliSeconds = TimeUnit.MILLISECONDS.toSeconds(new Date().getTime());



        return (double)timeInMilliSeconds;
    }

    public double roundTwo(double val)
    {


        return (double)Math.round(val * 100.0) / 100.00;
    }

    public double toMB(double val)
    {


        return (double)val/(1024*1024);
    }


}
