package com.media.vidplayer2.mond.ab_home_screen.aa_tabs.aa_ChatFragments;


import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.MetadataChanges;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.WriteBatch;
import com.media.vidplayer2.mond.R;
import com.media.vidplayer2.mond.ad_Profile.MyProfileActivity;
import com.media.vidplayer2.mond.zb_Config.Config;
import com.media.vidplayer2.mond.zb_a_chat.FireStoreChat.FirestoreChatActivity;
import com.media.vidplayer2.mond.zb_a_chat.GetTimeAgo;
import com.media.vidplayer2.mond.zc_Glide.FutureStudioAppGlideModule;
import com.media.vidplayer2.mond.zh_friendlist.ModelFriendlist;
import com.media.vidplayer2.mond.zz_TEST.TestCase;
import com.nex3z.notificationbadge.NotificationBadge;

import java.util.HashMap;

public class FriendListAdapterXX extends FirestoreRecyclerAdapter<ModelFriendlist, FriendListAdapterXX.NoteHolder> {

    DocumentReference docRef;
    FutureStudioAppGlideModule futureStudioAppGlideModule ;
    FriendListFragmentF friendListFragment;

   //public  ListenerRegistration listenerRegistration;

   // public  ListenerRegistration listenerRegistrationMessage;

    public static String current_id ;
    public static String current_name ;
    public static String current_image;
    public static String current_email;

    public   String current_uid ;


    public FriendListAdapterXX(@NonNull FirestoreRecyclerOptions<ModelFriendlist> options, FriendListFragmentF friendListFragment) {
        super(options);
        futureStudioAppGlideModule = new FutureStudioAppGlideModule();
        this.friendListFragment = friendListFragment;

        this.current_uid = FirebaseAuth.getInstance().getUid();

    }

    @Override
    protected void onBindViewHolder(@NonNull NoteHolder holder, int position, @NonNull ModelFriendlist model)
    {
        //holder.textViewTitle.setText(model.getId());
     //   holder.textViewDescription.setText(model.getDescription());
      //  holder.textViewPriority.setText(String.valueOf(model.getPriority()));








            if((FirebaseAuth.getInstance().getCurrentUser()!=null)  && (model.getTime_stamp()!=null))
            {


                listeners_users(holder,position,model);
                 listeners_chats(holder,position,model);










                if(FirebaseAuth.getInstance().getCurrentUser().getUid().equals(model.getId()) || model.getBlock())
                {
                    //  holder.recycler_item_frdlist.setVisibility(View.GONE);
                    holder.itemView.setVisibility(View.GONE);
                    holder.itemView.setLayoutParams(new RecyclerView.LayoutParams(0, 0));

                }
                else
                {


                    if(Config.DUPLI_TEST)
                        Log.d("DOWNLOAD_LINK_A_FULL", "other user ::  "+model.getId()+"current user::"+FirebaseAuth.getInstance().getCurrentUser().getUid()+"::"+position);


                }

            }

    }


    void listeners_users(NoteHolder holder, int position, @NonNull ModelFriendlist model)
    {

        ListenerRegistration listenerRegistration;
        listenerRegistration= FirebaseFirestore.getInstance().collection("UsersList").document(model.getId()).addSnapshotListener(MetadataChanges.INCLUDE, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                @Nullable FirebaseFirestoreException e) {


                if (e != null) {
                    System.err.println("Listen failed:" + e);
                    return;
                }




                if(FirebaseAuth.getInstance().getCurrentUser().getUid().equals(model.getId()) )
                {
                    //  holder.recycler_item_frdlist.setVisibility(View.GONE);

                    FriendListAdapterF1.current_id =FirebaseAuth.getInstance().getCurrentUser().getUid();
                    FriendListAdapterF1.current_name= documentSnapshot.getString("name");
                    FriendListAdapterF1.current_image= documentSnapshot.getString("thumb_image");
                    FriendListAdapterF1.current_email= documentSnapshot.getString("email");

                }
                else
                {

                    holder.other_user_id=documentSnapshot.getString("id");
                    holder.other_user_name=documentSnapshot.getString("name");
                    holder.other_user_image=documentSnapshot.getString("thumb_image");
                    holder.other_user_email=documentSnapshot.getString("email");
                }





                String id = documentSnapshot.getString("id");
                String status = documentSnapshot.getString("status");
                String thumb_image =  documentSnapshot.getString("thumb_image");
                String name =  documentSnapshot.getString("name");
                Boolean online =  documentSnapshot.getBoolean("online");
                Timestamp timestamp = documentSnapshot.getTimestamp("time_stamp");






                holder.user_single_name.setText(name );
                holder.statusfrd.setText(status );







                if(!thumb_image.equals("default"))
                {
                    futureStudioAppGlideModule.add_image_to_view_link(friendListFragment.getContext(),holder.user_single_image,thumb_image);

                }





                if( online )
                {
                    holder.user_single_online_icon.setVisibility(View.VISIBLE);
                    holder.time_left.setText("Online");
                }
                else
                {
                    holder.user_single_online_icon.setVisibility(View.GONE);
                    holder.user_single_online_icon.setVisibility(View.GONE);
                    GetTimeAgo getTimeAgo = new GetTimeAgo();
                    holder.time_left.setText("Online");
                    String lastSeenTime=null;
                    if(timestamp!=null)
                        lastSeenTime = getTimeAgo.getTimeAgo(timestamp.toDate().getTime(), friendListFragment.getActivity());
                    holder.time_left.setText(lastSeenTime);

                }



                if(Config.DUPLI_TEST)
                    Log.d("DOWNLOAD_LINK_A", "status "+status+"thumb_image "+thumb_image+"name :: "+name);

                if(Config.DUPLI_TEST)
                    Log.d("DOWNLOAD_LINK_A", "error "+ e);


                holder.user_single_name.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        //Toast.makeText(UsersActivity.this, String.valueOf(position), Toast.LENGTH_SHORT).show();
                        //SendtoAllUsersProfleActivity(user_id);
                        //Log.d("Clicked id", user_id);


                          // Intent chatIntent = new Intent(FriendListsAcitivty.this, ChatActivity.class);
                         Intent chatIntent = new Intent(friendListFragment.getActivity(), FirestoreChatActivity.class);
                         //Intent chatIntent = new Intent(friendListFragment.getActivity(), TestCase.class);
                        chatIntent.putExtra("current_id", FriendListAdapterF1.current_id);
                        chatIntent.putExtra("current_name", FriendListAdapterF1.current_name);
                        chatIntent.putExtra("current_image", FriendListAdapterF1.current_image);
                        chatIntent.putExtra("current_email", FriendListAdapterF1.current_email);

                        chatIntent.putExtra("other_user_id", holder.other_user_id);
                        chatIntent.putExtra("other_user_name",holder.other_user_name);
                        chatIntent.putExtra("other_user_image", holder.other_user_image);
                        chatIntent.putExtra("other_user_email", holder.other_user_email);

                        friendListFragment.getActivity().startActivity(chatIntent);




                    }
                });
                holder.statusfrd.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        //Toast.makeText(UsersActivity.this, String.valueOf(position), Toast.LENGTH_SHORT).show();
                        //SendtoAllUsersProfleActivity(user_id);
                        //Log.d("Clicked id", user_id);


                        //   Intent chatIntent = new Intent(FriendListsAcitivty.this, ChatActivity.class);
                       // Intent chatIntent = new Intent(friendListFragment.getActivity(), FirestoreChatActivity.class);
                         Intent chatIntent = new Intent(friendListFragment.getActivity(), TestCase.class);
                        chatIntent.putExtra("current_id", FriendListAdapterF1.current_id);
                        chatIntent.putExtra("current_name", FriendListAdapterF1.current_name);
                        chatIntent.putExtra("current_image", FriendListAdapterF1.current_image);
                        chatIntent.putExtra("current_email", FriendListAdapterF1.current_email);

                        chatIntent.putExtra("other_user_id", holder.other_user_id);
                        chatIntent.putExtra("other_user_name",holder.other_user_name);
                        chatIntent.putExtra("other_user_image", holder.other_user_image);
                        chatIntent.putExtra("other_user_email", holder.other_user_email);

                        friendListFragment.getActivity().startActivity(chatIntent);




                    }
                });



                holder.user_single_image.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        //Toast.makeText(UsersActivity.this, String.valueOf(position), Toast.LENGTH_SHORT).show();
                        //SendtoAllUsersProfleActivity(user_id);
                        //Log.d("Clicked id", user_id);

                        CharSequence options[] = new CharSequence[]{"Open Profile", "Remove","Block"};

                        final AlertDialog.Builder builder = new AlertDialog.Builder(friendListFragment.getActivity());

                        builder.setTitle("Select Options");
                        builder.setItems(options, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {


                                switch(i)
                                {
                                    case 0:

                                        //   Intent chatIntent = new Intent(FriendListsAcitivty.this, ChatActivity.class);
                                        Intent profileintent = new Intent(friendListFragment.getActivity(), MyProfileActivity.class);
                                        profileintent.putExtra("otherprofile", true);
                                        profileintent.putExtra("other_user_id", holder.other_user_id);



                                        friendListFragment.getActivity().startActivity(profileintent);


                                        break;

                                    case 1:





                                        WriteBatch batch =  FirebaseFirestore.getInstance().batch();


                                        batch.delete(holder.FriendListReference_1);
                                        batch.delete(holder.FriendListReference_2);

                                        batch.commit().addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {


                                            }


                                        }).addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {





                                            }
                                        });


                                        //docRef.delete();
                                        break;
                                    case 2:


                                        HashMap<String, Object> FrduserMap_1 = new HashMap<>();



                                        FrduserMap_1.put("block",  true);
                                        HashMap<String, Object> FrduserMap_2 = new HashMap<>();



                                        FrduserMap_2.put("block",  true);
                                        WriteBatch batch1 =  FirebaseFirestore.getInstance().batch();


                                        batch1.update(holder.FriendListReference_1, FrduserMap_1);
                                        batch1.update(holder.FriendListReference_2, FrduserMap_2);

                                        batch1.commit().addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {



                                            }


                                        }).addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {





                                            }
                                        });

                                        break;
                                }


                            }
                        });

                        builder.show();

                    }
                });







            }
        });
        if(friendListFragment.listenersUsers.size()>this.getItemCount())
        {
            friendListFragment.listenersUsers.get(position).remove();
        }
        friendListFragment.listenersUsers.add(listenerRegistration);

    }

    void listeners_chats(NoteHolder holder, int position, @NonNull ModelFriendlist model)
    {

        ListenerRegistration listenerRegistrationMessage=  FirebaseFirestore.getInstance().collection("Chats").document(FirebaseAuth.getInstance().getCurrentUser().getUid()).collection(model.getId()).addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot value,
                                @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.w("ERROR", "Listen failed.", e);
                    return;
                }



                Timestamp timestamp = new Timestamp(model.getTime_stamp());
                if(Config.DUPLI_TEST)
                    Log.d("FRDID", model.getId()+"::"+model.getBlock()+"::"+model.getTime_stamp()+"::"+position);




                             FirebaseFirestore.getInstance().collection("Chats").document(FirebaseAuth.getInstance().getCurrentUser().getUid()).collection(model.getId()).whereGreaterThanOrEqualTo("timestamp",timestamp).get()
                                    .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>()
                                    {
                                        @Override
                                        public void onSuccess(QuerySnapshot queryDocumentSnapshots)
                                        {


                                            if (queryDocumentSnapshots.size() > 0)
                                            {


                                                try {
                                                   // holder.noti_newmsg.setText("( " +queryDocumentSnapshots.size()+" )");;

                                                    holder.badge.setNumber(queryDocumentSnapshots.size());
                                                    holder.badge.getAnimationEnabled();
                                                    holder.badge.setAnimationDuration(1000);
                                                    holder.badge.animate();

                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }

                                            }
                                            else
                                            {




                                                try {
                                                    holder.badge.setVisibility(View.GONE);
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }




                                            }



                                        }
                                    });



            }
        });
        if(friendListFragment.listenersNewMessage.size()>this.getItemCount())
        {
            friendListFragment.listenersNewMessage.get(position).remove();
        }
        friendListFragment.listenersNewMessage.add( listenerRegistrationMessage);


    }

    @Override
    public NoteHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.am_friendlist_item,
                parent, false);
        return new NoteHolder(v);
    }

    class NoteHolder extends RecyclerView.ViewHolder {

        ImageView user_single_image;
        TextView user_single_name;
        TextView statusfrd;
        TextView noti_newmsg;

        NotificationBadge badge;

        LinearLayout slice;

        LinearLayout recycler_item_frdlist;
        ImageView user_single_online_icon;
        ImageView more;
        ImageView chatF;
        TextView time_left;
        public DocumentReference FriendListReference_1 ;
        public  DocumentReference FriendListReference_2  ;


        public   String other_user_id;
        public   String other_user_name;
        public   String other_user_image;
        public   String other_user_email;


        public NoteHolder(View itemView) {
            super(itemView);
            user_single_image = itemView.findViewById(R.id.user_single_image);

            time_left = itemView.findViewById(R.id.time_left);

            user_single_online_icon = itemView.findViewById(R.id.user_single_online_icon);
            user_single_name = itemView.findViewById(R.id.user_single_name);
            statusfrd = itemView.findViewById(R.id.statusfrd);
            badge = itemView.findViewById(R.id.badge);


            slice = itemView.findViewById(R.id.recycler_item_frdlist);
            recycler_item_frdlist = itemView.findViewById(R.id.recycler_item_frdlist);
        }


    }

}



