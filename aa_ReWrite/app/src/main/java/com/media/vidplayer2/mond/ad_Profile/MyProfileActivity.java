package com.media.vidplayer2.mond.ad_Profile;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;
import androidx.core.content.ContextCompat;

import com.droidbond.loadingbutton.LoadingButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.MetadataChanges;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.media.vidplayer2.mond.R;
import com.media.vidplayer2.mond.ac_Selector_Reg_or_Login.LoginActivity1;
import com.media.vidplayer2.mond.ac_Selector_Reg_or_Login.MimeFinder;
import com.media.vidplayer2.mond.zb_Config.Config;
import com.media.vidplayer2.mond.zc_Glide.GlideApp;
import com.media.vidplayer2.mond.ze_LeakManager.IMMLeaks;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.media.vidplayer2.mond.zc_Glide.FutureStudioAppGlideModule;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;


public class MyProfileActivity extends AppCompatActivity {
    private static final String TAG = MyProfileActivity.class.getSimpleName();
    public static final int REQUEST_IMAGE = 100;
    FutureStudioAppGlideModule futureStudioAppGlideModule;



    public FirebaseAuth mAuth;
    public FirebaseFirestore mFirestore;
    ListenerRegistration UsersDetailsListner;


    Uri uri;
  //  mCurrentUser = FirebaseAuth.getInstance().getCurrentUser();
    String userIDtoDisplay;

    Integer tokenVal;
    public double first_time =0;
    FileManagerProfile mDriveServiceHelper;
    public Handler handler;



    @Override
    public void onBackPressed() {


        try {
            Config.semaphoreprofileUpload.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        handler.removeCallbacksAndMessages(null);

        Config.deleteHashprofileUpload(tokenVal.toString());
        mDriveServiceHelper.myProfileActivity =null;
        Config.semaphoreprofileUpload.release();
        super.onBackPressed();
        NavUtils.navigateUpFromSameTask(this);



    }


    @BindView(R.id.profile_desc)
    TextView profile_desc;


    @BindView(R.id.phno)
    TextView phno;
    @BindView(R.id.loadingbutton_p)
    LoadingButton loadingButton;


    @BindView(R.id.settings_image_btn_change_status)
    Button settings_image_btn_change_status;

    @OnClick(R.id.settings_image_btn_change_status)
    public void settings_image_btn_change_status(View view)
    {

        String status_value = profile_desc.getText().toString();

        Intent status_intent = new Intent(MyProfileActivity.this, statusActvity.class);
        status_intent.putExtra("status_value", status_value);
        //   Log.d("#STOP_DET", "inside status Button ::" +CLOSING);
        startActivity(status_intent);




    }



    @BindView(R.id.name_id)
    TextView name_id;

   @BindView(R.id.img_profile)
   ImageView img_profile;

    @BindView(R.id.img_plus)
    ImageView img_plus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aj_b_activity_myprofile);
        ButterKnife.bind(this);
        loadingButton.setVisibility(View.GONE);

        handler= new Handler(Looper.getMainLooper());

        if(getIntent() != null && getIntent().getExtras() != null)
        {
            if (getIntent().getExtras().getBoolean("otherprofile"))
            {
                img_plus.setVisibility(View.GONE);
                settings_image_btn_change_status.setVisibility(View.GONE);

                userIDtoDisplay = getIntent().getStringExtra("other_user_id");

            }
        }
        else
        {
            userIDtoDisplay = FirebaseAuth.getInstance().getUid();
        }





        mAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();

        name_id.setText(mAuth.getCurrentUser().getDisplayName());

        loadProfileDefault();



          futureStudioAppGlideModule = new FutureStudioAppGlideModule();



        GoogleAccountCredential credential =
                GoogleAccountCredential.usingOAuth2(
                        getApplicationContext(), Collections.singleton(DriveScopes.DRIVE_FILE));

        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(getApplicationContext());

        credential.setSelectedAccount(acct.getAccount());
        Drive googleDriveService =
                new Drive.Builder(
                        AndroidHttp.newCompatibleTransport(),
                        new GsonFactory(),
                        credential)
                        .setApplicationName("Drive API Migration")
                        .build();


        /*
        uri =  Uri.fromFile( new File(path));

        */

        tokenVal =  LoginActivity1.getRandomNumberInRange( 0,100000);
        while (insert_Random(tokenVal))
        {
            tokenVal =  LoginActivity1.getRandomNumberInRange( 0,100000);
        }

        Config.profileUpload.put(tokenVal.toString(),tokenVal);

        mDriveServiceHelper = new FileManagerProfile(googleDriveService, MyProfileActivity.this,tokenVal);

        IMMLeaks.fixFocusedViewLeak(getApplication());



    }

    Boolean insert_Random(Integer val )
    {

        Integer integer=val;

        while(true &&  Config.profileUpload.size()>=0)
        {

            for (Map.Entry<String, Integer> entry : Config.profileUpload.entrySet())
            {

                if(entry.getValue()==integer)
                    return true;
            }

            return  false;

        }
        return  false;

    }




    public  void loadProfile_link(String strlink) {


        //  futureStudioAppGlideModule. add_image_to_view_link(this, img_profile,uri);
        //  futureStudioAppGlideModule. add_image_to_view_link(this, img_profile,uri);


        Log.d(TAG, "Image cache path: " + uri);

        GlideApp.with(this).load(strlink)
                .into(img_profile);
        img_profile.setColorFilter(ContextCompat.getColor(this, android.R.color.transparent));

    }

    private void loadProfileDefault() {



        GlideApp.with(this).load(R.drawable.baseline_account_circle_black_48)
                .into(img_profile);
        img_profile.setColorFilter(ContextCompat.getColor(this, R.color.profile_default_tint));


    }

    //@OnClick({R.id.img_plus, R.id.img_profile})
    @OnClick({R.id.img_plus})
    void onProfileImageClick() {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener()
                {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report)
                    {
                        if (report.areAllPermissionsGranted())
                        {
                           // showImagePickerOptions();

                            loadingButton.setVisibility(View.VISIBLE);
                            loadingButton.showLoading();
                            CropImage.activity()
                                    .setGuidelines(CropImageView.Guidelines.ON)
                                    .start(MyProfileActivity.this);
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }






    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri uri = result.getUri();

                String mime_type = MimeFinder.getMimeType(MyProfileActivity.this, uri) ;
                mDriveServiceHelper.UploadTotalOperation_Dir_Upload("profile_pic", uri  ,  mime_type ,null );

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }

        super.onActivityResult(  requestCode,   resultCode,    data);
    }


    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MyProfileActivity.this);
        builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton(getString(android.R.string.cancel), (dialog, which) -> dialog.cancel());
        builder.show();

    }




    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }
    @Override
    protected void onStop()
    {

        super.onStop();  // Always call the superclass method first
        UsersDetailsListner.remove();




    }

    @Override
    public void onResume()
    {
        super.onResume();







        UsersDetailsListner= mFirestore.collection("UsersList").document(userIDtoDisplay).addSnapshotListener(MetadataChanges.INCLUDE, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                @Nullable FirebaseFirestoreException e)
            {


                if (e != null) {
                    System.err.println("Listen failed:" + e);
                    return;
                }




              //  String image = documentSnapshot.getString("image");
                String status = documentSnapshot.getString("status");
                String thumb_image =  documentSnapshot.getString("thumb_image");
                String mob =  documentSnapshot.getString("mob");

                if(Config.DUPLI_TEST)
                    Log.d("DOWNLOAD_LINK", "status "+status+"thumb_image "+thumb_image);

                if(Config.DUPLI_TEST)
                    Log.d("DOWNLOAD_LINK", "error "+ e);

                profile_desc.setText(status);
                phno.setText(mob);

                 if(!thumb_image.equals("default"))
                {



                     loadProfile_link(thumb_image);
                }




            }
        });


    }



}
