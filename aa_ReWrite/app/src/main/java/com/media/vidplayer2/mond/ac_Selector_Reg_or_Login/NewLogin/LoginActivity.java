package com.media.vidplayer2.mond.ac_Selector_Reg_or_Login.NewLogin;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;

import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;

import com.droidbond.loadingbutton.LoadingButton;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.api.services.drive.DriveScopes;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.WriteBatch;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.media.vidplayer2.mond.OtpReceivedInterface;
import com.media.vidplayer2.mond.R;
import com.media.vidplayer2.mond.SmsBroadcastReceiver;
import com.media.vidplayer2.mond.ab_home_screen.HomeActivity;
import com.media.vidplayer2.mond.zb_Config.Config;
import com.rilixtech.CountryCodePicker;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.media.vidplayer2.mond.ac_Selector_Reg_or_Login.LoginActivity1.RC_REQUEST_PERMISSION_SUCCESS_CONTINUE_FILE_CREATION;
import static com.media.vidplayer2.mond.ac_Selector_Reg_or_Login.LoginActivity1.RC_SIGN_IN;
import static com.media.vidplayer2.mond.ac_Selector_Reg_or_Login.LoginActivity1.getRandomNumberInRange;


public class LoginActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        OtpReceivedInterface, GoogleApiClient.OnConnectionFailedListener {


    GoogleSignInAccount acct1  ;

    FileManagerLoginAcitivity fileManagerLoginAcitivity=null;

    SmsBroadcastReceiver mSmsBroadcastReceiver;

    private DocumentReference UserListReference;
    private   DocumentReference FriendListReference;

    public GoogleSignInClient                                           mGoogleSignInClient = null;

    private  Boolean DoneLogin =false;

    private static final int GOOGLE_SIGN_IN = 123;
    List<AuthUI.IdpConfig> providers;

    @BindView(R.id.loadingbutton)
    public LoadingButton loadingbutton;

    @BindView(R.id.text_id_heading)
    public TextView text_id_heading;


    @BindView(R.id.ccp_sms)
    public CountryCodePicker ccp;

    @BindView(R.id.mob_no_sms)
    public EditText mob_no;

    @BindView(R.id.google_button)
    public SignInButton signInButton;

    @BindView(R.id.gobackhome)
    public Button gobackhome;

    @OnClick(R.id.google_button)
    public void GoogleSignIn(View view) {

        showSignInOptions();
    }

   @OnClick(R.id.gobackhome)
    public void gobackhome(View view) {
       onBackPressed();



   }


    @Override
    public void onBackPressed() {



        super.onBackPressed();
        NavUtils.navigateUpFromSameTask(this);



    }




    @OnClick(R.id.loadingbutton)
    public void loadingbutton_phone(View view) {


        ccp.registerPhoneNumberTextView(mob_no);

        fileManagerLoginAcitivity.requestMultiplePermissionsSMS();

    }















    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.an_e_activity_login);

        ButterKnife.bind(this);


/*

*/








        signInButton.setVisibility(View.GONE);
        gobackhome.setVisibility(View.GONE);





        mSmsBroadcastReceiver = null;
        mSmsBroadcastReceiver = new SmsBroadcastReceiver();



        mSmsBroadcastReceiver.setOnOtpListeners(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION);
        registerReceiver(mSmsBroadcastReceiver, intentFilter);

        fileManagerLoginAcitivity = new FileManagerLoginAcitivity(this);



    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onOtpReceived(String otp) {

        if(Config.DUPLI_TEST)
            Log.d("SMS_TEST", "onOtpReceived: "+otp);


        String[] parts = otp.split(" ");



        if(Integer.toString(Config.OTP).equals(parts[4])) {
            loadingbutton.showSuccess();
            text_id_heading.setText("Verification is done \n" + otp + "\n" + parts[4]);
            signInButton.setVisibility(View.VISIBLE);
            mSmsBroadcastReceiver.resetOnOtpListeners();
            unregisterReceiver(mSmsBroadcastReceiver);
        }
        else
        {
            loadingbutton.showError();
            text_id_heading.setText("Verification is failed \n" + otp + "\n" + parts[4]);
        }


    }

    @Override
    public void onOtpTimeout() {
        text_id_heading.setText("Time out");
    }


    private void showSignInOptions()
    {

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestIdToken(getString(R.string.default_web_client_id))
                .requestScopes(new Scope(Scopes.EMAIL),new Scope(Scopes.PROFILE))
                .requestEmail()
                .requestProfile()
                .build();

        providers = Arrays.asList(

                new AuthUI.IdpConfig.GoogleBuilder().setSignInOptions(gso).build()

        );

        /*
        startActivityForResult(
                AuthUI.getInstance().createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .setTheme(R.style.Theme_HomeTheme)
                        .setLogo(R.drawable.aa_crown)
                        .build(), GOOGLE_SIGN_IN
        );

*/
        mGoogleSignInClient = GoogleSignIn.getClient(getApplicationContext(), gso);

        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);



    }


    public void signoutCase()
    {
        FirebaseAuth.getInstance().signOut();
        mGoogleSignInClient.signOut();

    }

    public void startSMSListener() {
        SmsRetrieverClient mClient = SmsRetriever.getClient(this);

        Task<Void> mTask = mClient.startSmsRetriever();

        mTask.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override public void onSuccess(Void aVoid) {
                //layoutInput.setVisibility(View.GONE);
                // layoutVerify.setVisibility(View.VISIBLE);
                //  Toast.makeText(LoginActivity.this, "SMS Retriever starts", Toast.LENGTH_LONG).show();

                if(Config.DUPLI_TEST)
                    Log.d("SMS_TEST", "SMS Retriever starts: ");
                send_sms();
            }
        });
        mTask.addOnFailureListener(new OnFailureListener() {
            @Override public void onFailure(@NonNull Exception e) {
                // Toast.makeText(LoginActivity.this, "Error", Toast.LENGTH_LONG).show();

                if(Config.DUPLI_TEST)
                    Log.d("SMS_TEST", "SMS Retriever fails: "+e);
            }
        });
    }

    void send_sms()
    {


        if(FirebaseAuth.getInstance().getCurrentUser()!=null)
        {


            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestIdToken(getString(R.string.default_web_client_id))
                    .requestScopes(new Scope(Scopes.EMAIL),new Scope(Scopes.PROFILE))
                    .requestEmail()
                    .requestProfile()
                    .build();

            GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(getApplicationContext(), gso);

            mGoogleSignInClient.signOut().addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Toast.makeText(LoginActivity.this, "User Signed Out", Toast.LENGTH_SHORT).show();
                    FirebaseAuth.getInstance().signOut();
                    sms_test();
                }
            });
        }
        else {
            sms_test();
        }








    }

    void sms_test()
    {

        Config.OTP = getRandomNumberInRange(1000,9999);
        Config.SMS_MESSAGE = "<#> Your OTP is: "+ Integer.toString(Config.OTP)+" "+Config.SIGNATURE_HASH_TOKEN;
        // Set the service center address if needed, otherwise null.
        String scAddress = null;




        // Set pending intents to broadcast
        // when an_b_message sent and when delivered, or set to null.


        PendingIntent sentIntent = null, deliveryIntent = null;






        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage
                (ccp.getFullNumberWithPlus(), scAddress, Config.SMS_MESSAGE,
                        sentIntent, deliveryIntent);

    }


    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if(Config.DUPLI_TEST)
            Log.d("LOGIN_TEST", requestCode+"::"+resultCode);

        switch (requestCode   )
        {

            case GOOGLE_SIGN_IN :
                IdpResponse response = IdpResponse.fromResultIntent(data);
                if (resultCode == RESULT_OK)
                {





                    if (GoogleSignIn.hasPermissions(
                            GoogleSignIn.getLastSignedInAccount(getApplicationContext()),new Scope(DriveScopes.DRIVE_FILE)))
                    {

                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                        String display_name = user.getDisplayName();

                        Config.USER_NAME = user.getDisplayName();


                        if(Config.DUPLI_TEST)
                            Log.d("LOGIN_TEST", "XXXWith drive Permission "+ user.getDisplayName());
                        //  startActivity(new Intent(this, LoginActivity.class));
                        // finish();
                        if(Config.DUPLI_TEST)
                            Log.d("LOGIN_TEST_D", "onActivityResult: "+ user.getDisplayName());
                        // Toast.makeText(this, "Welcome! " + user.getDisplayName(), Toast.LENGTH_LONG).show();





                        afterLogInwithDrivePermission ();
                        //    onBackPressed();




                    }
                    else
                    {
                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                        String display_name = user.getDisplayName();
                        if(Config.DUPLI_TEST)
                            Log.d("LOGIN_TEST_D", " With No drive Permission "+ user.getDisplayName());
                        signInButton.setVisibility(View.GONE);
                        gobackhome.setVisibility(View.VISIBLE);

                        snack_bar_permission();
                    }




                }
                else
                {
                    Toast.makeText(this, "" + response.getError().getMessage(), Toast.LENGTH_LONG).show();
                    if(Config.DUPLI_TEST)
                        Log.d("SMS_TEST", "error case: "+ response.getError().getMessage());
                }

                break;


            case RC_REQUEST_PERMISSION_SUCCESS_CONTINUE_FILE_CREATION:

                if (resultCode == RESULT_OK)
                {

                    afterLogInwithDrivePermission ();
                }
                else
                {

                    snack_bar_permission();
                }
                break;



            case  RC_SIGN_IN :

                if (resultCode == RESULT_OK)
                {
                    Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                    try
                    {
                        GoogleSignInAccount account = task.getResult(ApiException.class);

                         acct1 = GoogleSignIn.getLastSignedInAccount(getApplicationContext());

                        if(Config.DUPLI_TEST)
                        {
                           // Log.d("DRIVE_UPLOAD_TEST", "path 14 "+"::"+acct1+"::"+acct1.getEmail()+":Display:"+account.getDisplayName());
                        }

                        if (account != null)
                        {
                            if(Config.DUPLI_TEST)
                                Log.d("DRIVE_UPLOAD_TEST", "sign in name ::"+ account.getEmail()+"::"+account.getDisplayName());


                            text_id_heading.setText("Your Google Login is successful and waiting for firebase login ...... ");


                            firebaseAuthWithGoogle(account);
                        }




                    }
                    catch (ApiException e)
                    {
                        if(Config.DUPLI_TEST)
                            Log.d("LOGIN_TEST", "Google sign from google not from FB", e);

                        text_id_heading.setText("Your Google Login is failed ");
                        // setToAfterLoginFailedGoogleUI();
                    }
                }
                else
                {
                    text_id_heading.setText("Your Google Login is failed ");

                }




                break;



        }





    }

    public void firebaseAuthWithGoogle(GoogleSignInAccount acct)
    {

        if(Config.DUPLI_TEST)
            Log.d("GOOGLES_3", "firebaseAuthWithGoogle id:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);


        if(FirebaseAuth.getInstance().getCurrentUser()!=null)
            afterLogInwithDrivePermission ();
        else {
            FirebaseAuth.getInstance().signInWithCredential(credential)
                    .addOnCompleteListener(this, task ->
                    {
                        if (task.isSuccessful())
                        {



                            if (GoogleSignIn.hasPermissions(
                                    GoogleSignIn.getLastSignedInAccount(getApplicationContext()),new Scope(DriveScopes.DRIVE_FILE)))
                            {



                                GoogleSignInAccount acct1 = GoogleSignIn.getLastSignedInAccount(getApplicationContext());

                                if(Config.DUPLI_TEST)
                                {
                                    Log.d("DRIVE_UPLOAD_TEST", "path 15 "+"::"+acct1+"::"+acct1.getEmail());
                                }



                                afterLogInwithDrivePermission ();





                            }
                            else
                            {
                                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                                if(Config.DUPLI_TEST)
                                    Log.d("SMS_TEST", " With No drive Permission "+ user.getDisplayName());
                                signInButton.setVisibility(View.GONE);
                                gobackhome.setVisibility(View.VISIBLE);

                                GoogleSignInAccount acct1 = GoogleSignIn.getLastSignedInAccount(getApplicationContext());

                                if(Config.DUPLI_TEST)
                                {
                                    Log.d("DRIVE_UPLOAD_TEST", "path 15 "+"::"+acct1+"::"+acct1.getEmail());
                                }


                                snack_bar_permission();
                            }




                        }
                        else
                        {


                            text_id_heading.setText("Your Firebase Login is failed ");


                        }
                    });

        }


    }

    void snack_bar_permission()
    {
        text_id_heading.setText("Mond requires Google Drive Sign In for Space share");
        Snackbar.make((  LoginActivity.this).findViewById(android.R.id.content),
                "Mond requires Google Drive Sign In for Space Share",
                Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {

                        GoogleSignIn.requestPermissions(
                                LoginActivity.this,
                                RC_REQUEST_PERMISSION_SUCCESS_CONTINUE_FILE_CREATION,
                                GoogleSignIn.getLastSignedInAccount(getApplicationContext()),
                                new Scope(DriveScopes.DRIVE_FILE),new Scope(Scopes.EMAIL));
                    }
                }).show();
    }


    void afterLogInwithDrivePermission ( )
    {





                        FirebaseFirestore.getInstance().collection("UsersList").document(FirebaseAuth.getInstance().getUid()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>()
                        {
                            @Override
                            public void onComplete(@NonNull Task<DocumentSnapshot> task)
                            {
                                if (task.isSuccessful())
                                {
                                    DocumentSnapshot documentSnapshot = task.getResult();
                                    if (documentSnapshot.exists())
                                    {




                                        String mob = documentSnapshot.getString("mob");

                                        if(mob.equals(ccp.getFullNumberWithPlus().toString()))
                                        {
                                            if(Config.DUPLI_TEST)
                                                Log.d("GOOGLES_14", "Already existing account");

                                            signInButton.setVisibility(View.GONE);
                                            gobackhome.setVisibility(View.VISIBLE);



                                            if(Config.DUPLI_TEST)
                                            {
                                                GoogleSignInAccount acct1 = GoogleSignIn.getLastSignedInAccount(getApplicationContext());
                                                Log.d("DRIVE_UPLOAD_TEST", "path 16 "+"::"+acct1+"::"+acct1.getEmail());
                                            }



                                            FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>()
                                            {
                                                @Override
                                                public void onComplete(@NonNull Task<InstanceIdResult> task)
                                                {
                                                    if (!task.isSuccessful())
                                                    {

                                                        if(Config.DUPLI_TEST)
                                                            Log.d("IDCASE", "getInstanceId failed", task.getException());
                                                        return;
                                                    }

                                                    // Get new Instance ID token
                                                    String  token = task.getResult().getToken();


                                                    HashMap<String, Object> userMap = new HashMap<>();


                                                    if(Config.DUPLI_TEST)
                                                    {
                                                        GoogleSignInAccount acct1 = GoogleSignIn.getLastSignedInAccount(getApplicationContext());
                                                        Log.d("DRIVE_UPLOAD_TEST_C", "path 20 "+"::"+acct1+"::"+acct1.getEmail()+"::"+acct1.getDisplayName());
                                                    }



                                                    userMap.put("online", true);
                                                    userMap.put("time_stamp",   FieldValue.serverTimestamp());
                                                    userMap.put("device_token", token);







                                                    String user_id = FirebaseAuth.getInstance().getCurrentUser().getUid();

                                                    WriteBatch batch =  FirebaseFirestore.getInstance().batch();
                                                    UserListReference = FirebaseFirestore.getInstance().collection("UsersList").document(user_id);


                                                    batch.update(UserListReference, userMap);


                                                    batch.commit().addOnSuccessListener(new OnSuccessListener<Void>()
                                                    {
                                                        @Override
                                                        public void onSuccess(Void aVoid)
                                                        {


                                                            signInButton.setVisibility(View.GONE);
                                                            gobackhome.setVisibility(View.VISIBLE);

                                                            text_id_heading.setText("You have successfully login in to your account. And you will be redirected to Home page ");

                                                            if(Config.DUPLI_TEST)
                                                            {
                                                                GoogleSignInAccount acct1 = GoogleSignIn.getLastSignedInAccount(getApplicationContext());
                                                                Log.d("DRIVE_UPLOAD_TEST", "path 17 "+"::"+acct1+"::"+acct1.getEmail());
                                                            }



                                                        }


                                                    }).addOnFailureListener(new OnFailureListener()
                                                    {
                                                        @Override
                                                        public void onFailure(@NonNull Exception e)
                                                        {


                                                            text_id_heading.setText("UnSuccessful Login database entry issue");
                                                            if(Config.DUPLI_TEST)
                                                            {
                                                                GoogleSignInAccount acct1 = GoogleSignIn.getLastSignedInAccount(getApplicationContext());
                                                                Log.d("DRIVE_UPLOAD_TEST", "path 18 "+"::"+acct1+"::"+acct1.getEmail());
                                                            }

                                                            signoutCase();

                                                        }
                                                    });





                                                }
                                            });




                                        }
                                        else
                                        {


                                            text_id_heading.setText("Email has registered with another number");
                                            signoutCase();
                                        }




                                    }
                                    else
                                    {



                                        FirebaseFirestore.getInstance().collection("UsersList").whereEqualTo("mob", ccp.getFullNumberWithPlus()).get()
                                                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>()
                                                {
                                                    @Override
                                                    public void onSuccess(QuerySnapshot queryDocumentSnapshots)
                                                    {


                                                        if (queryDocumentSnapshots.size() > 0)
                                                        {

                                                            text_id_heading.setText("This number already taken");
                                                            if(Config.DUPLI_TEST)
                                                            {
                                                                GoogleSignInAccount acct1 = GoogleSignIn.getLastSignedInAccount(getApplicationContext());
                                                                Log.d("DRIVE_UPLOAD_TEST", "path 19 "+"::"+acct1+"::"+acct1.getEmail());
                                                            }

                                                            signoutCase();


                                                        }
                                                        else
                                                        {





                                                            FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                                                                @Override
                                                                public void onComplete(@NonNull Task<InstanceIdResult> task) {
                                                                    if (!task.isSuccessful()) {

                                                                        if(Config.DUPLI_TEST)
                                                                            Log.d("IDCASE", "getInstanceId failed", task.getException());
                                                                        return;
                                                                    }

                                                                    // Get new Instance ID token
                                                                    String  token = task.getResult().getToken();


                                                                    if(Config.DUPLI_TEST)
                                                                    {
                                                                        GoogleSignInAccount acct1 = GoogleSignIn.getLastSignedInAccount(getApplicationContext());
                                                                        Log.d("DRIVE_UPLOAD_TEST_C", "path 20 "+"::"+acct1+"::"+acct1.getEmail()+"::"+acct1.getDisplayName());
                                                                    }


                                                                    HashMap<String, Object> userMap = new HashMap<>();


                                                                    // Name, email address, and profile photo Url
                                                                    String name  =null;
                                                                    String email ;
                                                                    Uri photoUrl ;


                                                                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                                                                    if (user != null) {
                                                                        for (UserInfo profile : user.getProviderData()) {
                                                                            // Id of the provider (ex: google.com)
                                                                            String providerId = profile.getProviderId();

                                                                            // UID specific to the provider
                                                                            String uid = profile.getUid();


                                                                            // Name, email address, and profile photo Url
                                                                              name = profile.getDisplayName();
                                                                              email = profile.getEmail();
                                                                              photoUrl = profile.getPhotoUrl();



                                                                        }
                                                                    }

                                                                    userMap.put("mob",  ccp.getFullNumberWithPlus());
                                                                    userMap.put("id",  FirebaseAuth.getInstance().getCurrentUser().getUid());
                                                                    userMap.put("status", "Hi there I'm using Mond App.");
                                                                    userMap.put("thumb_image", "default");
                                                                    userMap.put("online", true);
                                                                    userMap.put("time_stamp",   FieldValue.serverTimestamp());
                                                                    userMap.put("device_token", token);
                                                                    userMap.put("email", FirebaseAuth.getInstance().getCurrentUser().getEmail());
                                                                    userMap.put("name",name);












                                                                            HashMap<String, Object> FrduserMap = new HashMap<>();


                                                                            FrduserMap.put("id",  FirebaseAuth.getInstance().getCurrentUser().getUid());
                                                                            FrduserMap.put("block",  false);
                                                                            FrduserMap.put("time_stamp",   FieldValue.serverTimestamp());




                                                                            String user_id = FirebaseAuth.getInstance().getCurrentUser().getUid();

                                                                            WriteBatch batch =  FirebaseFirestore.getInstance().batch();
                                                                            UserListReference = FirebaseFirestore.getInstance().collection("UsersList").document(user_id);
                                                                            FriendListReference = FirebaseFirestore.getInstance().collection("Friends").document(user_id).collection("MyFriends").document(user_id);


                                                                            batch.set(UserListReference, userMap);
                                                                            batch.set(FriendListReference, FrduserMap);

                                                                            batch.commit().addOnSuccessListener(new OnSuccessListener<Void>() {
                                                                                @Override
                                                                                public void onSuccess(Void aVoid) {

                                                                                    text_id_heading.setText("Successful Login");

                                                                                    signInButton.setVisibility(View.GONE);
                                                                                    gobackhome.setVisibility(View.VISIBLE);

                                                                                    if(Config.DUPLI_TEST)
                                                                                    {
                                                                                        GoogleSignInAccount acct1 = GoogleSignIn.getLastSignedInAccount(getApplicationContext());
                                                                                        Log.d("DRIVE_UPLOAD_TEST", "path 20 "+"::"+acct1+"::"+acct1.getEmail());
                                                                                    }

                                                                                    text_id_heading.setText("Congratulation Successful  first time Login. You will be directed to the home page");






                                                                                }


                                                                            }).addOnFailureListener(new OnFailureListener() {
                                                                                @Override
                                                                                public void onFailure(@NonNull Exception e) {


                                                                                    text_id_heading.setText("UnSuccessful Login database entry issue");
                                                                                    gobackhome(null);
                                                                                    if(Config.DUPLI_TEST)
                                                                                    {
                                                                                        GoogleSignInAccount acct1 = GoogleSignIn.getLastSignedInAccount(getApplicationContext());
                                                                                        Log.d("DRIVE_UPLOAD_TEST", "path 21 "+"::"+acct1+"::"+acct1.getEmail());
                                                                                    }

                                                                                    signoutCase();

                                                                                }
                                                                            });












                                                                }
                                                            });




                                                        }



                                                    }
                                                });



                                    }
                                }
                                else
                                {
                                    if(Config.TEST)
                                        Log.d("GSINGIN", "Failed with: ", task.getException());



                                    text_id_heading.setText("Network error");

                                }
                            }
                        });








    }

}



