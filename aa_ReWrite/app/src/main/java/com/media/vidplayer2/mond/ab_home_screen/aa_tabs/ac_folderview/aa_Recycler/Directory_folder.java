package com.media.vidplayer2.mond.ab_home_screen.aa_tabs.ac_folderview.aa_Recycler;

public class Directory_folder {


    public String file_path;
    public String size;

    public String getFile_path() {
        return file_path;
    }

    public void setFile_path(String file_path) {
        this.file_path = file_path;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public String file_name;

    public Directory_folder(String file_path, String size, String file_name) {
        this.file_path = file_path;
        this.size = size;
        this.file_name = file_name;
    }



}
