package com.media.vidplayer2.mond.zg_contactlist;

import java.util.Date;

public class UsersData {

    public UsersData() {
    }

    public UsersData(String mob, String id, String status, String thumb_image, String device_token, String email, String name, Boolean online, Date time_stamp) {
        this.mob = mob;
        this.id = id;
        this.status = status;
        this.thumb_image = thumb_image;
        this.device_token = device_token;
        this.email = email;
        this.name = name;
        this.online = online;
        this.time_stamp = time_stamp;
    }

    String mob,id,status,thumb_image,device_token,email,name;

    public String getMob() {
        return mob;
    }

    public void setMob(String mob) {
        this.mob = mob;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getThumb_image() {
        return thumb_image;
    }

    public void setThumb_image(String thumb_image) {
        this.thumb_image = thumb_image;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getOnline() {
        return online;
    }

    public void setOnline(Boolean online) {
        this.online = online;
    }

    public Date getTime_stamp() {
        return time_stamp;
    }

    public void setTime_stamp(Date time_stamp) {
        this.time_stamp = time_stamp;
    }

    Boolean online;
    Date time_stamp;


}