
package com.media.vidplayer2.mond.ad_Profile;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.api.client.googleapis.media.MediaHttpUploader;
import com.google.api.client.googleapis.media.MediaHttpUploaderProgressListener;
import com.google.api.client.http.InputStreamContent;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.model.Permission;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.media.vidplayer2.mond.zb_Config.Config;
import com.media.vidplayer2.mond.zf_space_share.uploadSpace.GoogleDriveFileHolder;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


public class FileManagerProfile
{
    private  Executor mExecutor = Executors.newSingleThreadExecutor();
    private  Drive mDriveService=null;
    public MyProfileActivity myProfileActivity =null;
    public final Runnable[] runnable = new Runnable[5];
    FileList result=null;
    File file;
    private  String foldername ="MondPlayerSystemFiles";
     private  Integer tokenVal;

    public FileManagerProfile(Drive mDriveService, Context context, Integer tokenVal) {
        this.mDriveService = mDriveService;

        myProfileActivity = (MyProfileActivity)context;

        this.tokenVal=tokenVal;


        runnable[0]=new Runnable() {
            public void run() {


/*

                myProfileActivity.statU.setVisibility(View.VISIBLE);
                myProfileActivity.percentage_size_u.setText( "Uploading to Spaceshare  ");
                myProfileActivity.speed_download_u.setText("Started");
                */

                if(Config.DUPLI_TEST)
                    Log.d("DRIVE_UPLOAD_TEST", "Running 1");

            }
        };



        runnable[1]=new Runnable() {
            public void run() {

                /*

                myProfileActivity.progress_circular_space_u.setVisibility(View.GONE);
                myProfileActivity.text_id_space_u.setText("Failed  file creation");
                if(Config.DUPLI_TEST)
                    Log.d("DRIVE_UPLOAD_TEST", "Running 1");
                    */

            }
        };



        runnable[2]=new Runnable() {
            public void run() {

                /*
                if(Config.DUPLI_TEST)
                    Log.d("DRIVE_UPLOAD_TEST", "Running 2");
                myProfileActivity.statU.setVisibility(View.GONE);
                myProfileActivity.text_id_space_u.setText("Upload Finished.Open Space share");
                */
            }


        };



        runnable[3]=new Runnable() {
            public void run() {
                // UI code goes here


                if(Config.DUPLI_TEST)
                    Log.d("DRIVE_UPLOAD_TEST", "Running 3");
/*
                myProfileActivity.statU.setVisibility(View.GONE);

                myProfileActivity.text_id_space_u.setText("Upload Failed.Try again");
                */
                if(Config.DUPLI_TEST)
                    Log.d("DRIVE_UPLOAD_TEST", "Download failure   activity   existing");



            }
        };



    }


    class CustomProgressListenerU implements MediaHttpUploaderProgressListener {

        Uri uri=null;

        java.io.File file;
        double first_numBytes =0,second_numBytes=0;
        double first_time =0,second_time=0;
        double download_speed=0;








        public CustomProgressListenerU(Uri uri) {
            this.uri = uri;

            file = new java.io.File(uri.getPath().toString());


        }


        void Progress(MediaHttpUploader uploader) throws InterruptedException {


            second_numBytes = uploader.getNumBytesUploaded();
            second_time = getCurrentTimeInseconds() ;

            download_speed = toMB((second_numBytes - first_numBytes)/ (second_time - first_time));

            double percent = (((double) uploader.getNumBytesUploaded() / (double) file.length()) * 100);


            double percentRoundOff = roundTwo(percent);
            double download_speedRoundOff = roundTwo(download_speed);


            if (Config.DUPLI_TEST)
                Log.d("DRIVE_UPLOAD_TEST", "_#download Progress :: " + file.length() + "::" + uploader.getNumBytesUploaded() + "::" + percentRoundOff + "% :: " + download_speedRoundOff);




                runnable[4]=new Runnable() {
                    public void run() {
                        // UI code goes here

                        if(Config.DUPLI_TEST)
                            Log.d("DRIVE_UPLOAD_TEST", "Running 4");
/*
                        myProfileActivity.statU.setVisibility(View.VISIBLE);
                        myProfileActivity.percentage_size_u.setText(percentRoundOff + "%   (" + uploader.getNumBytesUploaded() / (1024 * 1024) + "/" + file.length() / (1024 * 1024) + " bytes)");
                        myProfileActivity.speed_download_u.setText(download_speedRoundOff + " MB/second");
                        */
                    }
                };

                Config.semaphoreUpload.acquire();

                if(Config.KeyExistUpload(tokenVal.toString()))
                {

                    myProfileActivity.handler.post(runnable[4]);

                }
                Config.semaphoreUpload.release();


            first_numBytes = second_numBytes;
            first_time = second_time;




        }


        @Override
        public void progressChanged(MediaHttpUploader uploader) throws IOException {
            switch (uploader.getUploadState())
            {
                case MEDIA_IN_PROGRESS:

                    try {
                        Progress(uploader);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    break;
                case MEDIA_COMPLETE:
                    if(Config.DUPLI_TEST)
                        Log.d("DRIVE_UPLOAD_TEST", "_#upload Complete :: "+ "::"+uploader.getNumBytesUploaded());
                    break;

                case INITIATION_STARTED:

                    break;

                case INITIATION_COMPLETE:
                    first_numBytes = 0;
                    first_time = getCurrentTimeInseconds();
                    if(Config.DUPLI_TEST)
                        Log.d("DRIVE_UPLOAD_TEST", "_#iNITIAL :: "+ "::"+uploader.getNumBytesUploaded());
                    break;





            }
        }
    }








    public double getCurrentTimeInseconds()
    {

        long timeInMilliSeconds = TimeUnit.MILLISECONDS.toSeconds(new Date().getTime());



        return (double)timeInMilliSeconds;
    }

    public double roundTwo(double val)
    {


        return (double)Math.round(val * 100.0) / 100.00;
    }

    public double toMB(double val)
    {


        return (double)val/(1024*1024);
    }



////////////////////////////////////////////////////////////////////////////////////////////////////

    public Task<File> SearchProfilePic( String id )
    {
        return Tasks.call(mExecutor, () -> {
            if(Config.DUPLI_TEST)
                Log.d("DRIVE_UPLOAD_TEST", " Inside Search Pic "+ id );

            //  File file = mDriveService.files().get(id).getFileId()




            file =mDriveService.files().get(id).setFields("*").execute();


            return file;
        }).addOnSuccessListener(new OnSuccessListener<File>() {
            @Override
            public void onSuccess(File file) {

                if(Config.DUPLI_TEST)
                    Log.d("DRIVE_UPLOAD_TEST", " Inside Search Pic sucess "+file.getName()+"::"+file.getId()+file.getThumbnailLink() );


                try {
                    Config.semaphoreprofileUpload.acquire();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }


                if(Config.DUPLI_TEST)
                    Log.d("DRIVE_UPLOAD_TEST", "2a"+tokenVal.toString());
                try {
                    if(Config.KeyExistprofileUpload(tokenVal.toString())) {
                        if(Config.DUPLI_TEST)
                            Log.d("DRIVE_UPLOAD_TEST", "2ab"+tokenVal.toString());
                        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                            @Override
                            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                                if (!task.isSuccessful()) {

                                    if(Config.DUPLI_TEST)
                                        Log.d("IDCASE", "getInstanceId failed", task.getException());
                                    return;
                                }

                                // Get new Instance ID token
                                String  token = task.getResult().getToken();


                                HashMap<String, Object> userMap = new HashMap<>();

                                userMap.put("thumb_image", file.getWebContentLink());



                                String user_id = myProfileActivity.mAuth.getCurrentUser().getUid();


                                myProfileActivity.mFirestore.collection("UsersList").document(user_id)
                                        .update(userMap).addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {



                                        if(Config.DUPLI_TEST)
                                            Log.d("GOOGLES_12", "New phone added for new gmail in firebase");


                                      myProfileActivity.loadingButton.showSuccess();
                                    }
                                });


                            }
                        });


                        if(Config.DUPLI_TEST)
                            Log.d("DRIVE_UPLOAD_TEST", "2ac"+tokenVal.toString());

                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if(Config.DUPLI_TEST)
                    Log.d("DRIVE_UPLOAD_TEST", "2b");


                Config.semaphoreprofileUpload.release();












            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {


                if(Config.DUPLI_TEST)
                    Log.d("DRIVE_UPLOAD_TEST", " Inside Search Pic failed "+e );



            }
        });
    }



    public Task<File> uploadFilePart(String name, Uri uri, String mime_type, String folderId, Bitmap bitmap )
    {


        return Tasks.call(mExecutor, () -> {



            if(Config.DUPLI_TEST)
                Log.d("DRIVE_UPLOAD_TEST", "1 "+tokenVal.toString());


            Config.semaphoreprofileUpload.acquire();



            if(Config.DUPLI_TEST)
                Log.d("DRIVE_UPLOAD_TEST", "2a"+tokenVal.toString());
            if(Config.KeyExistprofileUpload(tokenVal.toString())) {
                if(Config.DUPLI_TEST)
                    Log.d("DRIVE_UPLOAD_TEST", "2ab"+tokenVal.toString());

                myProfileActivity.handler.post(runnable[0]);
                if(Config.DUPLI_TEST)
                    Log.d("DRIVE_UPLOAD_TEST", "2ac"+tokenVal.toString());

            }
            if(Config.DUPLI_TEST)
                Log.d("DRIVE_UPLOAD_TEST", "2b");


            Config.semaphoreprofileUpload.release();



            if(Config.DUPLI_TEST)
                Log.d("DRIVE_UPLOAD_TEST", "2c");

            if(Config.DUPLI_TEST)
                Log.d("DRIVE_UPLOAD_TEST", "2");


            List<String> root;
            if (folderId == null) {
                root = Collections.singletonList("root");
            } else {

                root = Collections.singletonList(folderId);
            }



            File metadata = new File()
                    .setParents(root)
                    .setMimeType(mime_type)
                    .setName(name);

            if(Config.DUPLI_TEST)
                Log.d("DRIVE_UPLOAD_TEST", "3");





             InputStream targetStream = new FileInputStream(uri.getPath());
            ///  InputStream targetStream = new FileInputStream(_context.getContentResolver().openFileDescriptor(uri, "r").getFileDescriptor());
            InputStreamContent inputStreamContent = new InputStreamContent(mime_type, targetStream);
            Drive.Files.Create create =  mDriveService.files().create(metadata, inputStreamContent);
            MediaHttpUploader uploader = create.getMediaHttpUploader();
            uploader.setDirectUploadEnabled(false);

            uploader.setChunkSize(MediaHttpUploader.DEFAULT_CHUNK_SIZE);
            uploader.setProgressListener(new CustomProgressListenerU(uri));


            File googleFile = create.execute();

            Permission permission = new Permission();
            permission.setRole("reader");
            permission.setType("anyone");

            mDriveService.permissions().create(googleFile.getId(),permission).execute();

         //   mDriveService.set===


/*
            mDriveService.permissions.create({
                    fileId: '......',
                    requestBody: {
                role: 'reader',
                        type: 'anyone',
            }
});

/*

            Get the webLinkView value using:

const webViewLink = await drive.files.get({
                    fileId: file.id,
                    fields: 'webViewLink'
}).then(response =>
                    response.data.webViewLink
);
*/



            if(Config.DUPLI_TEST)
                Log.d("DRIVE_UPLOAD_TEST", "4");


            if (googleFile == null)
            {

                if(Config.DUPLI_TEST)
                    Log.d("DRIVE_UPLOAD_TEST", "File creation in upload failed");


                Config.semaphoreprofileUpload.acquire();

                if(Config.KeyExistprofileUpload(tokenVal.toString()))
                    myProfileActivity.handler.post(runnable[1]);
                Config.semaphoreprofileUpload.release();


                throw new IOException("Null result when requesting file creation.");
            }

            else

            {
                if(Config.DUPLI_TEST)
                    Log.d("DRIVE_UPLOAD_TEST", "nw file in drive"+googleFile.getMimeType()+"::"+googleFile.getName()+"::");

            }
            return googleFile ;
        }).addOnSuccessListener(new OnSuccessListener<File>() {
            @Override
            public void onSuccess(File file) {

                if(Config.DUPLI_TEST)
                    Log.d("DRIVE_UPLOAD_TEST", " Upload  success   :: 6 a " );

                try {
                    Config.semaphoreprofileUpload.acquire();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                try {
                    if(Config.KeyExistprofileUpload(tokenVal.toString()))
                        myProfileActivity.handler.post( runnable[2] );
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                Config.semaphoreprofileUpload.release();
                if(Config.DUPLI_TEST)
                    Log.d("DRIVE_UPLOAD_TEST", " Upload  success   :: 6 b " );


                if(Config.DUPLI_TEST)
                    Log.d("DRIVE_UPLOAD_TEST", " Inside Search Pic sucess "+file.getName()+"::"+file.getId()+file.getThumbnailLink() );


                SearchProfilePic(  file.getId());



            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                if(Config.DUPLI_TEST)
                    Log.d("DRIVE_UPLOAD_TEST", " Upload  Failure   :: 6 "+tokenVal+"::"+e );


                try {
                    Config.semaphoreUpload.acquire();
                } catch (InterruptedException e2) {
                    e2.printStackTrace();

                    if(Config.DUPLI_TEST)
                        Log.d("DRIVE_UPLOAD_TEST", " Upload  Failure   :: 6 "+e2+tokenVal );

                }


                try {
                    if(Config.KeyExistUpload(tokenVal.toString()))
                        myProfileActivity.handler.post(runnable[3]);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }

                Config.semaphoreUpload.release();





            }
        });
    }



    public Task<String> deleteFolder(String name, Uri uri, String mime_type, Bitmap bitmap,String fid )
    {
        return Tasks.call(mExecutor, () -> {


            mDriveService.files().delete(fid).execute();



            return fid;
        }).addOnSuccessListener(new OnSuccessListener<String>() {
            @Override
            public void onSuccess(String s) {


                if(Config.DUPLI_TEST)
                    Log.d("DRIVE_UPLOAD_TEST", " Folder creation success   :: 5 " );
                createFolder(  name,   uri,   mime_type,bitmap);

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {



                if(Config.DUPLI_TEST)
                    Log.d("DRIVE_UPLOAD_TEST", " Folder creation failed   :: 4 " );


                try {
                    Config.semaphoreprofileUpload.acquire();
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }

                try {
                    if(Config.KeyExistprofileUpload(tokenVal.toString()))
                        myProfileActivity.handler.post(runnable[3]);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }

                Config.semaphoreprofileUpload.release();


            }
        });
    }


    public Task<String> createFolder(String name, Uri uri, String mime_type, Bitmap bitmap )
    {
        return Tasks.call(mExecutor, () -> {

            File fileMetadata = new File();
            fileMetadata.setName(foldername);
            fileMetadata.setMimeType("application/vnd.google-apps.folder");

            file = mDriveService.files().create(fileMetadata)
                    .setFields("id")
                    .execute();
            return file.getId();
        }).addOnSuccessListener(new OnSuccessListener<String>() {
            @Override
            public void onSuccess(String s) {


                if(Config.DUPLI_TEST)
                    Log.d("DRIVE_UPLOAD_TEST", " Folder creation success   :: 5 " );

                uploadFilePart(  name,   uri,   mime_type,    file.getId(),bitmap);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {



                if(Config.DUPLI_TEST)
                    Log.d("DRIVE_UPLOAD_TEST", " Folder creation failed   :: 4 " );


                try {
                    Config.semaphoreprofileUpload.acquire();
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }

                try {
                    if(Config.KeyExistprofileUpload(tokenVal.toString()))
                        myProfileActivity.handler.post(runnable[3]);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }

                Config.semaphoreprofileUpload.release();


            }
        });
    }

    public Task<GoogleDriveFileHolder> UploadTotalOperation_Dir_Upload(String name, Uri uri, String mime_type, Bitmap bitmap   )
    {

        return Tasks.call(mExecutor, () -> {

            result=null;

            result = mDriveService.files().list()
                    .setQ("mimeType = '" + "application/vnd.google-apps.folder"+ "' and name = '" + foldername + "' ")
                    .setSpaces("drive")
                    .execute();
            GoogleDriveFileHolder googleDriveFileHolder = new GoogleDriveFileHolder();
            if (result.getFiles().size() > 0)
            {
                googleDriveFileHolder.setFile(result.getFiles().get(0));




            }
            return googleDriveFileHolder;
        }).addOnSuccessListener(new OnSuccessListener<GoogleDriveFileHolder>() {
            @Override
            public void onSuccess(GoogleDriveFileHolder googleDriveFileHolder) {



                if (!(result.getFiles().size() > 0) )
                {
                    if(Config.DUPLI_TEST)
                        Log.d("DRIVE_UPLOAD_TEST", " Folder already not exists   :: 2 " );

                    createFolder(  name,   uri,   mime_type,bitmap);


                }
                else
                {


                    if(Config.DUPLI_TEST)
                        Log.d("DRIVE_UPLOAD_TEST", " Folder already   exists   :: 3 "+ name+"::"+uri.toString()+"::"+mime_type+"::"+ googleDriveFileHolder.getFile().getId() );
                    deleteFolder(  name,   uri,   mime_type,   bitmap,googleDriveFileHolder.getFile().getId());
                   // uploadFilePart(  name,   uri,   mime_type,    googleDriveFileHolder.getFileSize().getId(),bitmap);
                  //  String name, Uri uri, String mime_type, Bitmap bitmap,String fid
                }

            }

        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {



                if(Config.DUPLI_TEST)
                    Log.d("DRIVE_UPLOAD_TEST", " Folder creation/search failed   :: 1 "+e);


                try {
                    Config.semaphoreprofileUpload.acquire();
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }

                try {
                    if(Config.KeyExistprofileUpload(tokenVal.toString()))
                        myProfileActivity.handler.post(runnable[3]);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }

                Config.semaphoreprofileUpload.release();


            }
        });

    }






}
