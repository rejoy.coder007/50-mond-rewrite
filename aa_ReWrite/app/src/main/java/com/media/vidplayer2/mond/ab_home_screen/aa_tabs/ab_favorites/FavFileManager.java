package com.media.vidplayer2.mond.ab_home_screen.aa_tabs.ab_favorites;

import android.content.Context;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.media.vidplayer2.mond.ab_home_screen.HomeActivity;
import com.media.vidplayer2.mond.ab_home_screen.aa_tabs.ab_favorites.aa_Recycler.DirectoryFav;
import com.media.vidplayer2.mond.ab_home_screen.aa_tabs.ab_favorites.aa_Recycler.RecyclerAdapterFav;
import com.media.vidplayer2.mond.zb_Config.Config;
import com.media.vidplayer2.mond.zc_VideoFileParser.VideoFileParser;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.CyclicBarrier;

public class FavFileManager {

    ///Fav Fragments
    Handler Fav_Frag_directory_handler = null;
    public RecyclerView Fav_Frag_recyclerView = null;
    public RecyclerAdapterFav Fav_Frag_recyclerAdapter = null;
    public SwipeRefreshLayout Fav_Frag_mSwipeRefreshLayout = null;
    public boolean FAV_FRAG_READY_TO_UPDATE = false;

    Context context = null;

    public FavFileManager(Context context) {
        this.context = context;
        Fav_Frag_directory_handler = new Handler(Looper.myLooper());
    }




    public void Fav_Frag_updateUIRecycler()
    {

        Runnable r = new Runnable() {
            @Override
            public void run() {


                if (Config.TEST)
                    Log.d("_#YESCASE_YY", "5 -- "+ VideoFileParser.getInstance().total_SD_MEM);


                if (Config.TEST)
                {
                    for (int x = 0; x <  VideoFileParser.getInstance().total_SD_MEM.size(); x++)
                    {
                        DirectoryFav directoryFav = VideoFileParser.getInstance().total_SD_MEM.get(x);
                        Log.d("_#YESCASE_YY_@@",directoryFav.dir_name+":**:"+directoryFav.path+"::"+directoryFav.video_count);

                    }

                }




                if (Config.TIME_TEST)
                {
                    if(VideoFileParser.getInstance().total_SD_MEM==null)
                    {
                        Log.d("NULL_FINDER_Array_TIME",  "null at MainActivity_Start.mainActivityStartS._fileManager_mainActivity.total_SD_MEM");
                    }
                    else
                    {
                        Log.d("NULL_FINDER_Array_TIME",  "not null total_SD_MEM");

                    }


                    if( Fav_Frag_recyclerAdapter ==null)
                    {
                        Log.d("NULL_FINDER_ADT_TIME",  "Null pointer  adapter");
                    }
                    else{
                        Log.d("NULL_FINDER_ADT_TIME",  "not null adapter");

                    }




                }


                Fav_Frag_recyclerAdapter.setRecyclerAdapter(VideoFileParser.getInstance().total_SD_MEM, context);
                Fav_Frag_recyclerAdapter.notifyItemRangeChanged(0,VideoFileParser.getInstance().total_SD_MEM.size());
                Fav_Frag_recyclerAdapter.notifyDataSetChanged();

                Config.FIRST_TIME_RUN=false;



                if (Config.TEST)
                    Log.d("_#YESCASE_YY", "6");

            }
        };

        ///Block here
        while (!FAV_FRAG_READY_TO_UPDATE) ;


        if (Config.TEST)
            Log.d("_#YESCASE_YY", "7");

        if (!VideoFileParser.THREAD_STOPPER)
        {
            Fav_Frag_directory_handler.post(r);
        }
        else
        {
            Fav_Frag_directory_handler.removeCallbacksAndMessages(null);
        }


        if (Config.TEST)
            Log.d("_#YESCASE_YY", "8");


    }

    /////////////////////////////////////////////////////////////////

    public void Fav_Frag_ReadDirByThread()
    {

        VideoFileParser.getInstance().names_fav_MEM.clear();
        VideoFileParser.getInstance().names_fav_SD.clear();
      //  File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath());
         File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath());

        if(Config.DUPLI_TEST)
            Log.d("EXTERNAL_PATH", "Fav_Frag_ReadDirByThread: "+Environment.getExternalStorageState().toString());

       // File dir1 = new File(Environment.getExternalStorageDirectory().getAbsolutePath());

        Runnable barrierAction = new Runnable() { public void run()
        {


            VideoFileParser.getInstance().total_SD_MEM = new ArrayList<>(VideoFileParser.getInstance().names_fav_MEM.size() + VideoFileParser.getInstance().names_fav_SD.size());
            VideoFileParser.getInstance().total_SD_MEM.addAll(VideoFileParser.getInstance().names_fav_MEM);
            //total_SD_MEM.addAll(names_fav_SD);
           ((HomeActivity) context).fileManager_homeActivity.favFileManager.Fav_Frag_updateUIRecycler();
       }};


        CyclicBarrier barrier = new CyclicBarrier(2, barrierAction);
        VideoFileParser.getInstance().MakeNewThreadsForIntenralAndExternalDirReading();
        VideoFileParser.getInstance().threadForStorageReadDirectoryI.SetThreadValue(dir,"Thread_1",barrier);
        VideoFileParser.getInstance().threadForStorageReadDirectoryE.SetThreadValue(dir,"Thread_2",barrier);
        VideoFileParser.getInstance().threadForStorageReadDirectoryI.start();
        VideoFileParser.getInstance().threadForStorageReadDirectoryE.start();



    }


}
