package com.leak.solver.Mond.ab_home_page.aa_tabs.ab_favorites.aa_Recycler;

import android.content.Context;
import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import com.leak.solver.Mond.R;
import com.leak.solver.Mond.ab_home_page.aa_activity_home_screen;
import com.leak.solver.Mond.zz_Config;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapterFav extends RecyclerView.Adapter<ViewHolderFav> {

    private List<DirectoryFav> directories;
    private Context context;



    public RecyclerAdapterFav(List<DirectoryFav> directories, Context context)
    {
        this.directories = new ArrayList<>();
        this.directories = directories;
        this.context = context;


    }

    public void setRecyclerAdapter(List<DirectoryFav> directories, Context context)
    {
        this.directories = directories;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolderFav onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {


        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.ab_d_slice_frag_fav_recycler_item, viewGroup, false);


        return new ViewHolderFav(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderFav viewHolderFav, final int i)
    {


        DirectoryFav directoryFav = this.directories.get(i);
        viewHolderFav.video_count.setText(directoryFav.video_count);
        final aa_activity_home_screen aa_activity_home_screen = ((aa_activity_home_screen)context);
        viewHolderFav.dir_name.setText(directoryFav.dir_name+"_"+i);


   ////     List<String> values = new ArrayList<String>();
       // values = aa_activity_home_screen._global_StartUP.ab_fileManagerStart.map.get(aa_activity_home_screen._global_StartUP.ab_fileManagerStart.names_fav.get(i));

        Integer i5 = new Integer(i);
        Integer i6 = new Integer(0);
        if(i==0)
        {


            viewHolderFav.dir_name.setTextColor(Color.parseColor("#008000"));
          //   Log.d("#__INDEX", aa_activity_home_screen._global_StartUP.ab_fileManagerStart.names_fav.get(i)+" : "+i+":"+ values.get(0)+"  "+i5+":"+i6+";"+i5.equals(i6));

        }
        else
        {

            viewHolderFav.dir_name.setTextColor(Color.parseColor("#000000"));
        }


        viewHolderFav.slice.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {



                List<String> values = new ArrayList<String>();

             //   values = ((aa_activity_home_screen) context)._global_StartUP.ab_fileManagerStart.names_fav.get(i);

                DirectoryFav directoryFav1 =  ((aa_activity_home_screen) context).fileManager_home_fav.directories.get(i);

               // Paramter_Parcelable paramter_parcelable=null;
             //   paramter_parcelable = new Paramter_Parcelable(values.get(0),values.get(1));
              //  ((MyApplication) (MyApplication.getContext())).map.clear();
           //     ((MyApplication) (MyApplication.getContext())).names_fav.clear();

                if(zz_Config.TEST)
                {
                    Toast.makeText(context, "Item " + (i ) + " clicked  :: "+directoryFav1.path,
                            Toast.LENGTH_SHORT).show();

                  //  Log.d("_##YES", "onClick: "+values.get(0));


                }

                File file = new File(directoryFav1.path);
                ((aa_activity_home_screen) context).fileManager_home_folder.ReadDirByThread_folder( ((aa_activity_home_screen) context),file);
                ((aa_activity_home_screen) context).fileManager_home.disable_tab(1,2);
                ((aa_activity_home_screen) context).fileManager_home.CURRENT_TAB=2;




               // ((MyApplication) (MyApplication.getContext())).StartThread_files(values.get(0).toString());

               // ViewGroup slidingTabStrip =  (ViewGroup) (MyApplication.getContext()).tabLayout.getChildAt(0);

   /*
                for (int i = 0; i <  (MyApplication.getContext()).tabLayout.getTabCount(); i++) {
                    View tab = slidingTabStrip.getChildAt(i);
                    LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) tab.getLayoutParams();
                    if(i==1)
                        layoutParams.weight = 0;
                    else
                        layoutParams.weight = 1;
                    tab.setLayoutParams(layoutParams);


                }
    */

            }
        });



        viewHolderFav.slice1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {



                List<String> values = new ArrayList<String>();

                //   values = ((aa_activity_home_screen) context)._global_StartUP.ab_fileManagerStart.names_fav.get(i);

                DirectoryFav directoryFav1 =  ((aa_activity_home_screen) context).fileManager_home_fav.directories.get(i);

                // Paramter_Parcelable paramter_parcelable=null;
                //   paramter_parcelable = new Paramter_Parcelable(values.get(0),values.get(1));
                //  ((MyApplication) (MyApplication.getContext())).map.clear();
                //     ((MyApplication) (MyApplication.getContext())).names_fav.clear();

                if(zz_Config.TEST)
                {
                    Toast.makeText(context, "Item " + (i ) + " clicked  :: "+directoryFav1.path,
                            Toast.LENGTH_SHORT).show();

                    //  Log.d("_##YES", "onClick: "+values.get(0));


                }

                File file = new File(directoryFav1.path);
                ((aa_activity_home_screen) context).fileManager_home.Thread_Kiler();

                ((aa_activity_home_screen) context).fileManager_home_folder.ReadDirByThread_folder( ((aa_activity_home_screen) context),file);
                ((aa_activity_home_screen) context).fileManager_home.disable_tab(1,2);
                ((aa_activity_home_screen) context).fileManager_home.CURRENT_TAB=2;




                // ((MyApplication) (MyApplication.getContext())).StartThread_files(values.get(0).toString());

                // ViewGroup slidingTabStrip =  (ViewGroup) (MyApplication.getContext()).tabLayout.getChildAt(0);

   /*
                for (int i = 0; i <  (MyApplication.getContext()).tabLayout.getTabCount(); i++) {
                    View tab = slidingTabStrip.getChildAt(i);
                    LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) tab.getLayoutParams();
                    if(i==1)
                        layoutParams.weight = 0;
                    else
                        layoutParams.weight = 1;
                    tab.setLayoutParams(layoutParams);


                }
    */

            }
        });




    }





    @Override
    public int getItemCount() {
        return directories.size();
    }


}
