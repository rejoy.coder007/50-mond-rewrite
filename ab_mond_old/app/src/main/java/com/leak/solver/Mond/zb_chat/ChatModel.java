package com.leak.solver.Mond.zb_chat;

import com.google.firebase.Timestamp;
import com.google.firebase.firestore.IgnoreExtraProperties;


@IgnoreExtraProperties
public class ChatModel
{






    String aa_user_id;
    String ab_user_name;
    String ac_other_image;

    String ad_current_id;
    String ae_current_name;
    String af_current_image;


    String ag_message;
    String ah_image_url;
    Timestamp ai_sendtime;
    String ai_sendtime_str;

    public String getAa_user_id() {
        return aa_user_id;
    }

    public void setAa_user_id(String aa_user_id) {
        this.aa_user_id = aa_user_id;
    }

    public String getAb_user_name() {
        return ab_user_name;
    }

    public void setAb_user_name(String ab_user_name) {
        this.ab_user_name = ab_user_name;
    }

    public String getAc_other_image() {
        return ac_other_image;
    }

    public void setAc_other_image(String ac_other_image) {
        this.ac_other_image = ac_other_image;
    }

    public String getAd_current_id() {
        return ad_current_id;
    }

    public void setAd_current_id(String ad_current_id) {
        this.ad_current_id = ad_current_id;
    }

    public String getAe_current_name() {
        return ae_current_name;
    }

    public void setAe_current_name(String ae_current_name) {
        this.ae_current_name = ae_current_name;
    }

    public String getAf_current_image() {
        return af_current_image;
    }

    public void setAf_current_image(String af_current_image) {
        this.af_current_image = af_current_image;
    }

    public String getAg_message() {
        return ag_message;
    }

    public void setAg_message(String ag_message) {
        this.ag_message = ag_message;
    }

    public String getAh_image_url() {
        return ah_image_url;
    }

    public void setAh_image_url(String ah_image_url) {
        this.ah_image_url = ah_image_url;
    }

    public Timestamp getAi_sendtime() {
        return ai_sendtime;
    }

    public void setAi_sendtime(Timestamp ai_sendtime) {
        this.ai_sendtime = ai_sendtime;
    }

    public String getAi_sendtime_str() {
        return ai_sendtime_str;
    }

    public void setAi_sendtime_str(String ai_sendtime_str) {
        this.ai_sendtime_str = ai_sendtime_str;
    }

    public String getAj_sender_id() {
        return aj_sender_id;
    }

    public void setAj_sender_id(String aj_sender_id) {
        this.aj_sender_id = aj_sender_id;
    }

    String aj_sender_id;

    public ChatModel(String aa_user_id, String ab_user_name, String ac_other_image, String ad_current_id, String ae_current_name, String af_current_image, String ag_message, String ah_image_url, Timestamp ai_sendtime, String ai_sendtime_str, String aj_sender_id) {
        this.aa_user_id = aa_user_id;
        this.ab_user_name = ab_user_name;
        this.ac_other_image = ac_other_image;
        this.ad_current_id = ad_current_id;
        this.ae_current_name = ae_current_name;
        this.af_current_image = af_current_image;
        this.ag_message = ag_message;
        this.ah_image_url = ah_image_url;
        this.ai_sendtime = ai_sendtime;
        this.ai_sendtime_str = ai_sendtime_str;
        this.aj_sender_id = aj_sender_id;
    }






    public ChatModel() {
    }




}
