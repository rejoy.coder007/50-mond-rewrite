package com.leak.solver.Mond.ad_Auth;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;

import androidx.core.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.leak.solver.Mond.R;
import com.leak.solver.Mond.zz_Config;

import java.util.Random;

public class FirebaseMessagingServiceForeground extends FirebaseMessagingService {
    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);

        Log.d("CHAT_NOTI", "New Token: "+s);
    }


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        String messageTitle = remoteMessage.getNotification().getTitle();
        String messageBody = remoteMessage.getNotification().getBody();
        String click_action = remoteMessage.getNotification().getClickAction();

        Log.d("CHAT_NOTI", "onMessageReceived: "+messageTitle+"::"+messageBody);


        /*
         message_d : from_message,
                        from_id_d : from_user_id,
                        from_name_d    : from_name,
                        to_name_d    : to_name

         */

        String  message_d = remoteMessage.getData().get("message_d");
        String  from_id_d = remoteMessage.getData().get("from_id_d");
        String  to_id_d = remoteMessage.getData().get("to_id_d");
        String  from_name_d = remoteMessage.getData().get("from_name_d");
        String  to_name_d = remoteMessage.getData().get("from_id_d");


        Intent intent = new Intent(click_action);

        intent.putExtra("message_d",message_d);
        intent.putExtra("from_id_d",from_id_d);
        intent.putExtra("from_name_d",from_name_d);
        intent.putExtra("to_name_d",to_name_d);
        //intent.putExtra("user_id","Xws25zs9BshIYYDQCGwaBHEx1es1");
        intent.putExtra("user_id",from_id_d);
        if(zz_Config.CHAT_TEST)
            Log.d("CHAT_TEST_1", from_id_d+":: "+to_id_d+"::"+"Xws25zs9BshIYYDQCGwaBHEx1es1");

        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.O){

            NotificationChannel notificationChannel = new NotificationChannel(getString(R.string.default_notification_channel_id),"Notification",NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.setDescription("EDMT Channel");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.BLUE);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableLights(true);
            mNotifyMgr.createNotificationChannel(notificationChannel);

        }


        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this, getString(R.string.default_notification_channel_id) );

        mBuilder.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setSmallIcon(R.drawable.aa_logo)
                .setContentTitle(messageTitle)
                .setContentText(messageBody)
                .setContentInfo("info");




        // Creating a pending intent and wrapping our intent
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(pendingIntent);


        mNotifyMgr.notify(new Random().nextInt(), mBuilder.build());




    }



    private void showNotification(String title, String body) {

        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        String NOTIFICATION_CHANNEL_ID = "mond.app.com";

        Log.d("CHAT_NOTI", "Message 1");

        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.O){

            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID,"Notification",NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.setDescription("EDMT Channel");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.BLUE);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableLights(true);

            mNotifyMgr.createNotificationChannel(notificationChannel);



        }


        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID );

        mBuilder.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(body)
                .setContentInfo("info");

        mNotifyMgr.notify(new Random().nextInt(), mBuilder.build());

        Log.d("CHAT_NOTI", "Message 2");

    }

}

