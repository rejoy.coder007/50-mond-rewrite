package com.leak.solver.Mond.aa_StartUp;

import android.app.ActivityManager;
import android.os.Handler;
import android.os.Looper;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;

import com.leak.solver.Mond.R;
import com.leak.solver.Mond.zz_Config;

public class aa_activity_StartUp extends AppCompatActivity {

    public static aa_activity_StartUp aaStartUp = null;


    public ab_FileManager_Start ab_fileManagerStart = null;
    public aa_activity_StartUp aaStartUp_ref = null;
    public ActivityManager activityManager = null;

    public Handler handler_for_intent=null;


    public aa_activity_StartUp Get_obj() {
        return aaStartUp_ref;
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        switch (keyCode) {
            case KeyEvent.KEYCODE_A:
            {
                //your Action code

                //ab_fileManagerStart.semaphore.release();

                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {


        super.onCreate(savedInstanceState);



        aa_activity_StartUp.aaStartUp = aaStartUp_ref = this;
        if (zz_Config.TEST)
        {
            Log.d("_##_ON _CREATE", "onCreate: aa_activity_StartUp -- A " +aa_activity_StartUp.aaStartUp);
        }


        ab_fileManagerStart = new ab_FileManager_Start();
        zz_Config.IS_BACK = false;
        handler_for_intent = new Handler(Looper.myLooper());


        if (ab_fileManagerStart.isFirstTime(aa_activity_StartUp.this))
        {
            setContentView(R.layout.aa_activity_startup);
        }
        else
        {
            setContentView(R.layout.ab_activity_splash_up);
        }


        if (zz_Config.TEST)
        {
            Log.d("_##_ON_CREATE", "onCreate: aa_activity_StartUp -- B");

            if (aa_activity_StartUp.aaStartUp.activityManager == null)
            {
                aa_activity_StartUp.aaStartUp.activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
            }


        }

    }

    @Override
    protected void onResume()
    {
        super.onResume();



        ab_fileManagerStart.initialize(aa_activity_StartUp.this);
        ab_fileManagerStart.TOKEN_SCENE_CHANGE   = 0;


        if (zz_Config.TEST)
        {
            Log.d("_##_ON_RESUME", "onResume: aa_activity_StartUp -- A");
        }

        if (zz_Config.IS_BACK)
        {
            // Runtime.getRuntime().gc();
            if (zz_Config.TEST)
            {

                Log.d("_##_APP_STATE_S", aa_activity_StartUp.aaStartUp.toString());

                Log.d("_##_APP_STATE_S", zz_Config.getAppTaskState(aa_activity_StartUp.aaStartUp.activityManager));
            }
            onBackPressed();
        }
        else
        {


            if (ab_fileManagerStart.isFirstTime(aa_activity_StartUp.this))
            {

                ab_fileManagerStart.FirstTimeScreen();
            }
            else
            {
                ab_fileManagerStart.SplashTimeScreen();

            }

        }


        if (zz_Config.TEST)
        {
            Log.d("_##_ON_RESUME", "onResume: aa_activity_StartUp -- B");
        }


    }


    @Override
    protected void onUserLeaveHint()
    {
        if (zz_Config.TEST)
            Log.d("_##_ON_LEAVE", "onUserLeaveHint  :  aa_activity_StartUp A ::"+ aa_activity_StartUp.aaStartUp.ab_fileManagerStart.TOKEN_SCENE_CHANGE);

        if(ab_fileManagerStart.TOKEN_SCENE_CHANGE!=1)
        {

            try
            {
                ab_fileManagerStart.semaphore.acquire();
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }


            ab_fileManagerStart.TOKEN_SCENE_CHANGE = 2;

            if (zz_Config.TEST)
                Log.d("_##_ON_LEAVE", "onUserLeaveHint  :  aa_activity_StartUp B");



            ab_fileManagerStart.semaphore.release();
        }


        if (zz_Config.TEST)
            Log.d("_##_ON_LEAVE", "onUserLeaveHint  :  aa_activity_StartUp C ::"+ ab_fileManagerStart.TOKEN_SCENE_CHANGE);


         super.onUserLeaveHint();
    }




    @Override
    protected void onStop()
    {


        switch( ab_fileManagerStart.TOKEN_SCENE_CHANGE )
        {
            case 1:

                if (zz_Config.TEST)
                    Log.d("_##_STOP", " Case 1 Inside Stop :   aa_activity_StartUp A");
                 break;


            default:


                    if (zz_Config.TEST)
                       Log.d("_##_STOP", " Case 1/2 Inside Stop :   aa_activity_StartUp A"+ab_fileManagerStart.semaphore.getQueueLength());

////////////////////////////////////////////////////////////////////////////////////////


                    ab_fileManagerStart.Thread_Kiler(null);
                    ab_fileManagerStart.reset_variables_start_UP();




////////////////////////////////////////////////////////////////////////////////////////

                if (zz_Config.TEST)
                    Log.d("_##_STOP", " Case 1/2 Inside Stop :   aa_activity_StartUp B");



             break;


        }


        super.onStop();




    }


    @Override
    public void onBackPressed()
    {



    if (zz_Config.TEST)
      Log.d("_##_BACKBUTTON", " Inside onBackPressed :   aa_activity_StartUp A");




        if(zz_Config.IS_BACK)
        {
            zz_Config.IS_BACK = false;
            super.onBackPressed();

        }


        zz_Config.IS_BACK = false;
    }


}
