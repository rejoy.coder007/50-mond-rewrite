package com.leak.solver.Mond.ad_Auth;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.leak.solver.Mond.R;

public class NotificationActivity extends AppCompatActivity {


    private TextView mNotifData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        String message_d = getIntent().getStringExtra("message_d");
        String from_id_d = getIntent().getStringExtra("from_id_d");
        String from_name_d = getIntent().getStringExtra("from_name_d");
        String to_name_d = getIntent().getStringExtra("to_name_d");


        mNotifData = (TextView) findViewById(R.id.notif_text);

        mNotifData.setText( message_d+"::"+from_id_d+"::"+from_name_d+"::"+to_name_d);


    }
}
