package com.leak.solver.Mond.ab_home_page.aa_tabs.ac_folderview;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;

import com.leak.solver.Mond.ab_home_page.aa_activity_home_screen;
import com.leak.solver.Mond.ab_home_page.aa_tabs.ac_folderview.aa_Recycler.Directory_folder;
import com.leak.solver.Mond.ab_home_page.aa_tabs.ac_folderview.aa_Recycler.RecyclerAdapter_folder;
import com.leak.solver.Mond.zz_Config;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.leak.solver.Mond.ab_home_page.aa_activity_home_screen.getContext;

public class FileManager_Home_folder {


    public RecyclerView recyclerView_folder=null;
    public Boolean mStopLoop_folder= false;
    public RecyclerAdapter_folder recyclerAdapter_folder=null;
    public Handler handler_folder=null;
    public List<Directory_folder> directories_folder  =null;
    public SwipeRefreshLayout mSwipeRefreshLayout_folder=null;





    public Thread Thread_Full_FOLDER =null;




    Handler directory_handler_folder =null;




    public Boolean READY_TO_UPDATE_FOLDER = false;

    public FileManager_Home_folder() {
         directories_folder  =new ArrayList<>();
    }


    public void ReadDirByThread_folder(Context context, File path)
    {
        //  Toast.makeText(getApplicationContext(), "Your Message", Toast.LENGTH_LONG).show();



        directory_handler_folder = new Handler(Looper.myLooper());



        Runnable runnable = () ->
        {



            fullread_exe(path);

        };
        Thread_Full_FOLDER =  new Thread(runnable);

        Thread_Full_FOLDER.start();

    }





    void fullread_exe(File path)
    {

        if(zz_Config.TEST)
        {


            Log.d("_#_F_FILL_DATA_A", "StartUpScreenThread_dir_fill : aa_activity_home_screen A");
        }

        if(!aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.THREAD_STOPPER ) {
            try {
                aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.getfolder_Content(path);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


        directories_folder.clear();


        if(zz_Config.TEST)
        {



        }
        List<String> Test_values = new ArrayList<String>();
        for (int i = 0; (!aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.THREAD_STOPPER)  && (i <aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.names_folder.size()) ; i++)
        {

            List<String> values = new ArrayList<String>();
            //       values = aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.map.get(aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.names_fav.get(i));
            //  Log.d("DIR_N", "getfile: " + ((MyApplication) (getActivity()).getApplication()).names_fav.get(i));

            values = aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.names_folder.get(i);

            Directory_folder directory_folder = new Directory_folder(values.get(0), values.get(1),values.get(2) + " Videos");
            Test_values.add(values.get(0));
            directories_folder.add(directory_folder);
        }



        if(zz_Config.TEST)
        {
            Log.d("_#_F_VALUES", " Hash Inside AdapterThread :   HomePage"+ Arrays.toString(Test_values.toArray()));

        }


        if(zz_Config.TEST)
            Log.d("_#_F_FILL_DATA_B", "StartUpScreenThread_dir_fill : aa_activity_home_screen B" );

      //  aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.names_folder.clear();

        Runnable r = new Runnable() {
            @Override
            public void run()
            {
/*
                public RecyclerView recyclerViewFav;
                public RecyclerAdapterFav recyclerAdapterFav;
                public List<DirectoryFav> directories =new ArrayList<>();
                public SwipeRefreshLayout mSwipeRefreshLayoutFav;

                */


                if(zz_Config.TEST)
                    Log.d("_#_F_FILL_DATA_C_FOLDER", "StartUpScreenThread_dir_fill : aa_activity_home_screen A" );


                recyclerAdapter_folder.setRecyclerAdapter(directories_folder,getContext());
                recyclerAdapter_folder.notifyItemRangeChanged(0, directories_folder.size());
                recyclerAdapter_folder.notifyDataSetChanged();
                // mSwipeRefreshLayoutFav.setRefreshing(false);
                //     aa_activity_home_screen.mContext.fileManager_home_fav.recyclerViewFav.setNestedScrollingEnabled(false);

                aa_activity_home_screen.mContext.fileManager_home_folder.recyclerView_folder.setLayoutFrozen(false);

                aa_activity_home_screen.mContext.fileManager_home.viewPager.setCurrentItem(2);

               // directories_folder.clear();
            //    aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.names_folder.clear();

                // enable_tab(aa_activity_home_screen.mContext);
                if(zz_Config.TEST)
                    Log.d("_#_F_FILL_DATA_FOLDER", "Handler StartUpScreenThread_dir_fill : aa_activity_home_screen B" );

            }
        };


        while(!READY_TO_UPDATE_FOLDER);

        if(!aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.THREAD_STOPPER )

            directory_handler_folder.post(r);

        if(aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.THREAD_STOPPER )
            directory_handler_folder.removeCallbacksAndMessages(null);

        if(zz_Config.TEST)
            Log.d("_#_F_FILL_DATA__FOLDER", "StartUpScreenThread_dir_fill : aa_activity_home_screen D" );



    }








}
