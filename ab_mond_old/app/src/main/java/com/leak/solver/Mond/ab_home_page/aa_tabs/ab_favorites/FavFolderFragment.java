package com.leak.solver.Mond.ab_home_page.aa_tabs.ab_favorites;


import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.leak.solver.Mond.R;
import com.leak.solver.Mond.ab_home_page.FileManager_Home;
import com.leak.solver.Mond.ab_home_page.aa_activity_home_screen;
import com.leak.solver.Mond.ab_home_page.aa_tabs.ab_favorites.aa_Recycler.RecyclerAdapterFav;
import com.leak.solver.Mond.zz_Config;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavFolderFragment extends Fragment
{
    public static final String ARG_PAGE = "ARG_PAGE";
    private int mPage;
    int refresh_i=0;

    aa_activity_home_screen  aa_activity_home_screen =null;
    public FavFolderFragment()
    {
        // Required empty public constructor


    }


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt(ARG_PAGE);
        aa_activity_home_screen = ((aa_activity_home_screen) getActivity());



        if (zz_Config.TEST)
        {
            Log.d("_@@_#FAV_FRAG_ON_CREATE", "onCreate fav fragment  : aa_activity_home_screen__ " + mPage);

        }

    }


    void reset_layout()
    {
       // aa_activity_home_screen.fileManager_home_fav=null;
       // aa_activity_home_screen.ada=null;
       // aa_activity_home_screen.fileManager_home_fav=null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = null;

        aa_activity_home_screen.fileManager_home.TAB_PRESS =false;

        if (zz_Config.TEST) {
            Log.d("_@@_#FAV_FRAG_ON_VIEW", " onCreateView fav fragment  : aa_activity_home_screen__A " + mPage);

        }




        rootView = inflater.inflate(R.layout.ad_b_fragment_home_fav, container, false);

        //   ((aa_activity_home_screen)getActivity()).ad_back.setVisibility(View.VISIBLE);


        aa_activity_home_screen.fileManager_home_fav.recyclerViewFav =  rootView.findViewById(R.id.contact_recycleView);
        aa_activity_home_screen.fileManager_home_fav.recyclerViewFav.setHasFixedSize(true);
        aa_activity_home_screen.fileManager_home_fav.recyclerAdapterFav = new RecyclerAdapterFav(aa_activity_home_screen.fileManager_home_fav.directories, getContext());




        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        aa_activity_home_screen.fileManager_home_fav.recyclerViewFav.setLayoutManager(linearLayoutManager);



        aa_activity_home_screen.fileManager_home_fav.recyclerViewFav.setAdapter(aa_activity_home_screen.fileManager_home_fav.recyclerAdapterFav);


        // SwipeRefreshLayout
        aa_activity_home_screen.fileManager_home_fav.mSwipeRefreshLayoutFav = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
        aa_activity_home_screen.fileManager_home_fav.mSwipeRefreshLayoutFav.setColorSchemeResources(R.color.colorAccent,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        //   StartThread_dir_update(false);

        aa_activity_home_screen.fileManager_home_fav.recyclerViewFav.setLayoutFrozen(true);
        aa_activity_home_screen.fileManager_home_fav.mSwipeRefreshLayoutFav.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh()
            {
                // Your recyclerview reload logic function will be here!!!

                if (zz_Config.TEST)
                {
                    Log.d("_@@_#FAV_FRAG_REFRESH", "onRefresh fav fragment  : aa_activity_home_screen ");

                }

                aa_activity_home_screen.fileManager_home_fav.mSwipeRefreshLayoutFav.setRefreshing(false);

            }
        });


        if (zz_Config.TEST) {
            Log.d("_@@_#FAV_FRAG_ON_VIEW", " onCreateView fav fragment  : aa_activity_home_screen__B " + mPage);

        }




        return rootView;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {


        if(zz_Config.TEST)
            Log.d("_@@_#FAV_FRAG_VIEW_CTED", " Fragment View Created aa_activity_home_screen A" );
        aa_activity_home_screen.fileManager_home_fav.READY_TO_UPDATE_FAV=true;
        /*
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
*
        aa_activity_home_screen.fileManager_home_fav.READY_TO_UPDATE_FOLDER=true;


        if (zz_Config.TEST)
        {
            Log.d("_#_FAV_FRAG_VIEW_CTED", "onCreate fav fragment  : aa_activity_home_screen__ " + mPage);

        }

        if(zz_Config.TEST)
            Log.d("__#_FAV_FRAG_VIEW_CTED", " Fragment View Created aa_activity_home_screen B" );

        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    public void onResume() {
        Log.e("_#_FAV_FRAG_VIEW_CTED", "onResume of FavFolderFragment");
        super.onResume();
    }

    @Override
    public void onPause() {
        Log.e("_#_FAV_FRAG_VIEW_CTED", "OnPause of FavFolderFragment");
        super.onPause();
    }

    public void refresh(View view) {


        /*
        if(zz_Config.TEST)
        {
            Log.d("_#_FAV_FRAG_REFRESH", "refresh fav fragment    : aa_activity_home_screen A__"+mPage);

        }

        StartThread_dir_update(true);

        if(zz_Config.TEST)
        {
            Log.d("_#_FAV_FRAG_REFRESH", "refresh fav fragment    : aa_activity_home_screen B__  "+mPage);

        }
*/

    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {

        if(zz_Config.TEST)
        {
            Log.d("_@@_#FAV_FRAG_VIS", " VISIBLE folder fragment  : aa_activity_home_screen__ "+mPage+"::"+isVisibleToUser+"::");

        }


         FileManager_Home.FAV_TAB_VISIBLE=isVisibleToUser;
        super.setUserVisibleHint(isVisibleToUser);

    }


    @Override
    public void onStop() {

        if(zz_Config.TEST)
        {
            Log.d("&_@@_#FAV_FRAG_STP", " VISIBLE folder fragment  : aa_activity_home_screen__ "+mPage+"::");

        }

        if(  aa_activity_home_screen.fileManager_home.TAB_PRESS)
            {

                aa_activity_home_screen.fileManager_home_fav.recyclerAdapterFav = null;

                aa_activity_home_screen.fileManager_home_fav.recyclerViewFav = null;
                aa_activity_home_screen.fileManager_home_fav.mSwipeRefreshLayoutFav = null;
                aa_activity_home_screen.fileManager_home_fav.directory_handler = null;



                Runtime.getRuntime().gc();

            }
        super.onStop();  // Always call the superclass method first

    }


    // public


}
