package com.leak.solver.Mond.af_friendlist;

import android.content.DialogInterface;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.MetadataChanges;
import com.leak.solver.Mond.R;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.google.firebase.firestore.FirebaseFirestoreException;
import com.leak.solver.Mond.ad_Auth.ProfileActivity;
import com.leak.solver.Mond.zb_chat.FireStoreChat.FirestoreChatActivity;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import android.widget.RelativeLayout;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;

public class FriendListsAcitivty extends AppCompatActivity {

    ProgressBar progressBar;
    RecyclerView friendList;

    private FirebaseAuth mAuth;
    private  String currentId;
   public FirebaseFirestore mFirestore;

    private FirebaseFirestore db;
    private FirestoreRecyclerAdapter adapter;
    LinearLayoutManager linearLayoutManager;

    ListenerRegistration UsersList_registration_CurrentList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_friend_lists);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.users_appBar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Friends");
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        friendList = findViewById(R.id.friendlists);
        linearLayoutManager = new LinearLayoutManager(this);
        friendList.setLayoutManager(linearLayoutManager);
        friendList.setHasFixedSize(true);

        mAuth = FirebaseAuth.getInstance();

        currentId = mAuth.getUid().toString();
        init();
        getFriendList();


        mAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();
    }

    private void init(){
        linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        friendList.setLayoutManager(linearLayoutManager);
        db = FirebaseFirestore.getInstance();
    }



    public static class FriendsHolder extends RecyclerView.ViewHolder {

        public View mView;

        public RelativeLayout root;
        public ImageView OnlineImage;
        public FriendsHolder(View itemView)
        {
            super(itemView);

            mView = itemView;


              OnlineImage = (ImageView) mView.findViewById(R.id.user_single_online_icon);

        }

        public void setRoot()
        {

            root = (RelativeLayout) mView.findViewById(R.id.root);


        }

        public void setDisplayName(String name)
        {

            TextView userNameView = (TextView) mView.findViewById(R.id.user_single_name);
            userNameView.setText(name);

        }


        public void setUserStatus(String status)
        {

            TextView userStatusView = (TextView) mView.findViewById(R.id.date_and_time);
            userStatusView.setText(status);


        }

        public void setUserImage(String thumb_image, Context ctx)
        {

            CircleImageView c = (CircleImageView) mView.findViewById(R.id.user_single_image);


            Picasso.get().load(thumb_image).placeholder(R.drawable.default_avatar).into(c, new Callback() {

                @Override
                public void onSuccess() {



                    //  Toast.makeText(UsersActivity.this,"image uploaded", Toast.LENGTH_SHORT).show();


                }

                @Override
                public void onError(Exception e) {

                }
            });

        }

    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }


    private void getFriendList(){
        Query query = db.collection("Friends").document(currentId).collection("MyFriends");



        FirestoreRecyclerOptions<FriendsListModel> response = new FirestoreRecyclerOptions.Builder<FriendsListModel>()
                .setQuery(query, FriendsListModel.class)
                .build();

        adapter = new FirestoreRecyclerAdapter<FriendsListModel, FriendListsAcitivty.FriendsHolder>(response) {
            @Override
            public void onBindViewHolder(FriendListsAcitivty.FriendsHolder holder, int position, FriendsListModel model) {


                holder.setDisplayName(model.getOther_user_name());
                holder.setUserStatus(model.getCurrentDate()+"::"+model.getOnline());
                holder.setUserImage(model.getOther_user_thumb_id(), FriendListsAcitivty.this);


                UsersList_registration_CurrentList = mFirestore.collection("UsersList").document(model.getCurrent_id()).addSnapshotListener(MetadataChanges.INCLUDE, new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                        @Nullable FirebaseFirestoreException e) {


                        if (e != null) {
                            System.err.println("Listen failed:" + e);
                            return;
                        }



                        Boolean online = documentSnapshot.getBoolean("online");


                        if(online)
                        {
                            holder.OnlineImage.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            holder.OnlineImage.setVisibility(View.GONE);
                        }
                        // ...
                    }
                });







                holder.setRoot();
                final String user_id = getSnapshots().getSnapshot(position).getId();

                Log.d("C_ID", user_id);

                holder.root.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        //Toast.makeText(UsersActivity.this, String.valueOf(position), Toast.LENGTH_SHORT).show();
                        //SendtoAllUsersProfleActivity(user_id);
                        //Log.d("Clicked id", user_id);

                        CharSequence options[] = new CharSequence[]{"Open Profile", "Send message"};

                        final AlertDialog.Builder builder = new AlertDialog.Builder(FriendListsAcitivty.this);

                        builder.setTitle("Select Options");
                        builder.setItems(options, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                //Click Event for each item.
                                if(i == 0){

                                   Intent profileIntent = new Intent(FriendListsAcitivty.this, ProfileActivity.class);



                                    profileIntent.putExtra("user_id", model.getOther_user_id());
                                    profileIntent.putExtra("user_name", model.getOther_user_name());





                                    startActivity(profileIntent);


                                }

                                if(i == 1){


                                 //   Intent chatIntent = new Intent(FriendListsAcitivty.this, ChatActivity.class);
                                    Intent chatIntent = new Intent(FriendListsAcitivty.this, FirestoreChatActivity.class);
                                    chatIntent.putExtra("current_id", model.getCurrent_id());
                                    chatIntent.putExtra("user_id", model.getOther_user_id());
                                    chatIntent.putExtra("current_name", model.getCurrent_name());
                                    chatIntent.putExtra("user_name", model.getOther_user_name());
                                    chatIntent.putExtra("current_image", model.getCurrent_thumb_id());
                                    chatIntent.putExtra("other_image", model.getOther_user_thumb_id());
                                    startActivity(chatIntent);


                                }

                            }
                        });

                        builder.show();

                    }
                });


            }

            @Override
            public FriendListsAcitivty.FriendsHolder onCreateViewHolder(ViewGroup group, int i) {
                View view = LayoutInflater.from(group.getContext())
                        .inflate(R.layout.users_single_layout, group, false);

                return new FriendsHolder(view);
            }

            @Override
            public void onError(FirebaseFirestoreException e) {
                Log.e("error", e.getMessage());
            }
        };

        adapter.notifyDataSetChanged();
        friendList.setAdapter(adapter);
    }


    public void SendtoAllUsersProfleActivity( String user_id ) {

        Intent intent = new Intent(FriendListsAcitivty.this, ProfileActivity.class);
        intent.putExtra("user_id", user_id);
        startActivity(intent);
    }

}
