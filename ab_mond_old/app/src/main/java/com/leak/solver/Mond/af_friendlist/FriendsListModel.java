package com.leak.solver.Mond.af_friendlist;

import com.google.firebase.firestore.IgnoreExtraProperties;

@IgnoreExtraProperties
public class FriendsListModel
{
    public FriendsListModel(String currentDate, String current_id, String other_user_id, String current_thumb_id, String online, String other_user_thumb_id, String current_name, String other_user_name, Boolean intialized) {
        this.currentDate = currentDate;
        this.current_id = current_id;
        this.other_user_id = other_user_id;
        this.current_thumb_id = current_thumb_id;
        this.online = online;
        this.other_user_thumb_id = other_user_thumb_id;
        this.current_name = current_name;
        this.other_user_name = other_user_name;
        this.intialized = intialized;
    }


    public String currentDate;

    public String current_id;
    public String other_user_id;

    public String current_thumb_id;
    public String other_user_thumb_id;

    public String current_name;
    public String other_user_name;

    public Boolean intialized;

    public String getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(String currentDate) {
        this.currentDate = currentDate;
    }

    public String getCurrent_id() {
        return current_id;
    }

    public void setCurrent_id(String current_id) {
        this.current_id = current_id;
    }

    public String getOther_user_id() {
        return other_user_id;
    }

    public void setOther_user_id(String other_user_id) {
        this.other_user_id = other_user_id;
    }

    public String getCurrent_thumb_id() {
        return current_thumb_id;
    }

    public void setCurrent_thumb_id(String current_thumb_id) {
        this.current_thumb_id = current_thumb_id;
    }

    public String getOnline() {
        return online;
    }

    public void setOnline(String online) {
        this.online = online;
    }

    public String getOther_user_thumb_id() {
        return other_user_thumb_id;
    }

    public void setOther_user_thumb_id(String other_user_thumb_id) {
        this.other_user_thumb_id = other_user_thumb_id;
    }

    public String getCurrent_name() {
        return current_name;
    }

    public void setCurrent_name(String current_name) {
        this.current_name = current_name;
    }

    public String getOther_user_name() {
        return other_user_name;
    }

    public void setOther_user_name(String other_user_name) {
        this.other_user_name = other_user_name;
    }

    public Boolean getIntialized() {
        return intialized;
    }

    public void setIntialized(Boolean intialized) {
        this.intialized = intialized;
    }



    public String online;

    public FriendsListModel() {
    }




}
