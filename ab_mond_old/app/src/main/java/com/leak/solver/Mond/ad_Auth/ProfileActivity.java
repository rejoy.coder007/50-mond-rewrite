package com.leak.solver.Mond.ad_Auth;

import android.app.ProgressDialog;

import android.os.Build;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.MetadataChanges;
import com.google.firebase.firestore.WriteBatch;
import com.leak.solver.Mond.R;
import com.leak.solver.Mond.zz_Config;
import com.squareup.picasso.Picasso;


import java.text.SimpleDateFormat;
import java.util.Calendar;

import java.util.HashMap;
import java.util.Map;

public class ProfileActivity extends AppCompatActivity {

    private ImageView mProfileImage;
    private TextView mProfileName, mProfileStatus, mProfileFriendsCount;
    private Button mProfileSendReqBtn, mDeclineBtn;
    private Button TestButton;
    private DatabaseReference mUsersDatabase;

    private ProgressDialog mProgressDialog;

    private String mCurrent_state;

    private DatabaseReference mRootRef;

    private FirebaseUser mCurrent_user;

    private String TAG = "FIRE_STORE_TEST_WRITE";
    private String TAG1 = "FIRE_STORE_TEST_READ";
    private String TAG3= "FIRE_STORE_TEST_LISTEN";

    ValueEventListener valueEventListener;

    String user_id;
    String thumb_id;
    String user_name;
    String who_made_you_friend_id;
    String status;
    String last_active;



    String other_user_id;
    String other_user_thumb_id;
    String other_user_name;




    String current_id;
    String current_thumb_id;
    String current_name;





    String current_user_id;

    OnSuccessListener userStaticsListener =null;
    OnSuccessListener friendReqStaticsListener =null;
    OnSuccessListener friendsListener =null;


    private FirebaseAuth mAuth;

    private FirebaseFirestore mFirestore;


    ListenerRegistration UsersList_registration_UserList;
    ListenerRegistration UsersList_registration_FrdReq;
    ListenerRegistration UsersList_registration_Frds;
    ListenerRegistration UsersList_registration_CurrentList;

    @Override
    public void onResume()
    {
        super.onResume();






        mCurrent_state="not_friends";


        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setTitle("Loading User Data");
        mProgressDialog.setMessage("Please wait while we load the user data.");
        mProgressDialog.setCanceledOnTouchOutside(false);




        mProgressDialog.show();



        UsersList_registration_UserList = mFirestore.collection("UsersList").document(user_id).addSnapshotListener(MetadataChanges.INCLUDE, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                @Nullable FirebaseFirestoreException e) {


                if (e != null) {
                    System.err.println("Listen failed:" + e);
                    return;
                }




                String display_name = documentSnapshot.getString("name");
                String status = documentSnapshot.getString("status");
                String image = documentSnapshot.getString("image");
                String thumb_image = documentSnapshot.getString("thumb_image");



                  other_user_id=user_id;
                  other_user_thumb_id=thumb_image;
                  other_user_name = display_name;


                mProfileName.setText(display_name);
                mProfileStatus.setText(status);

                Picasso.get().load(image).placeholder(R.drawable.default_avatar).into(mProfileImage);


                if(mCurrent_user.getUid().equals(user_id)){

                    mDeclineBtn.setEnabled(false);
                    mDeclineBtn.setVisibility(View.INVISIBLE);

                    mProfileSendReqBtn.setEnabled(false);
                    mProfileSendReqBtn.setVisibility(View.INVISIBLE);

                }

                mProgressDialog.dismiss();
                // ...
            }
        });


        UsersList_registration_CurrentList = mFirestore.collection("UsersList").document(current_user_id).addSnapshotListener(MetadataChanges.INCLUDE, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                @Nullable FirebaseFirestoreException e) {


                if (e != null) {
                    System.err.println("Listen failed:" + e);
                    return;
                }



                String display_name = documentSnapshot.getString("name");
                String status = documentSnapshot.getString("status");
                String image = documentSnapshot.getString("image");
                String thumb_image = documentSnapshot.getString("thumb_image");



                current_id=current_user_id;
                current_thumb_id=thumb_image;
                current_name = display_name;




                // ...
            }
        });

        UsersList_registration_FrdReq = mFirestore.collection("FriendsReqList").document(current_user_id).collection("MyFriends").document(user_id).addSnapshotListener(MetadataChanges.INCLUDE, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                @Nullable FirebaseFirestoreException e) {


                if (e != null) {
                    System.err.println("Listen failed:" + e);
                    return;
                }

                String type = documentSnapshot.getString("type");

                if(zz_Config.CHAT_TEST)
                    Log.d(TAG3, "onSuccess: 1 "+type);


                if(documentSnapshot.contains("type"))
                {

                    if(zz_Config.CHAT_TEST)
                        Log.d(TAG3, "onSuccess: 2 "+type);
                    if(type.equals("received"))
                    {

                        if(zz_Config.CHAT_TEST)
                            Log.d(TAG3, "onSuccess: 3 "+type);
                        mCurrent_state = "req_received";
                        mProfileSendReqBtn.setText("Accept Friend Request");

                        mDeclineBtn.setVisibility(View.VISIBLE);
                        mDeclineBtn.setEnabled(true);


                    }
                    else if(type.equals("sent"))
                    {

                        if(zz_Config.CHAT_TEST)
                            Log.d(TAG3, "onSuccess: 4 "+type);
                        mCurrent_state = "req_sent";
                        mProfileSendReqBtn.setText("Cancel Friend Request");

                        mDeclineBtn.setVisibility(View.INVISIBLE);
                        mDeclineBtn.setEnabled(false);

                    }

                }








            }
        });



        UsersList_registration_Frds= mFirestore.collection("Friends").document(current_user_id).collection("MyFriends").document(user_id).addSnapshotListener(MetadataChanges.INCLUDE, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                @Nullable FirebaseFirestoreException e) {


                if (e != null) {
                    System.err.println("Listen failed:" + e);
                    return;
                }

                String date = documentSnapshot.getString("currentDate");

                if(documentSnapshot.contains("currentDate")) {


                    mCurrent_state = "friends";
                    mProfileSendReqBtn.setText("Unfriend this Person");

                    mDeclineBtn.setVisibility(View.INVISIBLE);
                    mDeclineBtn.setEnabled(false);


                }




            }
        });



        mProfileSendReqBtn.setOnClickListener(new View.OnClickListener()
                                              {
                                                  @RequiresApi(api = Build.VERSION_CODES.N)
                                                  @Override
                                                  public void onClick(View view) {

                                                      mProfileSendReqBtn.setEnabled(false);


                                                      mProgressDialog = new ProgressDialog(ProfileActivity.this);
                                                      mProgressDialog.setTitle(" Sending Request");
                                                      mProgressDialog.setMessage("Please wait while we load the user data.");
                                                      mProgressDialog.setCanceledOnTouchOutside(false);
                                                      mProgressDialog.show();

                                                      if(zz_Config.CHAT_TEST)
                                                          Log.d("SEND_REQ", "Running 0 ");



 ///////////////////////////////////////////////////////////////////////////////////////////////////////

                                                      if (mCurrent_state.equals("not_friends")) {


                                                          mProfileSendReqBtn.setEnabled(false);

// Get a new write batch
                                                          // Get a new write batch
                                                          WriteBatch batch = mFirestore.batch();

                                                          Map<String, Object> status_0 = new HashMap<>();
                                                          status_0.put("Sender", current_user_id);
                                                          status_0.put("Receiver", user_id);
                                                          status_0.put("type", "sent");

                                                          // Set the value of 'NYC'
                                                          DocumentReference nycRef_1 = mFirestore.collection("FriendsReqList").document(current_user_id).collection("MyFriends").document(user_id);
                                                          batch.set(nycRef_1, status_0);


                                                          Map<String, Object> status_1 = new HashMap<>();
                                                          status_1.put("Sender", current_user_id);
                                                          status_1.put("Receiver", user_id);
                                                          status_1.put("type", "received");

                                                          DocumentReference nycRef_2 = mFirestore.collection("FriendsReqList").document(user_id).collection("MyFriends").document(current_user_id);
                                                          batch.set(nycRef_2, status_1);


                                                          Map<String, Object> notificationMessage = new HashMap<>();
                                                          notificationMessage.put("message", "You have friend request from " + user_id);
                                                          notificationMessage.put("from", current_user_id);
                                                          notificationMessage.put("receiver", user_id);


                                                          DocumentReference nycRef_3 = mFirestore.collection("UsersList/" + user_id + "/Notifications").document();
                                                          batch.set (nycRef_3, notificationMessage);

                                                          // Commit the batch
                                                          batch.commit().addOnSuccessListener(new OnSuccessListener<Void>() {
                                                              @Override
                                                              public void onSuccess(Void aVoid) {
                                                                 mProgressDialog.dismiss();

                                                                  mCurrent_state = "req_sent";
                                                                  mProfileSendReqBtn.setText("Cancel Friend Request");
                                                                  mDeclineBtn.setVisibility(View.INVISIBLE);
                                                                  mDeclineBtn.setEnabled(false);
                                                                  mProfileSendReqBtn.setEnabled(true);

                                                                  if (zz_Config.CHAT_TEST)
                                                                      Log.d("SEND_REQ", "Running 5");


                                                                  Toast.makeText(ProfileActivity.this, "Notification Sent.", Toast.LENGTH_LONG).show();

                                                              }


                                                          }).addOnFailureListener(new OnFailureListener() {
                                                              @Override
                                                              public void onFailure(@NonNull Exception e) {
                                                                  Toast.makeText(ProfileActivity.this, "Data updation failed  !!! Error : " + e.getMessage(), Toast.LENGTH_LONG).show();

                                                              }
                                                          });
///////////////////////////////////////////////////////////////////////////////////////////////////////
                                                      }

                                                      if (mCurrent_state.equals("req_sent"))
                                                      {


                                                          mProfileSendReqBtn.setEnabled(false);
                                                          // Get a new write batch
                                                          WriteBatch batch = mFirestore.batch();


                                                          // Set the value of 'NYC'
                                                          DocumentReference nycRef_1 = mFirestore.collection("FriendsReqList").document(current_user_id).collection("MyFriends").document(user_id);
                                                          batch.delete(nycRef_1);




                                                          DocumentReference nycRef_2 = mFirestore.collection("FriendsReqList").document(user_id).collection("MyFriends").document(current_user_id);
                                                          batch.delete(nycRef_2);





                                                          // Commit the batch
                                                          batch.commit().addOnSuccessListener(new OnSuccessListener<Void>() {
                                                              @Override
                                                              public void onSuccess(Void aVoid) {

                                                                  mProfileSendReqBtn.setEnabled(true);
                                                                  mCurrent_state = "not_friends";
                                                                  mProfileSendReqBtn.setText("Send Friend Request");

                                                                  mDeclineBtn.setVisibility(View.INVISIBLE);
                                                                  mDeclineBtn.setEnabled(false);
                                                                  mProgressDialog.dismiss();
                                                                  Toast.makeText(ProfileActivity.this, "Request Cancelled", Toast.LENGTH_LONG).show();

                                                                  mProgressDialog.dismiss();

                                                              }


                                                          }).addOnFailureListener(new OnFailureListener() {
                                                              @Override
                                                              public void onFailure(@NonNull Exception e) {

                                                                  Toast.makeText(ProfileActivity.this, "Data deletion failed  !!! Server Error : " + e.getMessage(), Toast.LENGTH_LONG).show();



                                                              }
                                                          });






                                                      }



                                                      if (mCurrent_state.equals("req_received"))
                                                      {
                                                          mProfileSendReqBtn.setEnabled(false);

                                                          // Get a new write batch
                                                          WriteBatch batch = mFirestore.batch();


                                                          /*
                                                           String other_user_id;
    String other_user_thumb_id;
    String other_user_name;




    String current_id;
    String current_thumb_id;
    String current_name;


                                                           */


                                                          String currentDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());

                                                          Map<String, Object> status_friends_1 = new HashMap<>();
                                                          status_friends_1.put("currentDate",currentDate);

                                                          status_friends_1.put("current_id",current_id);
                                                          status_friends_1.put("other_user_id",other_user_id);


                                                          status_friends_1.put("current_thumb_id",current_thumb_id);
                                                          status_friends_1.put("other_user_thumb_id",other_user_thumb_id);

                                                          status_friends_1.put("current_name",current_name);
                                                          status_friends_1.put("other_user_name",other_user_name);

                                                          status_friends_1.put("intialized",false);


                                                          // Set the value of 'NYC'
                                                          DocumentReference nycRef_1 =  mFirestore.collection("Friends").document(current_user_id).collection("MyFriends").document(user_id);
                                                          batch.set(nycRef_1, status_friends_1);

                                                          Map<String, Object> status_friends_2 = new HashMap<>();

                                                          status_friends_2.put("currentDate",currentDate);

                                                          status_friends_2.put("current_id",other_user_id);
                                                          status_friends_2.put("other_user_id",current_id);


                                                          status_friends_2.put("current_thumb_id",other_user_thumb_id);
                                                          status_friends_2.put("other_user_thumb_id",current_thumb_id);

                                                          status_friends_2.put("current_name",other_user_name);
                                                          status_friends_2.put("other_user_name",current_name);

                                                          status_friends_2.put("intialized",true);

                                                          // Set the value of 'NYC'
                                                          DocumentReference nycRef_2 =   mFirestore.collection("Friends").document(user_id).collection("MyFriends").document(current_user_id);
                                                          batch.set(nycRef_2, status_friends_2);







                                                          // Set the value of 'NYC'
                                                          DocumentReference nycRef_3 = mFirestore.collection("FriendsReqList").document(current_user_id).collection("MyFriends").document(user_id);
                                                          batch.delete(nycRef_3);




                                                          DocumentReference nycRef_4 = mFirestore.collection("FriendsReqList").document(user_id).collection("MyFriends").document(current_user_id);
                                                          batch.delete(nycRef_4);





                                                          // Commit the batch
                                                          batch.commit().addOnSuccessListener(new OnSuccessListener<Void>() {
                                                              @Override
                                                              public void onSuccess(Void aVoid) {



                                                                  mProfileSendReqBtn.setEnabled(true);
                                                                  mCurrent_state = "friends";
                                                                  mProfileSendReqBtn.setText("Unfriend this person");


                                                                  mDeclineBtn.setVisibility(View.INVISIBLE);
                                                                  mDeclineBtn.setEnabled(false);

                                                                  Toast.makeText(ProfileActivity.this, "Request accepted", Toast.LENGTH_LONG).show();


                                                                  mProgressDialog.dismiss();
                                                              }


                                                          }).addOnFailureListener(new OnFailureListener() {
                                                              @Override
                                                              public void onFailure(@NonNull Exception e) {

                                                                  Toast.makeText(ProfileActivity.this, "Data deletion failed  !!! Server Error : " + e.getMessage(), Toast.LENGTH_LONG).show();



                                                              }
                                                          });





                                                      }

                                                      if(mCurrent_state.equals("friends")) {



                                                          mProfileSendReqBtn.setEnabled(false);
                                                          // Get a new write batch
                                                          WriteBatch batch = mFirestore.batch();


                                                          // Set the value of 'NYC'
                                                          DocumentReference nycRef_1 = mFirestore.collection("Friends").document(current_user_id).collection("MyFriends").document(user_id);
                                                          batch.delete(nycRef_1);




                                                          DocumentReference nycRef_2 =  mFirestore.collection("Friends").document(user_id).collection("MyFriends").document(current_user_id);
                                                          batch.delete(nycRef_2);





                                                          // Commit the batch
                                                          batch.commit().addOnSuccessListener(new OnSuccessListener<Void>() {
                                                              @Override
                                                              public void onSuccess(Void aVoid) {

                                                                  mProfileSendReqBtn.setEnabled(true);
                                                                  mCurrent_state = "not_friends";
                                                                  mProfileSendReqBtn.setText("Send Friend Request");


                                                                  Toast.makeText(ProfileActivity.this, "unfriend", Toast.LENGTH_LONG).show();
                                                                  mProgressDialog.dismiss();
                                                              }


                                                          }).addOnFailureListener(new OnFailureListener() {
                                                              @Override
                                                              public void onFailure(@NonNull Exception e) {

                                                                  Toast.makeText(ProfileActivity.this, "Data deletion failed  !!! Server Error : " + e.getMessage(), Toast.LENGTH_LONG).show();



                                                              }
                                                          });





                                                      }




                                                      }


                                              });


        mDeclineBtn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub



                mProfileSendReqBtn.setEnabled(false);
                // Get a new write batch
                WriteBatch batch = mFirestore.batch();


                // Set the value of 'NYC'
                DocumentReference nycRef_1 = mFirestore.collection("FriendsReqList").document(current_user_id).collection("MyFriends").document(user_id);
                batch.delete(nycRef_1);




                DocumentReference nycRef_2 = mFirestore.collection("FriendsReqList").document(user_id).collection("MyFriends").document(current_user_id);
                batch.delete(nycRef_2);





                // Commit the batch
                batch.commit().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {


                        mProfileSendReqBtn.setEnabled(true);
                        mCurrent_state = "not_friends";
                        mProfileSendReqBtn.setText("Send Friend Request");

                        mDeclineBtn.setVisibility(View.INVISIBLE);
                        mDeclineBtn.setEnabled(false);
                        mProgressDialog.dismiss();
                        Toast.makeText(ProfileActivity.this, "Request Declined", Toast.LENGTH_LONG).show();

                        mProgressDialog.dismiss();

                    }


                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                        Toast.makeText(ProfileActivity.this, "Data deletion failed  !!! Server Error : " + e.getMessage(), Toast.LENGTH_LONG).show();



                    }
                });





            }
        });









    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);


        mAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();


        user_id = getIntent().getStringExtra("user_id");

        thumb_id = getIntent().getStringExtra("thumb");

        if(zz_Config.CHAT_TEST)
            Log.d("ID_USER_3",   user_id+"::"+thumb_id);

    //    final String user_id =  FirebaseAuth.getInstance().getUid();

        if(zz_Config.CHAT_TEST)
            Log.d("CHAT_TEST_2",   user_id+"::"+"Xws25zs9BshIYYDQCGwaBHEx1es1");

        mRootRef = FirebaseDatabase.getInstance().getReference();
        mCurrent_user = FirebaseAuth.getInstance().getCurrentUser();
        current_user_id = mCurrent_user.getUid().toString();
        mProfileImage = (ImageView) findViewById(R.id.profile_image);
        mProfileName = (TextView) findViewById(R.id.profile_displayName);
        mProfileStatus = (TextView) findViewById(R.id.profile_status);
        mProfileFriendsCount = (TextView) findViewById(R.id.profile_totalFriends);
        mProfileSendReqBtn = (Button) findViewById(R.id.profile_send_req_btn);
        mDeclineBtn = (Button) findViewById(R.id.profile_decline_btn);
        TestButton = (Button) findViewById(R.id.testButton);

        mDeclineBtn.setVisibility(View.INVISIBLE);
        mDeclineBtn.setEnabled(false);


    }
    @Override
    protected void onStop()
    {
        super.onStop();  // Always call the superclass method first

//        mFirestore.collection("UsersList").document(user_id).addSnapshotListener(MetadataChanges.INCLUDE, null);

          UsersList_registration_UserList.remove();
          UsersList_registration_FrdReq.remove();
          UsersList_registration_Frds.remove();
          UsersList_registration_CurrentList.remove();

    }

}


