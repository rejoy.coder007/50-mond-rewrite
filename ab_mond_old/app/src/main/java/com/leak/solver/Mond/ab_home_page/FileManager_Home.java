package com.leak.solver.Mond.ab_home_page;


import android.Manifest;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;

import android.preference.PreferenceManager;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import androidx.core.app.ActivityCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;


import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.leak.solver.Mond.R;
import com.leak.solver.Mond.aa_StartUp.aa_activity_StartUp;
import com.leak.solver.Mond.ab_home_page.aa_tabs.aa_home_screen_PagerAdapter;
import com.leak.solver.Mond.ad_Auth.StartPageActivity;
import com.leak.solver.Mond.ae_CloudUsers.UserListActivity;
import com.leak.solver.Mond.af_friendlist.FriendListsAcitivty;
import com.leak.solver.Mond.zd_cloud.aa_CloudActivity;
import com.leak.solver.Mond.zz_Config;
import com.leak.solver.Mond.ad_Auth.SettingsActivity;

public class FileManager_Home {
    public FirebaseUser current_user=null;

    Boolean Toolbar                                                                     ;
    private FirebaseAuth mAuth;
    public Intent intent=null;
    public Toolbar                                                                     toolbar=null;
    final int                                           MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 20;
    public final ActivityManager                                                       activityManager=null;


    public static boolean                                                       FOLDER_TAB_VISIBLE=true;
    public static boolean                                                          FAV_TAB_VISIBLE=true;

    aa_home_screen_PagerAdapter                                    aa_home_screen_pagerAdapter=null;

    public CustomViewPager                                                                        viewPager=null;
    TabLayout                                                                        tabLayout=null;
    ImageButton                                                                          back =null;

    public int                                                                          CURRENT_TAB =0;

    public Boolean                                                                            TAB_PRESS =false;
    aa_activity_home_screen aa_activity_home_screen;
    public FileManager_Home(aa_activity_home_screen aa_activity_home_screen) {

        this.aa_activity_home_screen=aa_activity_home_screen;
    }


    public void window_toolbar_navbar_bg(AppCompatActivity appCompatActivity, Context context)
    {
        Window window = appCompatActivity.getWindow();
        Drawable background = appCompatActivity.getResources().getDrawable(R.drawable.ad_toolbar_bg);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(appCompatActivity.getResources().getColor(android.R.color.transparent));
        window.setNavigationBarColor(appCompatActivity.getResources().getColor(android.R.color.transparent));
        window.setBackgroundDrawable(background);



        appCompatActivity.setSupportActionBar(aa_activity_home_screen.toolbar);
       // appCompatActivity.getSupportActionBar().setDisplayShowTitleEnabled(false);

    }


    public void getPermission(AppCompatActivity appCompatActivity)
    {

        if (ActivityCompat.checkSelfPermission(appCompatActivity, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
        {

            if(zz_Config.TEST)
            {
                Log.d("_#GOT_PERMISSION", "gotPermission --getPermission : aa_activity_home_screen -- A");
            }

          //  aa_activity_home_screen.mContext.fileManager_home_fav.ReadDirByThread_folder(appCompatActivity,false);
            aa_activity_home_screen.mContext.STATUS_RESUME =1;
            //setFirstTime_permission(appCompatActivity);
            //StartUpScreenThread_dir_fill(false);

            //  enable_tab();

            //    StartThread_dir_update(false);

            //StartThread_dir_update(false);

          //  aa_activity_home_screen.mContext.fileManager_home_fav.StartUpScreenThread_dir_fill(false,false,appCompatActivity);


            //aa_activity_home_screen.mContext.fileManager_home_fav.Init_Half_Read_Dir(appCompatActivity);

            if(zz_Config.TEST)
            {
                Log.d("_#GOT_PERMISSION", "gotPermission --getPermission : aa_activity_home_screen -- B");
            }



        }
        else
        {


            if (ActivityCompat.shouldShowRequestPermissionRationale(appCompatActivity, Manifest.permission.READ_EXTERNAL_STORAGE))
            {


                permission_snack_message(appCompatActivity);
                if(zz_Config.TEST)
                {
                    Log.d("_#NO_PERMISSION_PREV", "shouldShowRequestPermissionRationale --getPermission : aa_activity_home_scree");
                }

            }
            else
            {
                if(zz_Config.TEST)
                {
                    Log.d("_#NO_PERMISSION_FIRST", "noPermission --getPermission : aa_activity_home_screen -- A");
                }

                ActivityCompat.requestPermissions(appCompatActivity,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_SETTINGS},
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

                if(zz_Config.TEST)
                {
                    Log.d("_#NO_PERMISSION_FIRST", "noPermission --getPermission : aa_activity_home_screen -- B");
                }


            }
        }


    }

    public void permission_snack_message(final AppCompatActivity appCompatActivity)
    {



        Snackbar.make(appCompatActivity.findViewById(android.R.id.content),
                "Needs permission to read  video",
                Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {

                        ActivityCompat.requestPermissions(appCompatActivity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

                        if(zz_Config.TEST)
                        {
                            Log.d("_#SNACK_BAR", "permission_snack_message : aa_activity_home_screen");
                        }

                    }
                }).show();
    }

    private void setFirstTime_permission(AppCompatActivity appCompatActivity)
    {


        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(appCompatActivity);

        // first time
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("Permission", true);
        editor.commit();
        boolean ranBefore = preferences.getBoolean("Permission", false);

        if(zz_Config.TEST)
            Log.d("_#FIRST_FLAG", "isFirstTime shared pref : aa_activity_home_screen " + ranBefore);




    }



    public void onRequestPermissionsResult(AppCompatActivity appCompatActivity,int requestCode, String permissions[], int[] grantResults)
    {

        switch (requestCode)
        {


            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
            {

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    //                    grantResult[0] means it will check for the first postion permission which is READ_EXTERNAL_STORAGE
                    //                    grantResult[1] means it will check for the Second postion permission which is CAMERA
                    Toast.makeText(appCompatActivity, "Permission Granted", Toast.LENGTH_SHORT).show();

                    if(zz_Config.TEST){
                        Log.d("_#PERMISSION_RESULT", "granted onRequestPermissionsResult : aa_activity_home_screen -- A");
                    }

                    setFirstTime_permission(appCompatActivity);

                    aa_activity_home_screen.mContext.STATUS_RESUME =2;
                   // aa_activity_home_screen.mContext.fileManager_home_fav.StartUpScreenThread_dir_fill(true,false,appCompatActivity);
                    //aa_activity_home_screen.mContext.fileManager_home_fav.ReadDirByThread_folder(appCompatActivity);
                    //aa_activity_home_screen.mContext.fileManager_home_fav.Thread_Full_FAV.start();
                    //aa_activity_home_screen.mContext.fileManager_home_fav.ReadDirByThread_folder(appCompatActivity,true);


                    if(zz_Config.TEST){
                        Log.d("_#PERMISSION_RESULT", "granted onRequestPermissionsResult : aa_activity_home_screen -- B");
                    }


                }
                else
                {


                    permission_snack_message(appCompatActivity);

                }

            }
        }


    }


    public void initialize(AppCompatActivity appCompatActivity)
    {


        aa_activity_home_screen.mContext=(aa_activity_home_screen)appCompatActivity;
        aa_activity_StartUp.aaStartUp = aa_activity_home_screen.mContext._global_StartUP;




    }


    public void reset_variables(AppCompatActivity appCompatActivity)
    {



       if( aa_activity_StartUp.aaStartUp!=null && aa_activity_home_screen.mContext!=null)
       {
           aa_activity_StartUp.aaStartUp=null;


           if(!aa_activity_home_screen.mContext.BY_iNTENT)
               aa_activity_home_screen.mContext=null;
           Runtime.getRuntime().gc();
       }


    }

    public void Thread_Kiler()
    {


        Log.d("_#_TT_TEST_H", " first  Alive test A"+aa_activity_home_screen.mContext.fileManager_home_fav.Thread_Full_and_Half_FAV);

        int j=0;
        if(aa_activity_home_screen.mContext.fileManager_home_fav.Thread_Full_and_Half_FAV !=null)
        {

            if(aa_activity_home_screen.mContext.fileManager_home_fav.Thread_Full_and_Half_FAV.isAlive())
            {
                aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.THREAD_STOPPER =true;
            }

            while (aa_activity_home_screen.mContext.fileManager_home_fav.Thread_Full_and_Half_FAV.isAlive())
            {

              if(zz_Config.TEST)
                  Log.d("_#_TT_TEST_H", " Home first deead Alive test"+j+++aa_activity_home_screen.mContext.fileManager_home_fav.Thread_Full_and_Half_FAV);
            }


        }



         j=0;
        if(aa_activity_home_screen.mContext.fileManager_home_folder.Thread_Full_FOLDER !=null)
        {

            if(aa_activity_home_screen.mContext.fileManager_home_folder.Thread_Full_FOLDER.isAlive())
            {
                aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.THREAD_STOPPER =true;
            }

            while (aa_activity_home_screen.mContext.fileManager_home_folder.Thread_Full_FOLDER.isAlive())
            {

                if(zz_Config.TEST)
                    Log.d("_#_TT_TEST_H", " Home first deead Alive test"+j+++aa_activity_home_screen.mContext.fileManager_home_folder.Thread_Full_FOLDER);
            }


        }



       if(zz_Config.TEST)
          Log.d("_#_TT_TEST_H", " Home first  Alive test B"+j+++aa_activity_home_screen.mContext.fileManager_home_fav.Thread_Full_and_Half_FAV);




        //fileManager_home_fav.Init_Full_Read_Dir_Reset();
        //  fileManager_home_fav.Init_Half_Read_Dir_Reset();
        aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.THREAD_STOPPER =false;

        aa_activity_home_screen.mContext.fileManager_home_fav.Thread_Full_and_Half_FAV =null;



        //    fileManager_home.reset_variables(aa_activity_home_screen.this);


    }



    public void enable_tab(aa_activity_home_screen context)
    {



       aa_home_screen_pagerAdapter=new aa_home_screen_PagerAdapter(context.getSupportFragmentManager(),
               context);

        viewPager = (CustomViewPager) context.findViewById(R.id.viewpager);
        back = (ImageButton) context.findViewById(R.id.backb);


        if(zz_Config.TEST)
            Log.d("_#ENABLE_TAB", "enable_tab  : aa_activity_home_screen A" );




        viewPager.setAdapter(aa_home_screen_pagerAdapter);
        // Give the TabLayout the ViewPager
        tabLayout = (TabLayout) context.findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);





        LinearLayout tabStrip = ((LinearLayout)tabLayout.getChildAt(0));
        for(int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {

                    if(zz_Config.TEST)
                        Log.d("_@@_#TAB_ID", " ID Tab: aa_activity_home_screen A ::"+tabLayout.getSelectedTabPosition());
                    switch (tabLayout.getSelectedTabPosition())
                    {
                        case 1 :

                         //   viewPager.setCurrentItem(0);
                            if(current_user==null)
                            {
                               // SendtoStartActivity();
                            }
                            break;


                        case 2 :
                          //  viewPager.setCurrentItem(0);
                            break;


                    }
                    TAB_PRESS=true;
                    return false;
                }
            });
        }





        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position)
            {
                CURRENT_TAB=position;



                switch (position)
                {
                    case 0 :



                        current_user=mAuth.getCurrentUser();
                        //   viewPager.setCurrentItem(0);
                        if(current_user==null)
                        {


                             SendtoStartActivity();

                        }
                        else
                        {
                            Toast.makeText(aa_activity_home_screen.mContext,"User :: "+current_user.getDisplayName()+current_user.getEmail() , Toast.LENGTH_SHORT).show();

                        }
                        break;


                    case 2 :
                        //  viewPager.setCurrentItem(0);
                        break;


                }


                if(zz_Config.TEST)
                    Log.d("_#ENABLE_TAB", "enable_tab : aa_activity_home_screen B"+CURRENT_TAB );





            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
/*
        ViewGroup slidingTabStrip =  (ViewGroup)  tabLayout.getChildAt(0);
        for (int i = 0; i < tabLayout.getTabCount(); i++)
        {
            View tab = slidingTabStrip.getChildAt(i);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) tab.getLayoutParams();
            if(i==2)
                layoutParams.weight = 0;
            else
                layoutParams.weight = 1;
            tab.setLayoutParams(layoutParams);


        }*/


        viewPager.setCurrentItem(1);

        if(zz_Config.TEST)
            Log.d("_#ENABLE_TAB", "enable_tab : aa_activity_home_screen B" );



    }

    public void disable_tab(int no,int sel_tab)
    {

        ViewGroup slidingTabStrip =  (ViewGroup)  tabLayout.getChildAt(0);

        for (int i = 0; i <   tabLayout.getTabCount(); i++)
        {
            View tab = slidingTabStrip.getChildAt(i);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) tab.getLayoutParams();
            if(i==no)
                layoutParams.weight = 0;
            else
                layoutParams.weight = 1;
            tab.setLayoutParams(layoutParams);


        }

        viewPager.setCurrentItem(sel_tab);


    }

    public void SendtoStartActivity(){






        Intent intent = new Intent(aa_activity_home_screen.mContext, StartPageActivity.class);


        if(zz_Config.TEST)
        {


            //   Log.d("_##YES","from intent"+paracelableFolderVideoPlayer.mContext+paracelableFolderVideoPlayer.CurrentIndex  );


        }
       // aa_activity_home_screen.mContext.BY_iNTENT=true;

        aa_activity_home_screen.mContext.startActivity(intent);



    }

    public void SendtoSettingsActivity( ) {

        Intent intent = new Intent(aa_activity_home_screen.mContext, SettingsActivity.class);


        if(zz_Config.TEST)
        {


            //   Log.d("_##YES","from intent"+paracelableFolderVideoPlayer.mContext+paracelableFolderVideoPlayer.CurrentIndex  );


        }
        // aa_activity_home_screen.mContext.BY_iNTENT=true;

        aa_activity_home_screen.mContext.startActivity(intent);
    }

    public void SendtoCloudActivity( ) {

        Intent intent = new Intent(aa_activity_home_screen.mContext, aa_CloudActivity.class);


        if(zz_Config.TEST)
        {


            //   Log.d("_##YES","from intent"+paracelableFolderVideoPlayer.mContext+paracelableFolderVideoPlayer.CurrentIndex  );


        }
        // aa_activity_home_screen.mContext.BY_iNTENT=true;

        aa_activity_home_screen.mContext.startActivity(intent);
    }

    public void SendtoFriendssActivity( ) {

         Intent intent = new Intent(aa_activity_home_screen.mContext, FriendListsAcitivty.class);

       // Intent intent = new Intent(aa_activity_home_screen.mContext, FirestoreChatActivity.class);
        if(zz_Config.TEST)
        {


            //   Log.d("_##YES","from intent"+paracelableFolderVideoPlayer.mContext+paracelableFolderVideoPlayer.CurrentIndex  );


        }
        // aa_activity_home_screen.mContext.BY_iNTENT=true;

        aa_activity_home_screen.mContext.startActivity(intent);
    }


    public void SendtoAllUsersActivity( ) {

      //  Intent intent = new Intent(aa_activity_home_screen.mContext, UsersActivity.class);
        Intent intent = new Intent(aa_activity_home_screen.mContext, UserListActivity.class);

        if(zz_Config.TEST)
        {


            //   Log.d("_##YES","from intent"+paracelableFolderVideoPlayer.mContext+paracelableFolderVideoPlayer.CurrentIndex  );


        }
        // aa_activity_home_screen.mContext.BY_iNTENT=true;

        aa_activity_home_screen.mContext.startActivity(intent);
    }
















}
