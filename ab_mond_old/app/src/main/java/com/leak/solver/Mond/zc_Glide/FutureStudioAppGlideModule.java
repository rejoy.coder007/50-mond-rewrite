package com.leak.solver.Mond.zc_Glide;

import android.content.Context;
import android.net.Uri;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;
import com.leak.solver.Mond.R;
import com.leak.solver.Mond.ab_home_page.aa_tabs.ac_folderview.aa_Recycler.ViewHolder_folder;


@GlideModule
public class FutureStudioAppGlideModule extends AppGlideModule {


    public void  add_image_to_view(final Context context, ViewHolder_folder viewHolderFolder, Uri uri)
    {



        final Boolean[] errorF = {false};


        GlideApp.with(context)

                .load(uri)
               // .apply(new RequestOptions().override(100, 70))

                .thumbnail(0.1f)
                .error(GlideApp.with(context).load(R.drawable.file_error))
               // .override(150, 150) // resizes the image to these dimensions (in pixel)
            //    .centerCrop() // this cropping technique scales the image so that it fills the requested bounds and then crops the extra.

                .into(viewHolderFolder.video_ref);



      //  if(errorF[0])
      //  GlideApp.with(context).load(R.drawable.file_error);



       // Log.d("#FILE_ERROR", "add_image_to_view: "+viewHolderFolder.video_thumb.getBackground().equals(R.drawable.file_error));



    }

    public void  add_image_to_view(final Context context, ViewHolder_folder viewHolderFolder, String uri)
    {



        final Boolean[] errorF = {false};


        GlideApp.with(context)

                .load(uri)
                // .apply(new RequestOptions().override(100, 70))

                .thumbnail(0.1f)
                .error(GlideApp.with(context).load(R.drawable.file_error))
                // .override(150, 150) // resizes the image to these dimensions (in pixel)
                //    .centerCrop() // this cropping technique scales the image so that it fills the requested bounds and then crops the extra.

                .into(viewHolderFolder.video_ref);



        //  if(errorF[0])
        //  GlideApp.with(context).load(R.drawable.file_error);



        // Log.d("#FILE_ERROR", "add_image_to_view: "+viewHolderFolder.video_thumb.getBackground().equals(R.drawable.file_error));



    }



}