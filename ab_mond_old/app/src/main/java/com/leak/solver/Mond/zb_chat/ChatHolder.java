package com.leak.solver.Mond.zb_chat;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.leak.solver.Mond.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public   class ChatHolder extends RecyclerView.ViewHolder {

    public View mView;





    public TextView chatmessage_S ;
    public TextView chatuser_S;
    public TextView chat_time_S  ;
    public ImageView pic_S   ;
    public RelativeLayout Send;


    public TextView chatmessage_R ;
    public TextView chatuser_R;
    public TextView chat_time_R  ;
    public ImageView pic_R   ;
    public RelativeLayout  Receive;


    public ChatHolder(View itemView)
    {
        super(itemView);

        mView = itemView;






        Send = (RelativeLayout)itemView.findViewById(R.id.send);
        Receive = (RelativeLayout)itemView.findViewById(R.id.receive);

        chatmessage_S           = (TextView)  itemView.findViewById(R.id.chatmessage_S);
        chatuser_S              = (TextView)  itemView.findViewById(R.id.chatuser_S);
        chat_time_S             = (TextView)  itemView.findViewById(R.id.chat_time_S);
        pic_S                   = (ImageView) itemView.findViewById(R.id.chat_photo_S);


        chatmessage_R           = (TextView)  itemView.findViewById(R.id.chatmessage_R);
        chatuser_R              = (TextView)  itemView.findViewById(R.id.chatuser_R);
        chat_time_R             = (TextView)  itemView.findViewById(R.id.chat_time_R);
        pic_R                   = (ImageView) itemView.findViewById(R.id.chat_photo_R);

    }


    public void setSenderImage(String thumb_image, Context ctx)
    {




        Picasso.get().load(thumb_image).placeholder(R.drawable.default_avatar).into(pic_S, new Callback() {

            @Override
            public void onSuccess() {



                //  Toast.makeText(UsersActivity.this,"image uploaded", Toast.LENGTH_SHORT).show();


            }

            @Override
            public void onError(Exception e) {

            }
        });

    }

    public void setReceiveImage(String thumb_image, Context ctx)
    {




        Picasso.get().load(thumb_image).placeholder(R.drawable.default_avatar).into(pic_R, new Callback() {

            @Override
            public void onSuccess() {



                //  Toast.makeText(UsersActivity.this,"image uploaded", Toast.LENGTH_SHORT).show();


            }

            @Override
            public void onError(Exception e) {

            }
        });

    }



    /*
    public void setRoot()
    {

        root = (RelativeLayout) mView.findViewById(R.id.root);


    }

    public void setDisplayName(String name)
    {

        TextView userNameView = (TextView) mView.findViewById(R.id.user_single_name);
        userNameView.setText(name);

    }


    public void setUserStatus(String status)
    {

        TextView userStatusView = (TextView) mView.findViewById(R.id.date_and_time);
        userStatusView.setText(status);


    }



    */

}