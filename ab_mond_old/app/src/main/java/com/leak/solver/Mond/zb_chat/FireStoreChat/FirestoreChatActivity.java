package com.leak.solver.Mond.zb_chat.FireStoreChat;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.MetadataChanges;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.WriteBatch;
import com.leak.solver.Mond.R;
import com.leak.solver.Mond.zb_chat.GetTimeAgo;
import com.leak.solver.Mond.zz_Config;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Class demonstrating how to setup a {@link RecyclerView} with an adapter while taking sign-in
 * states into consideration. Also demonstrates adding data to a ref and then reading it ad_back using
 * the {@link FirestoreRecyclerAdapter} to build a simple chat app.
 * <p>
 * For a general intro to the RecyclerView, see <a href="https://developer.android.com/training/material/lists-cards.html">Creating
 * Lists</a>.
 */
public class FirestoreChatActivity extends AppCompatActivity
        implements FirebaseAuth.AuthStateListener {
    private static final String TAG = "FirestoreChatActivity";

/*
    private static final CollectionReference SenderChatCollection =
            FirebaseFirestore.getInstance().collection("chats");

            */

    private   CollectionReference SenderChatCollection;
    private   CollectionReference ReceiverChatCollection;


    private   DocumentReference SenderChatCollectionD;
    private   DocumentReference ReceiverChatCollectionD;


    Query SenderChatQuery;
    Query ReceiverChatQuery;
    /** Get the last 50 chat messages ordered by timestamp . */

    /*
    private     Query SenderChatQuery =
            SenderChatCollection.orderBy("timestamp", Query.Direction.DESCENDING).limit(50);

            */

    static {
        FirebaseFirestore.setLoggingEnabled(true);
    }

    RecyclerView mRecyclerView;
    Button mSendButton ;


    //   @BindView(R.id.messageEdit)

    EditText mMessageEdit ;


    TextView mEmptyListMessage  ;


    String user_id  ;
    String user_name  ;
    String current_id  ;
    String current_name ;
    String current_image ;
    String other_image  ;
    private Toolbar mChatToolbar;
    private TextView mTitleView;
    private TextView mLastSeenView;
    private CircleImageView mProfileImage;
    private FirebaseAuth mAuth;

    private FirebaseFirestore mFirestore;
    ListenerRegistration UsersList_registration_CurrentList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);


        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setReverseLayout(true);
        manager.setStackFromEnd(true);



        mChatToolbar = (Toolbar) findViewById(R.id.chat_app_bar);
        setSupportActionBar(mChatToolbar);

        ActionBar actionBar = getSupportActionBar();

        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);

        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View action_bar_view = inflater.inflate(R.layout.chat_custom_bar, null);
        actionBar.setCustomView(action_bar_view);

        mTitleView = (TextView) findViewById(R.id.custom_bar_title);
        mLastSeenView = (TextView) findViewById(R.id.custom_bar_seen);
        mProfileImage = (CircleImageView) findViewById(R.id.custom_bar_image);


///////////////////////////////////////////////////////////////////////////////////////////////
        user_id = getIntent().getStringExtra("user_id");
        user_name = getIntent().getStringExtra("user_name");
        current_id = getIntent().getStringExtra("current_id");
        current_name = getIntent().getStringExtra("current_name");
        current_image = getIntent().getStringExtra("current_image");
        other_image = getIntent().getStringExtra("other_image");




        mAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();



        SenderChatCollection =
                FirebaseFirestore.getInstance().collection("chatsD").document(current_id).collection(user_id);

        ReceiverChatCollection =
                FirebaseFirestore.getInstance().collection("chatsD").document(user_id).collection(current_id);




        SenderChatQuery =
                SenderChatCollection.orderBy("timestamp", Query.Direction.DESCENDING).limit(50);
        ReceiverChatQuery =
                SenderChatCollection.orderBy("timestamp", Query.Direction.DESCENDING).limit(50);


       SenderChatCollectionD  = FirebaseFirestore.getInstance().collection("chatsD").document(current_id).collection(user_id).document();
       ReceiverChatCollectionD =  FirebaseFirestore.getInstance().collection("chatsD").document(user_id).collection(current_id).document();
        ;


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        //  @BindView(R.id.messagesList)
          mRecyclerView = (RecyclerView)findViewById(R.id.messagesList);


        RecyclerView.ItemAnimator animator = mRecyclerView.getItemAnimator();
        if (animator instanceof SimpleItemAnimator) {
            ((SimpleItemAnimator) animator).setSupportsChangeAnimations(false);
        }

       // @BindView(R.id.sendButton)
          mSendButton = (Button)findViewById(R.id.sendButton);


     //   @BindView(R.id.messageEdit)

          mMessageEdit = (EditText)findViewById(R.id.messageEdit);


                  //mEmptyListMessage = (TextView)findViewById(R.id.emptyTextView);


        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(manager);

        mRecyclerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View view, int left, int top, int right, int bottom,
                                       int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if (bottom < oldBottom) {
                    mRecyclerView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mRecyclerView.smoothScrollToPosition(0);
                        }
                    }, 100);
                }
            }
        });
/*
        ImeHelper.setImeOnDoneListener(mMessageEdit, new ImeHelper.DonePressedListener() {
            @Override
            public void onDonePressed() {
                onSendClick();
            }
        });*/


        mMessageEdit.setClickable(true);
        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSendClick();
            }
        });

      //  mMessageEdit.callOnClick();


        Map<String, Object> UserStat = new HashMap<>();
        UserStat.put("seen", true);

      //  if(zz_Config.CHAT_TEST)
         //   Log.d("CHAT_ACTIVITY", "onCreate: "+mCurrentUserId+"::"+user_id);

        mFirestore.collection("Chat").document(current_id).collection("MyFriends").document(user_id).set(UserStat);

        UsersList_registration_CurrentList = mFirestore.collection("UsersList").document(user_id).addSnapshotListener(MetadataChanges.INCLUDE, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                @Nullable FirebaseFirestoreException e) {


                if (e != null) {
                    System.err.println("Listen failed:" + e);
                    return;
                }



                Boolean online = documentSnapshot.getBoolean("online");
                Timestamp time_stamp = documentSnapshot.getTimestamp("time_stamp");
                String image = documentSnapshot.getString("image");


                String lastSeenTime="";


                if(online) {

                    mLastSeenView.setText("Online");


                    if(zz_Config.CHAT_TEST)
                    {
                        Log.d("ONLINE_TEST", "1"+time_stamp+"::"+time_stamp.toDate().getTime()+"::"+lastSeenTime);
                    }



                } else {

                    GetTimeAgo getTimeAgo = new GetTimeAgo();







                    lastSeenTime = getTimeAgo.getTimeAgo(time_stamp.toDate().getTime(), FirestoreChatActivity.this);

                    mLastSeenView.setText(lastSeenTime);


                    if(zz_Config.CHAT_TEST)
                    {
                        Log.d("ONLINE_TEST", "2"+time_stamp+"::"+time_stamp.toDate().getTime()+"::"+lastSeenTime);
                    }


                }




                if(!image.equals("default"))
                {
                    Picasso.get().load(image).networkPolicy(NetworkPolicy.OFFLINE).placeholder(R.drawable.default_avatar).into(mProfileImage, new Callback()
                    {

                        @Override
                        public void onSuccess() {

                            Log.d("#CONFIG_1", "image uploaded");
                            //  Toast.makeText(CloudSettingsActivity.this, "image uploaded", Toast.LENGTH_SHORT).show();


                        }

                        @Override
                        public void onError(Exception e) {

                            Picasso.get().load(image).placeholder(R.drawable.default_avatar).into(mProfileImage);


                        }
                    });


                }


                // ...
            }
        });


    }

    @Override
    public void onStart() {
        super.onStart();
        if (isSignedIn()) { attachRecyclerViewAdapter(); }
        FirebaseAuth.getInstance().addAuthStateListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        FirebaseAuth.getInstance().removeAuthStateListener(this);
    }

    @Override
    public void onAuthStateChanged(@NonNull FirebaseAuth auth) {
        mSendButton.setEnabled(isSignedIn());
        mMessageEdit.setEnabled(isSignedIn());

        if (isSignedIn()) {
            attachRecyclerViewAdapter();
        } else {
            Toast.makeText(this, R.string.signing_in, Toast.LENGTH_SHORT).show();
          //  auth.signInAnonymously().addOnCompleteListener(new SignInResultNotifier(this));
        }
    }

    private boolean isSignedIn() {
        return FirebaseAuth.getInstance().getCurrentUser() != null;
    }

    private void attachRecyclerViewAdapter() {
        final RecyclerView.Adapter adapter = newAdapter();

        // Scroll to bottom on new messages
        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                mRecyclerView.smoothScrollToPosition(0);
            }
        });

        mRecyclerView.setAdapter(adapter);
    }

   // @OnClick(R.id.sendButton)



    public void onSendClick() {
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        String name = current_name;

        onAddMessage(new Chat(name, mMessageEdit.getText().toString(), uid,FieldValue.serverTimestamp().toString()));

        mMessageEdit.setText("");
    }

    @NonNull
    private RecyclerView.Adapter newAdapter() {
        FirestoreRecyclerOptions<Chat> options =
                new FirestoreRecyclerOptions.Builder<Chat>()
                        .setQuery(SenderChatQuery, Chat.class)
                        .setLifecycleOwner(this)
                        .build();

        return new FirestoreRecyclerAdapter<Chat, ChatHolder>(options) {
            @NonNull
            @Override
            public ChatHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                return new ChatHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.message, parent, false));
            }

            @Override
            protected void onBindViewHolder(@NonNull ChatHolder holder, int position, @NonNull Chat model) {
                holder.bind(model);
            }

            @Override
            public void onDataChanged() {
                // If there are no chat messages, show a view that invites the user to add a message.
              //  mEmptyListMessage.setVisibility(getItemCount() == 0 ? View.VISIBLE : View.GONE);
            }
        };
    }

    private void onAddMessage(@NonNull Chat chat) {

        /*
        SenderChatCollection.add(chat).addOnFailureListener(this, new OnFailureListener() {


            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e(TAG, "Failed to write message", e);
            }
        });*/


        /*
        SenderChatCollection.add(chat).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
            @Override
            public void onSuccess(DocumentReference documentReference) {

                ReceiverChatCollection.add(chat).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {

                    }
                });


            }
        });
       */


        WriteBatch batch =  FirebaseFirestore.getInstance().batch();



        if(zz_Config.CHAT_TEST)
            Log.d("SEND_MSG", "sendMessage:  1");

        Map<String, Object> chat_status_1 = new HashMap<>();
        chat_status_1.put("message",chat.getMessage());
        chat_status_1.put("name",chat.getName());
        chat_status_1.put("mTimestamp",   FieldValue.serverTimestamp());
        chat_status_1.put("uid",chat.getUid());
        chat_status_1.put("TimeStampStr",chat.getTimeStampStr());



        //batch.set(SenderChatCollectionD, chat_status_1);
      //  batch.set(ReceiverChatCollectionD, chat_status_1);


        batch.set(SenderChatCollection.document(), chat);
         batch.set(ReceiverChatCollection.document(), chat);

        batch.commit().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

                mMessageEdit.setText("");


                Toast.makeText(FirestoreChatActivity.this, "Your Message sent", Toast.LENGTH_SHORT).show();

                if(zz_Config.CHAT_TEST)
                    Log.d("SEND_MSG", "sendMessage from batch:  ");


            }


        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {



                if(zz_Config.CHAT_TEST)
                    Log.d("SEND_MSG", "sendMessage from batch :: "+ e.getMessage());

            }
        });





    }
}
