package com.leak.solver.Mond.ab_home_page.aa_tabs;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import android.util.Log;


import com.leak.solver.Mond.ab_home_page.aa_tabs.aa_ChatFragments.FriendsFragment;
import com.leak.solver.Mond.ab_home_page.aa_tabs.aa_ChatFragments.SpaceShareFragment;
import com.leak.solver.Mond.ab_home_page.aa_tabs.ab_favorites.FavFolderFragment;
import com.leak.solver.Mond.ab_home_page.aa_tabs.ac_folderview.FolderFragment;
import com.leak.solver.Mond.ab_home_page.aa_tabs.aa_ChatFragments.RequestFragment;
import com.leak.solver.Mond.zz_Config;

public class aa_home_screen_PagerAdapter extends FragmentPagerAdapter
{

    public static final String                                     ARG_PAGE = "ARG_PAGE";


    final int                                                              PAGE_COUNT =5;
    //private String tabTitles[] = new String[] { "SpaceShare", "Favorite", "FolderView","group","contacts" };
    private String tabTitles[] = new String[] { "SpaceShare", "Favorite", "FolderView","Requests","Friends"  };
    private Context                                                              context;

    public aa_home_screen_PagerAdapter(FragmentManager fm, Context context)
    {
        super(fm);
        this.context = context;

        if(zz_Config.TEST)
        {
            Log.d("_#_FRAG_PAGER_ADAPTER", "constructor:   : aa_activity_home_screen  ");

        }

    }

    @Override
    public int getCount()
    {
        if(zz_Config.TEST)
        {
            Log.d("_#_FRAG_PAGER_ADAPTER", "getCount:   : aa_activity_home_screen  _"+PAGE_COUNT);

        }

        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position)
    {
        Fragment fragment=null;

        if(zz_Config.TEST)
        {
            Log.d("_#_FRAG_PAGER_ADAPTER", "getItem:   : aa_activity_home_screen  _ A_"+position+"_"+(position + 1));

        }

        switch (position)
        {
            case 0:
            {
                fragment = new SpaceShareFragment();
                Bundle args = new Bundle();
                args.putInt(ARG_PAGE, 0);
                fragment.setArguments(args);
                break;
            }


            case 3:
            {
                fragment = new RequestFragment();
                Bundle args = new Bundle();
                args.putInt(ARG_PAGE, 3);
                fragment.setArguments(args);
                break;
            }

            case 4:
            {
                fragment = new FriendsFragment();
                Bundle args = new Bundle();
                args.putInt(ARG_PAGE, 4);
                fragment.setArguments(args);
                break;
            }




            case 1:
            {
                fragment = new FavFolderFragment();
                Bundle args = new Bundle();
                args.putInt(ARG_PAGE, 1);
                fragment.setArguments(args);
                break;
            }



            case 2:
            {
                fragment = new FolderFragment();
                Bundle args = new Bundle();
                args.putInt(ARG_PAGE, 2);
                fragment.setArguments(args);
                break;
            }


        }

        if(zz_Config.TEST)
        {
            Log.d("_#_FRAG_PAGER_ADAPTER", "getItem:   : aa_activity_home_screen  _ B_"+position+"_"+(position + 1));

        }

        return fragment;

    }

    @Override
    public CharSequence getPageTitle(int position)
    {

        if(zz_Config.TEST)
        {
            Log.d("_#_FRAG_PAGER_ADAPTER", "getPageTitle:   : aa_activity_home_screen  _"+position);

        }

        return tabTitles[position];
    }
}
