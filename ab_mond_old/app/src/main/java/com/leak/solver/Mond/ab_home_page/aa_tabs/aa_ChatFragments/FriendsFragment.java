package com.leak.solver.Mond.ab_home_page.aa_tabs.aa_ChatFragments;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.leak.solver.Mond.R;
import com.leak.solver.Mond.ae_CloudUsers.FriendsResponse;
import com.leak.solver.Mond.ae_CloudUsers.UserListActivity;
import com.leak.solver.Mond.zz_Config;


/**
 * A simple {@link Fragment} subclass.
 */
public class FriendsFragment extends Fragment {


    RecyclerView friendList;

    FirebaseFirestore db;


    private FirestoreRecyclerAdapter adapter;
    LinearLayoutManager linearLayoutManager;

    public FriendsFragment() {
        // Required empty public constructor
    }


    public static final String ARG_PAGE = "ARG_PAGE";

    private int mPage;



    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt(ARG_PAGE);

        if(zz_Config.TEST)
        {
            Log.d("_#_SPACE_FRAG_ON_CREATE", "FRAGMENT SPACE ON_CREATE   : aa_activity_home_screen__ "+mPage);

        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View rootView=null;
        rootView = inflater.inflate(R.layout.ad_a_c_fragment_home_friends, container, false);
        friendList = rootView.findViewById(R.id.list);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        friendList.setLayoutManager(linearLayoutManager);
        friendList.setHasFixedSize(true);
        init();
        getFriendList();




        adapter.startListening();

        if(zz_Config.TEST)
        {
            Log.d("_#_SPACE_FRAG__ON_VIEW", "FRAGMENT SPACE VIRECREATE   : aa_activity_home_screen__ "+mPage);

        }

        return rootView;
    }


    private void init(){
        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        friendList.setLayoutManager(linearLayoutManager);
        db = FirebaseFirestore.getInstance();
    }




    private void getFriendList()
    {
        Query query = db.collection("UsersList");



        FirestoreRecyclerOptions<FriendsResponse> response = new FirestoreRecyclerOptions.Builder<FriendsResponse>()
                .setQuery(query, FriendsResponse.class)
                .build();

        adapter = new FirestoreRecyclerAdapter<FriendsResponse, UserListActivity.FriendsHolder>(response) {
            @Override
            public void onBindViewHolder(UserListActivity.FriendsHolder holder, int position, FriendsResponse model) {


                holder.setDisplayName(model.getName());
                holder.setUserStatus(model.getStatus());
                holder.setUserImage(model.getThumb_image(), getActivity());



                holder.setRoot();
                final String user_id = getSnapshots().getSnapshot(position).getId();

                Log.d("C_ID", user_id);

                holder.root.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        //Toast.makeText(UsersActivity.this, String.valueOf(position), Toast.LENGTH_SHORT).show();
                       // SendtoAllUsersProfleActivity(user_id);
                        Log.d("Clicked id", user_id);

                    }
                });
            }

            @Override
            public UserListActivity.FriendsHolder onCreateViewHolder(ViewGroup group, int i) {
                View view = LayoutInflater.from(group.getContext())
                        .inflate(R.layout.users_single_layout, group, false);

                return new UserListActivity.FriendsHolder(view);
            }

            @Override
            public void onError(FirebaseFirestoreException e) {
                Log.e("error", e.getMessage());
            }
        };

        adapter.notifyDataSetChanged();
        friendList.setAdapter(adapter);
    }




    /*
    @Override
    public void onStop() {

        if(zz_Config.TEST)
        {
            Log.d("&_@@_#SPC_FRAG_STP", " VISIBLE folder fragment  : aa_activity_home_screen__ "+mPage+"::");

        }





        Runtime.getRuntime().gc();


        super.onStop();  // Always call the superclass method first

    }








    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }




    public void SendtoAllUsersProfleActivity( String user_id ) {

        Intent intent = new Intent(UserListActivity.this, ProfileActivity.class);
        intent.putExtra("user_id", user_id);
        startActivity(intent);
    }


*/

}
