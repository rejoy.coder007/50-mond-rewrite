package com.leak.solver.Mond.ac_VideoPlayer;

import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.Toast;

import com.leak.solver.Mond.zz_Config;

import static com.leak.solver.Mond.zz_Config.brightnessSpeed;

public class MyGestureListener implements GestureDetector.OnGestureListener{


    public float sWidth,sHeight;

    public VideoPlayer videoPlayer=null;



    int STATUS = 0;


    private static final long VELOCITY_THRESHOLD = 3000;

    @Override
    public boolean onDown(final MotionEvent e){ return false; }

    @Override
    public void onShowPress(final MotionEvent e){ }

    @Override
    public boolean onSingleTapUp(final MotionEvent e){ return false; }

    @Override
    public boolean onScroll(final MotionEvent e1, final MotionEvent e2, final float distanceX,
                            final float distanceY){

    //    Log.i("__TAG", "POS :: "+distanceX+"::"+distanceY);

/*
        if(e2.getRawX()<(sWidth/2))
        {

            STATUS=0;
        }
        else
        {
            STATUS=1;
        }

    //    Log.i("__TAG", "POS :: "+e2.getRawX()+"::"+e2.getRawY()+"::"+sWidth/2+"::"+STATUS);
*/
        return false;


    }

    @Override
    public void onLongPress(final MotionEvent e){ }

    @Override
    public boolean onFling(final MotionEvent e1, final MotionEvent e2,
                           final float velocityX,
                           final float velocityY){



        /*

        if(Math.abs(velocityX) < VELOCITY_THRESHOLD
                && Math.abs(velocityY) < VELOCITY_THRESHOLD){
            return false;//if the fling is not fast enough then it's just like drag
        }

        //if velocity in X direction is higher than velocity in Y direction,
        //then the fling is horizontal, else->vertical
        if(Math.abs(velocityX) > Math.abs(velocityY))
        {
            if(velocityX >= 0)
            {
              //  Log.d("__TAG", "swipe right");
            }
            else
            {//if velocityX is negative, then it's towards left
              //  Log.d("__TAG", "swipe left");
            }
        }
        else
        {
            if(velocityY >= 0)
            {


                if(STATUS>0)
                {

                    float currentvolume=0;

                    currentvolume =   videoPlayer.player.getVolume()-videoPlayer.player.getVolume()* zz_Config.SOUND_RANGE;
                    if(currentvolume>=0)
                    videoPlayer.player.setVolume(currentvolume);
                   // Log.d("__TAG", "swipe down"+currentvolume);
                }
                else
                {
                    boolean settingsCanWrite = Settings.System.canWrite(videoPlayer);

                    if(!settingsCanWrite) {
                        Toast.makeText(videoPlayer, "Require Permission to Handle Screen Brightness", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.parse("package:" + videoPlayer.getPackageName()));
                        videoPlayer.startActivityForResult(intent, 200);

                    }
                    else
                        videoPlayer.changeScreenBrightness(videoPlayer,-zz_Config.brightnessSpeed);

                }

                //Context context, int screenBrightnessValue,   double brightnessSpeed
               // videoPlayer.changeScreenBrightness(videoPlayer,-1*brightnessSpeed);
            }
            else
            {
                //Log.d("__TAG", "swipe up");

                if(STATUS>0)
                {

                    float currentvolume;
                    currentvolume =   videoPlayer.player.getVolume()+videoPlayer.player.getVolume()*zz_Config.SOUND_RANGE;
                    videoPlayer.player.setVolume(currentvolume);
                  //  Log.d("__TAG", "swipe down"+currentvolume);
                }
                else{
                    boolean settingsCanWrite = Settings.System.canWrite(videoPlayer);

                    if(!settingsCanWrite) {
                        Toast.makeText(videoPlayer, "Require Permission to Handle Screen Brightness", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.parse("package:" + videoPlayer.getPackageName()));
                        videoPlayer.startActivityForResult(intent, 200);

                    }
                    else
                        videoPlayer.changeScreenBrightness(videoPlayer,zz_Config.brightnessSpeed);

                }

            }
        }
*/
        return true;
    }
}