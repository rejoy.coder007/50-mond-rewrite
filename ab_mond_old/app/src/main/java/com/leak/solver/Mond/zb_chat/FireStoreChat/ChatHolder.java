package com.leak.solver.Mond.zb_chat.FireStoreChat;

import android.graphics.PorterDuff;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.RotateDrawable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.leak.solver.Mond.R;
import com.leak.solver.Mond.zz_Config;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ChatHolder extends RecyclerView.ViewHolder {
    private final TextView mNameField;
    private final TextView timeview;
    private final TextView mTextField;
    private final FrameLayout mLeftArrow;
    private final FrameLayout mRightArrow;
    private final RelativeLayout mMessageContainer;
    private final LinearLayout mMessage;
    private final int mGreen300;
    private final int mGray300;

    public ChatHolder(@NonNull View itemView) {
        super(itemView);





        mNameField = itemView.findViewById(R.id.name_text);
        mTextField = itemView.findViewById(R.id.message_text);
        timeview = itemView.findViewById(R.id.time_chat);

        mLeftArrow = itemView.findViewById(R.id.left_arrow);
        mRightArrow = itemView.findViewById(R.id.right_arrow);
        mMessageContainer = itemView.findViewById(R.id.message_container);
        mMessage = itemView.findViewById(R.id.message);
        mGreen300 = ContextCompat.getColor(itemView.getContext(), R.color.material_green_300);
        mGray300 = ContextCompat.getColor(itemView.getContext(), R.color.material_gray_300);
    }

    public void bind(@NonNull AbstractChat chat) {
        setName(chat.getName());

        String pattern = "yyyy-MM-dd HH:mm:ss a";
        SimpleDateFormat simpleDateFormat =new SimpleDateFormat(pattern);
        setMessage(chat.getMessage());





        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();






        if(chat.getTimestamp()!=null)
        {
            // simpleDateFormat.format(chat.getTimestamp());

          //  Log.d("OBJECT_CHAT_NO_NULL", String.valueOf(chat));
            //   Log.d("OBJECT_CHAT_NO_NULL", );

            DateFormat inputFormatter1 = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date date1 = inputFormatter1.parse("2015-2-22");

                DateFormat outputFormatter1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                String output1 = outputFormatter1.format(chat.getTimestamp()); //
              setTime(output1);

                Log.d("OBJECT_CHAT_NO_NULL", output1+"::::"+String.valueOf(chat));
            } catch (ParseException e) {
                e.printStackTrace();
            }

          //  setIsSender(currentUser != null && chat.getUid().equals(currentUser.getUid()),false);



        }
        else
        {
            Log.d("OBJECT_CHAT_NULL", String.valueOf(chat));

            Calendar calendar = Calendar.getInstance();
            int day = calendar.get(Calendar.DAY_OF_WEEK);
            int year = calendar.get(Calendar.YEAR);
            String[] monthName = {"January", "February",
                    "March", "April", "May", "June", "July",
                    "August", "September", "October", "November",
                    "December"};

            Calendar cal = Calendar.getInstance();
            String month = monthName[cal.get(Calendar.MONTH)];


            String day_year ="";

            switch (day) {
                case Calendar.SUNDAY:
                    // Current day is Sunday
                              //  2019/05/09 08:19:32
                    day_year = "Sunday" +"/"+ year+"/"+"month";
                    break;
                case Calendar.MONDAY:
                    // Current day is Monday
                    day_year = "Monday" +"/"+ year+"/"+"month";
                    break;
                case Calendar.TUESDAY:
                    day_year = "Tuesday" +"/"+ year+"/"+"month";
                    // etc.
                    break;

                case Calendar.WEDNESDAY:
                    day_year = "Wednesday" +"/"+ year+"/"+"month";
                    // etc.
                    break;

                case Calendar.THURSDAY:
                    day_year = "Thursday" +"/"+ year+"/"+"month";
                    // etc.
                    break;

                case Calendar.FRIDAY:
                    day_year = "Friday"+"/"+ year+"/"+"month";
                    // etc.
                    break;

                case Calendar.SATURDAY:
                    day_year = "Saturday"+"/"+ year+"/"+"month";
                    // etc.
                    break;
            }

            java.util.Date date=new java.util.Date();


            DateFormat outputFormatter1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            String output1 = outputFormatter1.format(date); //
            setTime(output1);


         //   setIsSender(currentUser != null && chat.getUid().equals(currentUser.getUid()),true);
        }

      setIsSender(currentUser != null && chat.getUid().equals(currentUser.getUid()),false);




/*
        try {
            //  Block of code to try

            simpleDateFormat.format(chat.getTimestamp());
        }
        catch(Exception e) {
            //  Block of code to handle errors

            simpleDateFormat.format(chat.getTimestamp());
        }


*/

       if(zz_Config.CHAT_TEST){







       }



    }

    private void setName(@Nullable String name) {
        mNameField.setText(name);
    }
    private void setTime(@Nullable String time) {
        timeview.setText(time);
        timeview.setAlpha(1.0f);
    }

    private void setMessage(@Nullable String text) {
        mTextField.setText(text);
    }

    private void setIsSender(boolean isSender,Boolean go_both) {
          int color = mGreen300;

        if(!go_both){


            if (isSender) {

                mLeftArrow.setVisibility(View.GONE);

                if(!go_both)
                    mRightArrow.setVisibility(View.VISIBLE);
                else
                    mRightArrow.setVisibility(View.GONE);

                mMessageContainer.setGravity(Gravity.END);
            } else {
                color = mGray300;

                if(!go_both)
                    mLeftArrow.setVisibility(View.VISIBLE);
                else
                    mLeftArrow.setVisibility(View.GONE);


                mRightArrow.setVisibility(View.GONE);
                mMessageContainer.setGravity(Gravity.START);
            }





        }



        ((GradientDrawable) mMessage.getBackground()).setColor(color);
        ((RotateDrawable) mLeftArrow.getBackground()).getDrawable()
                .setColorFilter(color, PorterDuff.Mode.SRC);
        ((RotateDrawable) mRightArrow.getBackground()).getDrawable()
                .setColorFilter(color, PorterDuff.Mode.SRC);

    }
}
