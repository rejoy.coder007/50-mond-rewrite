package com.leak.solver.Mond.ae_CloudUsers;

import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.firebase.firestore.FirebaseFirestoreException;
import com.leak.solver.Mond.ad_Auth.ProfileActivity;
import com.leak.solver.Mond.zz_Config;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.leak.solver.Mond.R;

import android.widget.RelativeLayout;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserListActivity extends AppCompatActivity {

    ProgressBar progressBar;
    RecyclerView friendList;



    private FirebaseFirestore db;
    private FirestoreRecyclerAdapter adapter;
    LinearLayoutManager linearLayoutManager;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.user_list_sample);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.users_appBar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("All Users");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        friendList = findViewById(R.id.list);
        linearLayoutManager = new LinearLayoutManager(this);
        friendList.setLayoutManager(linearLayoutManager);
        friendList.setHasFixedSize(true);


         init();
        getFriendList();
    }

    private void init(){
        linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        friendList.setLayoutManager(linearLayoutManager);
        db = FirebaseFirestore.getInstance();
    }



    public static class FriendsHolder extends RecyclerView.ViewHolder {

        public View mView;

        public RelativeLayout root;

        public FriendsHolder(View itemView)
        {
            super(itemView);

            mView = itemView;

        }

        public void setRoot()
        {

            root = (RelativeLayout) mView.findViewById(R.id.root);


        }

        public void setDisplayName(String name)
        {

            TextView userNameView = (TextView) mView.findViewById(R.id.user_single_name);
            userNameView.setText(name);

        }


        public void setUserStatus(String status)
        {

            TextView userStatusView = (TextView) mView.findViewById(R.id.date_and_time);
            userStatusView.setText(status);


        }

        public void setUserImage(String thumb_image, Context ctx)
        {

            CircleImageView c = (CircleImageView) mView.findViewById(R.id.user_single_image);


            Picasso.get().load(thumb_image).placeholder(R.drawable.default_avatar).into(c, new Callback() {

                @Override
                public void onSuccess() {



                    //  Toast.makeText(UsersActivity.this,"image uploaded", Toast.LENGTH_SHORT).show();


                }

                @Override
                public void onError(Exception e) {

                }
            });

        }

    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }


    private void getFriendList(){
        Query query = db.collection("UsersList");



        FirestoreRecyclerOptions<FriendsResponse> response = new FirestoreRecyclerOptions.Builder<FriendsResponse>()
                .setQuery(query, FriendsResponse.class)
                .build();

        adapter = new FirestoreRecyclerAdapter<FriendsResponse, FriendsHolder>(response) {
            @Override
            public void onBindViewHolder(FriendsHolder holder, int position, FriendsResponse model) {


                holder.setDisplayName(model.getName());
                holder.setUserStatus(model.getStatus());
                holder.setUserImage(model.getThumb_image(), UserListActivity.this);



                holder.setRoot();
                final String user_id = getSnapshots().getSnapshot(position).getId();
                final String thumb = model.getThumb_image();

                Log.d("ID_USER_1", user_id+"::"+position);

                holder.root.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        //Toast.makeText(UsersActivity.this, String.valueOf(position), Toast.LENGTH_SHORT).show();
                        SendtoAllUsersProfleActivity(user_id,thumb);
                        Log.d("Clicked id", user_id+"::"+position);

                    }
                });
            }

            @Override
            public FriendsHolder onCreateViewHolder(ViewGroup group, int i) {
                View view = LayoutInflater.from(group.getContext())
                        .inflate(R.layout.users_single_layout, group, false);

                return new FriendsHolder(view);
            }

            @Override
            public void onError(FirebaseFirestoreException e) {
                Log.e("error", e.getMessage());
            }
        };

        adapter.notifyDataSetChanged();
        friendList.setAdapter(adapter);
    }


    public void SendtoAllUsersProfleActivity( String user_id, String thumb ) {

        Intent intent = new Intent(UserListActivity.this, ProfileActivity.class);
        intent.putExtra("user_id", user_id);
        intent.putExtra("thumb", thumb);

        if(zz_Config.CHAT_TEST)
            Log.d("ID_USER_2", "UserId: "+user_id);
        startActivity(intent);
    }




}
