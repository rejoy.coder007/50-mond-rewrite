package com.leak.solver.Mond.ad_Auth;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.leak.solver.Mond.R;

public class StartPageActivity extends AppCompatActivity {


    public FirebaseUser current_user=null;
    private Button mRegBtn;
    private Button mLoginBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ae_start_page);

        mRegBtn = (Button) findViewById(R.id.start_reg_btn);
        mLoginBtn = (Button) findViewById(R.id.start_login_btn);

        mRegBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent reg_intent = new Intent(StartPageActivity.this, Register.class);
                startActivity(reg_intent);

            }
        });

        mLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent login_intent = new Intent(StartPageActivity.this, LoginActivity.class);
                startActivity(login_intent);

            }
        });

    }

    @Override
    protected void onResume() {

        super.onResume();
        if(FirebaseAuth.getInstance().getCurrentUser()!=null)
            onBackPressed();


    }
}
