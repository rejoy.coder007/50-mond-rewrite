package com.leak.solver.Mond.ab_home_page.aa_tabs.ac_folderview.aa_Recycler;

public class Directory_folder {

    public Directory_folder(String video_thumb, String file_name, String size) {
        this.video_thumb = video_thumb;
        this.file_name = file_name;
        this.size = size;
    }



    public String getVideo_thumb() {
        return video_thumb;
    }

    public void setVideo_thumb(String video_thumb) {
        this.video_thumb = video_thumb;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String file_name;
    public String size;
    public String video_thumb;

}
