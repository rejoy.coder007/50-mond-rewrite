package com.leak.solver.Mond.ad_Auth;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.MetadataChanges;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.leak.solver.Mond.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;

public class SettingsActivity extends AppCompatActivity {
    private DatabaseReference mUserDatabase;
    private FirebaseUser mCurrentUser;

    private CircleImageView mDisplayImage;
    private TextView mName;
    private TextView mStatus;
    private Button mStatusBtn;
    private Button mImageBtn;
    private StorageReference mImageStorage;
    OnSuccessListener onSuccessListener=null;
    private ProgressDialog mProgressDialog;

    private static final int GALLERY_PICK = 1;
    private FirebaseAuth mAuth;

    private FirebaseFirestore mFirestore;
    ListenerRegistration UsersDetailsListner;


    UploadTask                           upload_task_main_image=null;
    UploadTask                                 uploadtask_thumb=null;
    ValueEventListener                        valueEventListener=null;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ah_activity_settings);


        mDisplayImage = (CircleImageView) findViewById(R.id.settings_image);
        mName = (TextView) findViewById(R.id.settings_name);
        mStatus = (TextView) findViewById(R.id.settings_status);
        mStatusBtn = (Button) findViewById(R.id.settings_status_btn);
        mImageBtn = (Button) findViewById(R.id.settings_image_btn);



        mAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();


        mStatusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {




                String status_value = mStatus.getText().toString();

                Intent status_intent = new Intent(SettingsActivity.this, statusActvity.class);
                status_intent.putExtra("status_value", status_value);
                //   Log.d("#STOP_DET", "inside status Button ::" +CLOSING);
                startActivity(status_intent);


            }
        });


        mImageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent galleryIntent = new Intent();
                galleryIntent.setType("image/*");
                galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
                // Log.d("#STOP_DET", "inside Image Button ::" +CLOSING);

                startActivityForResult(Intent.createChooser(galleryIntent, "SELECT IMAGE"), GALLERY_PICK);
            }
        });








    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);



        if(requestCode == GALLERY_PICK && resultCode == RESULT_OK)
        {

            Uri imageUri = data.getData();
            CropImage.activity(imageUri)
                    .setAspectRatio(1, 1)
                    .setMinCropWindowSize(500, 500)
                    .start(this);



        }


        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE)
        {




            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK)
            {



                mProgressDialog = new ProgressDialog(SettingsActivity.this);
                mProgressDialog.setTitle("Uploading Main Image...");
                mProgressDialog.setMessage("Please wait while we upload and process the image.");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.show();




                Uri resultUri = result.getUri();
                File thumb_filePath = new File(resultUri.getPath());

                Bitmap thumb_bitmap = null;
                try {
                    thumb_bitmap = new Compressor(this)
                            .setMaxWidth(200)
                            .setMaxHeight(200)
                            .setQuality(75)
                            .compressToBitmap(thumb_filePath);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                thumb_bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                final byte[] thumb_byte = baos.toByteArray();




                String current_user_id = mCurrentUser.getUid();
                StorageReference filepath = mImageStorage.child("profile_images").child(current_user_id + ".jpg");
                StorageReference thumb_filepath = mImageStorage.child("profile_images").child("thumbs").child(current_user_id + ".jpg");


                ///


                upload_task_main_image = filepath.putFile(resultUri);

                Task<Uri> urlTask = upload_task_main_image.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>()
                {
                    @Override
                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception
                    {
                        if (!task.isSuccessful())
                        {
                            //throw task.getException();
                            mProgressDialog.dismiss();
                        }

                        // Continue with the task to get the download URL
                        return filepath.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>()
                {
                    @Override
                    public void onComplete(@NonNull Task<Uri> task) {
                        if (task.isSuccessful())
                        {
                            Uri downloadUri = task.getResult();



                            uploadtask_thumb = thumb_filepath.putBytes(thumb_byte);

                            Task<Uri> urlTask = uploadtask_thumb.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                                @Override
                                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception
                                {
                                    if (!task.isSuccessful())
                                    {
                                        // throw task.getException();


                                        // throw task.getException();

                                    }

                                    // Continue with the task to get the download URL
                                    return thumb_filepath.getDownloadUrl();
                                }
                            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                                @Override
                                public void onComplete(@NonNull Task<Uri> thumb_task) {


                                    Uri downloadUri_thumb = task.getResult();



                                    if(thumb_task.isSuccessful())
                                    {



                                        Map update_hashMap = new HashMap();
                                        update_hashMap.put("image", downloadUri.toString());
                                        update_hashMap.put("thumb_image", downloadUri_thumb.toString());



                                        String current_id = mAuth.getCurrentUser().getUid();




                                        mFirestore.collection("UsersList").document(current_id).update(update_hashMap).addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {

                                           //     onBackPressed();


                                                mProgressDialog.dismiss();


                                            }
                                        });





                                    }

                                }
                            });






                        }
                        else
                        {

                            mProgressDialog.dismiss();



                        }
                    }
                });








            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {

                Exception error = result.getError();

            }
        }


    }




    @Override
    public void onResume() {
        super.onResume();

        mImageStorage = FirebaseStorage.getInstance().getReference();
        mCurrentUser = FirebaseAuth.getInstance().getCurrentUser();
        String current_uid = mCurrentUser.getUid();




        UsersDetailsListner= mFirestore.collection("UsersList").document(current_uid).addSnapshotListener(MetadataChanges.INCLUDE, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                @Nullable FirebaseFirestoreException e) {


                if (e != null) {
                    System.err.println("Listen failed:" + e);
                    return;
                }



                String name = documentSnapshot.getString("name");
                String image = documentSnapshot.getString("image");
                String status = documentSnapshot.getString("status");
                String thumb_image = documentSnapshot.getString("thumb_image");
                mName.setText(name);
                mStatus.setText(status);


                if(!image.equals("default"))
                {
                    Picasso.get().load(image).networkPolicy(NetworkPolicy.OFFLINE).placeholder(R.drawable.default_avatar).into(mDisplayImage, new Callback()
                    {

                        @Override
                        public void onSuccess() {

                            Log.d("#CONFIG_1", "image uploaded");
                            //  Toast.makeText(CloudSettingsActivity.this, "image uploaded", Toast.LENGTH_SHORT).show();


                        }

                        @Override
                        public void onError(Exception e) {

                            Picasso.get().load(image).placeholder(R.drawable.default_avatar).into(mDisplayImage);


                        }
                    });


                }



                // ...
            }
        });
    }

    @Override
    protected void onStop()
    {

        super.onStop();  // Always call the superclass method first


        if(upload_task_main_image != null)
        {
            upload_task_main_image.cancel();

        }


        if(uploadtask_thumb !=null)
        {
            uploadtask_thumb.cancel();
        }


        mStatusBtn.setOnClickListener(null);
        mImageBtn.setOnClickListener(null);
        mCurrentUser = FirebaseAuth.getInstance().getCurrentUser();



        UsersDetailsListner.remove();



        if( mProgressDialog!=null)
            mProgressDialog.dismiss();




    }


    }
