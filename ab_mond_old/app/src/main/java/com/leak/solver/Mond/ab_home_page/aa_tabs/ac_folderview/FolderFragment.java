package com.leak.solver.Mond.ab_home_page.aa_tabs.ac_folderview;


import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.leak.solver.Mond.R;
import com.leak.solver.Mond.ab_home_page.FileManager_Home;
import com.leak.solver.Mond.ab_home_page.aa_activity_home_screen;
import com.leak.solver.Mond.ab_home_page.aa_tabs.ac_folderview.aa_Recycler.Directory_folder;
import com.leak.solver.Mond.ab_home_page.aa_tabs.ac_folderview.aa_Recycler.RecyclerAdapter_folder;
import com.leak.solver.Mond.zz_Config;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class FolderFragment extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";
    private   int mPage;
    com.leak.solver.Mond.ab_home_page.aa_activity_home_screen aa_activity_home_screen =null;

    SwipeRefreshLayout mSwipeRefreshLayout;

    View rootView;
    RecyclerView recyclerView;
    Boolean mStopLoop = false;
    RecyclerAdapter_folder recyclerAdapterFolder;
    Handler handler;
    List<Directory_folder> directories = new ArrayList<>();
    public String dir;

    public FolderFragment() {
        // Required empty public constructor


    }






    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt(ARG_PAGE);
        aa_activity_home_screen = ((aa_activity_home_screen) getActivity());

        if(zz_Config.TEST)
        {
                Log.d("_@@_#FOLD_FRAG_ON_CREAT", "FRAGMENT FOLDER ON_CREATE   : aa_activity_home_screen__ "+mPage);

        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        aa_activity_home_screen.fileManager_home.TAB_PRESS =false;

        View rootView=null;

        if(zz_Config.TEST)
        {
            Log.d("_@@_#FOLD_FRAG_ON_VIEW", " onCreateView fav fragment  : aa_activity_home_screen__ "+mPage+ aa_activity_home_screen);

        }



        rootView = inflater.inflate(R.layout.ad_c_fragment_home_folder, container, false);




        aa_activity_home_screen.fileManager_home_folder.handler_folder = new Handler(getContext().getMainLooper());

        aa_activity_home_screen.fileManager_home_folder.recyclerView_folder = (RecyclerView) rootView.findViewById(R.id.contact_recycleView_folder);
       // aa_activity_home_screen.fileManager_home_folder.recyclerView_folder.setLayoutFrozen(true);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        aa_activity_home_screen.fileManager_home_folder.recyclerView_folder.setLayoutManager(linearLayoutManager);

        aa_activity_home_screen.fileManager_home_folder.recyclerView_folder.setHasFixedSize(true);

       // MyApplication.directories_folder = new ArrayList<>();
        aa_activity_home_screen.fileManager_home_folder.recyclerAdapter_folder=new RecyclerAdapter_folder( aa_activity_home_screen.fileManager_home_folder.directories_folder, getContext());
       aa_activity_home_screen.fileManager_home_folder.recyclerView_folder.setAdapter( aa_activity_home_screen.fileManager_home_folder.recyclerAdapter_folder);




        // SwipeRefreshLayout
        aa_activity_home_screen.fileManager_home_folder.mSwipeRefreshLayout_folder = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container_folder);
        aa_activity_home_screen.fileManager_home_folder.mSwipeRefreshLayout_folder.setColorSchemeResources(R.color.colorAccent,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);




        aa_activity_home_screen.fileManager_home_folder.mSwipeRefreshLayout_folder.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh()
            {
                // Your recyclerview reload logic function will be here!!!

                if (zz_Config.TEST)
                {
                    Log.d("_@@_#FAV_FRAG_REFRESH", "onRefresh fav fragment  : aa_activity_home_screen ");

                }

                aa_activity_home_screen.fileManager_home_folder.mSwipeRefreshLayout_folder.setRefreshing(false);

            }
        });


/*

        for (int i = 0; i <10; i++)
        {


ad_c_fragment_home_folder

            Directory_folder directory = new Directory_folder("hey",  "1","lol");

            // assert ( aa_activity_home_screen.fileManager_home_fav.directories_folder!=null);
            //Directory_folder directory = new Directory_folder( "hey",  "video","soso");
            aa_activity_home_screen.fileManager_home_folder.directories_folder.add(directory);
        }

        // recyclerAdapter.notifyDataSetChanged();
        aa_activity_home_screen.fileManager_home_folder.recyclerAdapter_folder.setRecyclerAdapter( aa_activity_home_screen.fileManager_home_folder.directories_folder,getContext());
        // recyclerAdapter.notifyItemRangeChanged(0, directories.size());
        aa_activity_home_screen.fileManager_home_folder.recyclerAdapter_folder.notifyDataSetChanged();
//        aa_activity_home_screen.fileManager_home_folder.mSwipeRefreshLayout_folder.setRefreshing(false);


        /*
*/

        return rootView;
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {

        if(zz_Config.TEST)
        {
            Log.d("_@@_FOLDER_FRAG_VIS", " VISIBLE folder fragment  : aa_activity_home_screen__ "+mPage+"::"+isVisibleToUser);

        }


        FileManager_Home.FOLDER_TAB_VISIBLE=isVisibleToUser;
        super.setUserVisibleHint(isVisibleToUser);

    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {


        if (zz_Config.TEST)
            Log.d("_@@_FOLD_FRAG_VIEW_CTED", " Fragment View Created aa_activity_home_screen A");
       // aa_activity_home_screen.fileManager_home_fav.READY_TO_UPDATE_FOLDER = true;
        aa_activity_home_screen.fileManager_home_folder.READY_TO_UPDATE_FOLDER = true;

    }


    @Override
    public void onStop() {

        if(zz_Config.TEST)
        {
            Log.d("&_@@_#FAV_FOLD_STP", " VISIBLE folder fragment  : aa_activity_home_screen__ "+mPage+"::");

        }


        if(  aa_activity_home_screen.fileManager_home.TAB_PRESS)
        {

            aa_activity_home_screen.fileManager_home_folder.recyclerAdapter_folder = null;

            aa_activity_home_screen.fileManager_home_folder.recyclerView_folder = null;
            aa_activity_home_screen.fileManager_home_folder.mSwipeRefreshLayout_folder = null;
            aa_activity_home_screen.fileManager_home_folder.handler_folder = null;
            aa_activity_home_screen.fileManager_home_folder.directory_handler_folder = null;
         ;

            Runtime.getRuntime().gc();
        }

        super.onStop();  // Always call the superclass method first

    }

    public void StartThread_dir_update(Boolean read_dir)
    {
        // Toast.makeText(getApplicationContext(), "Your Message", Toast.LENGTH_LONG).show();

/*
        new Thread(new Runnable()
        {
            public void run()
            {

                if(zz_Config.TEST)
                {
                    if(read_dir)
                        Log.d("_#_FAV_THREAD_FULL_READ", "FAV FRAGMENT THREAD   : aa_activity_home_screen __  "+mPage);
                    else
                        Log.d("_#_FAV_THREAD_FULL_READ", "FAV FRAGMENT THREAD   : aa_activity_home_screen __  "+mPage);

                }

                if(read_dir)
                {
                   // ((MyApplication) (getActivity()).getApplication()).getVideo("aa_activity_home_screen");
                }


                MyApplication.directories_folder= new ArrayList<>();
                MyApplication.directories_folder.clear();

                for (int i = 0; i <((MyApplication) (getActivity()).getApplication()).names_folder.size(); i++)
                {

                     List<String> values = new ArrayList<String>();
                     values = ((MyApplication) (getActivity()).getApplication()).map_folder.get(((MyApplication) (getActivity()).getApplication()).names_folder.get(i));



                    Directory_folder directory = new Directory_folder(((MyApplication) (getActivity()).getApplication()).names_folder.get(i), values.get(0) + " Videos",values.get(1));


                     //Directory_folder directory = new Directory_folder( "hey",  "video","soso");
                     MyApplication.directories_folder.add(directory);
                }


                try
                {
                    Thread.sleep(0);

                    ((MyApplication) (getActivity()).getApplication()).handler_folder.post(new Runnable()
                    {
                        @Override
                        public void run()
                        {


                            // recyclerAdapter.notifyDataSetChanged();
                            ((MyApplication) (getActivity()).getApplication()).recyclerAdapter_folder.setRecyclerAdapter( MyApplication.directories_folder,getContext());
                            // recyclerAdapter.notifyItemRangeChanged(0, directories.size());
                            ((MyApplication) (getActivity()).getApplication()).recyclerAdapter_folder.notifyDataSetChanged();
                            ((MyApplication) (getActivity()).getApplication()).mSwipeRefreshLayout_folder.setRefreshing(false);

                              // ((aa_activity_home_screen)getActivity()).vi.setCurrentItem(1);


                            if(zz_Config.TEST)
                            {

                                Log.d("_#_FAV_THREAD_HANDLER", "fav fagment handler for tab refresh from thread   : aa_activity_home_screen __  "+mPage);

                            }


                        }
                    });


                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }



            }
        }).start();

*/

    }




}
