package com.leak.solver.Mond.aa_StartUp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;
import android.view.View;

import com.leak.solver.Mond.ab_home_page.aa_activity_home_screen;
import com.leak.solver.Mond.zz_Config;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Semaphore;

public class ab_FileManager_Start {

    public  static Boolean video_flag_fav;
    public  static Integer count_fav;
    public  static Boolean video_flag_folder;
    public  static Integer count__folder;

    private FirstTimeThread                                                  firstTimeThread = null;
    private SplashScreenThread                                            splashScreenThread = null;
//    ViewGroup                                                                           layout=null;
//    public static MyApplication                                                       _global =null;





    /** Global  */

    public ViewPager viewPager=null;
    public Map<String, List<String>> map =null;
   // public List<String>                                                                 names_fav =null;
    public List<List<String>>                                                              names_fav =null;
    public List<List<String>>                                                          names_folder =null;


 //   public Map<String, List<String>>                                              map_folder = null;

    public Boolean                                                              start_thread =true;
    public Boolean                                                             splash_thread =true;


    public   Intent                                                            intent=null;

    public  Boolean                                                   THREAD_STOPPER =false;



    final int numberOfPermits = 1;
    public Semaphore semaphore = new Semaphore(numberOfPermits, true);

    public  int  TOKEN_SCENE_CHANGE = 0;


    public boolean isFirstTime(Context context)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        boolean ranBefore = preferences.getBoolean("Permission", false);

        if(zz_Config.TEST)
            Log.d("_#FIRST_FLAG", "isFirstTime shared pref : aa_activity_StartUp " + ranBefore);

        return !ranBefore;
    }

    public void initialize_memory()
    {

        if(zz_Config.TEST)
        {

            Log.d("_#_APP_MEM_MAKE", "onCreate: MyApplication make mem A ");

        }



        names_fav = new ArrayList<List<String>>();

        names_folder        =new ArrayList<List<String>>();

        if(zz_Config.TEST){

            Log.d("_#_APP_MEM_MAKE", "onCreate: MyApplication make mem B ");

        }



    }

    public void initialize(aa_activity_StartUp aa_activity_startUp)
    {

        firstTimeThread = new FirstTimeThread();
        splashScreenThread = new SplashScreenThread();
        aa_activity_StartUp.aaStartUp=aa_activity_startUp;
        initialize_memory();




    }


    public void FirstTimeScreen()
    {
        if(zz_Config.TEST)
            Log.d("_#FIRST_TIME_SCREEN", "FirstTimeScreen: aa_activity_StartUp -- A ");

        firstTimeThread.start();

        if(zz_Config.TEST)
            Log.d("_#FIRST_TIME_SCREEN", "FirstTimeScreen: aa_activity_StartUp -- B ");

    }
    public void SplashTimeScreen()
    {
        if(zz_Config.TEST)
            Log.d("_#SPLASH_SCREEN", "SplashTimeScreen: aa_activity_StartUp -- A ");
        splash_thread=true;
        splashScreenThread.start();

        if(zz_Config.TEST)
            Log.d("_#SPLASH_SCREEN", "SplashTimeScreen: aa_activity_StartUp -- B ");
    }


    public void Thread_Kiler(View view){


        int j =0;


        if(zz_Config.TEST)
            Log.d("_#_TT_TEST", " Inside T test"+start_thread);


        if(splashScreenThread!=null){

            if(splashScreenThread.isAlive())
            {
                aa_activity_StartUp.aaStartUp.ab_fileManagerStart.THREAD_STOPPER=true;
            }

            while (splashScreenThread.isAlive())
            {

                if(zz_Config.TEST)
                     Log.d("_#_TT_TEST", " Spalsh Thread Life"+splashScreenThread.isAlive()+"::"+aa_activity_StartUp.aaStartUp.ab_fileManagerStart.THREAD_STOPPER);
            }


        }



        if(firstTimeThread!=null)
        {

            aa_activity_StartUp.aaStartUp.ab_fileManagerStart.THREAD_STOPPER=true;

            while (firstTimeThread.isAlive())
            {

                if(zz_Config.TEST)
                   Log.d("_#_TT_TEST", " videoPlayerThread Thread Life"+firstTimeThread.isAlive()+"::"+aa_activity_StartUp.aaStartUp.ab_fileManagerStart.THREAD_STOPPER);
            }


        }



/*
        while (videoPlayerThread.isAlive())
        {

            if(zz_Config.TEST)
                Log.d("_#_TT_TEST", " first  Alive test"+videoPlayerThread.isAlive());
        }

        while (splashScreenThread.isAlive())
        {

            if(zz_Config.TEST)
                Log.d("_#_TT_TEST", " splash  Alive test"+videoPlayerThread.isAlive());
        }

*/
        if(zz_Config.TEST)
            Log.d("_#_TT_TEST", " Alive test C"+firstTimeThread.isAlive());

        firstTimeThread=null;
        splashScreenThread=null;
        aa_activity_StartUp.aaStartUp.ab_fileManagerStart.THREAD_STOPPER=false;


    }


    public void free_memory()
    {
        if(zz_Config.TEST)
        {

            Log.d("_#_APP_MEM_FREE", "from stop: MyApplication Mem Free A");

        }




        names_fav.clear();
        names_fav =null;

        names_folder.clear();
        names_fav =null;


        if(zz_Config.TEST){

            Log.d("_#_APP_MEM_FREE", "from stop: MyApplication Mem Free B");

        }


      //  aa_activity_StartUp.intent=null;


    }

    void reset_variables_start_UP()
    {
        free_memory();
        aa_activity_StartUp.aaStartUp=null;
        Runtime.getRuntime().gc();
    }


    private static void FirstTimeThread_redirect()
    {






            if(zz_Config.TEST)
                Log.d("_###_REDIR_FIRST_SCREEN", "FirstTimeThread_redirect()B1 : aa_activity_StartUp");

            aa_activity_StartUp.aaStartUp.ab_fileManagerStart.intent = new Intent(aa_activity_StartUp.aaStartUp, aa_activity_home_screen.class);


            aa_activity_StartUp.aaStartUp.startActivity(aa_activity_StartUp.aaStartUp.ab_fileManagerStart.intent);


            if(zz_Config.TEST)
                Log.d("_###_REDIR_FIRST_SCREEN", "FirstTimeThread_redirect()B2 : aa_activity_StartUp");








    }



    private static class FirstTimeThread extends Thread {
        @Override
        public void run()
        {

            if(zz_Config.TEST)
                Log.d("_##_FIRST_TIME", " Inside FirstTimeThread Thread :  aa_activity_StartUp A");



            int j=0;



            long   length =(long) (zz_Config.TIME_DELAY_START_SCREEN/zz_Config.TIME_DELAY_START_SCREEN_STEP);





            try
            {
                for(long i = 0; i<zz_Config.TIME_DELAY_START_SCREEN_STEP && !aa_activity_StartUp.aaStartUp.ab_fileManagerStart.THREAD_STOPPER; i++)
                {
                    Thread.sleep(length);
                    if (zz_Config.TEST)
                        Log.d("_##_FIRST_TIME", " CS :   aa_activity_StartUp  :: " +i);
                }
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }





///////////////////////////////////////////////////////////////////////////////////////////////
            try
            {
                aa_activity_StartUp.aaStartUp.ab_fileManagerStart.semaphore.acquire();
            }
            catch (InterruptedException e)
            {
                 e.printStackTrace();
            }


            if(aa_activity_StartUp.aaStartUp.ab_fileManagerStart.TOKEN_SCENE_CHANGE!=2)
            {
                if (zz_Config.TEST)
                    Log.d("_##_FIRST_TIME", " CS :   aa_activity_StartUp  :: "+ aa_activity_StartUp.aaStartUp.ab_fileManagerStart.TOKEN_SCENE_CHANGE);

                aa_activity_StartUp.aaStartUp.ab_fileManagerStart.TOKEN_SCENE_CHANGE = 1;

                FirstTimeThread_redirect();
            }
            aa_activity_StartUp.aaStartUp.ab_fileManagerStart.semaphore.release();
///////////////////////////////////////////////////////////////////////////////////////////////////



            if(zz_Config.TEST)
                Log.d("_##_FIRST_TIME", " Inside FirstTimeThread Thread :  aa_activity_StartUp B");


        }

    }

    private static void redirectToNewScreen_splash()
    {

        if(zz_Config.TEST)
        {
            Log.d("_#REDIR_SPLASH_SCREEN", "redirectToNewScreen_splash() A : aa_activity_StartUp"+aa_activity_StartUp.aaStartUp);

        }



        aa_activity_StartUp.aaStartUp.ab_fileManagerStart.intent = new Intent(aa_activity_StartUp.aaStartUp, aa_activity_home_screen.class);
        aa_activity_StartUp.aaStartUp.startActivity(aa_activity_StartUp.aaStartUp.ab_fileManagerStart.intent);



        if(zz_Config.TEST)
            Log.d("_#REDIR_SPLASH_SCREEN", "redirectToNewScreen_splash() B : aa_activity_StartUp");


    }

    private static class SplashScreenThread extends Thread
    {
        @Override
        public void run()
        {


           if(zz_Config.TEST)
                Log.d("_###_SPLASH_THREAD_A", " Inside SplashScreenThread :   aa_activity_StartUp A");

            aa_activity_StartUp.aaStartUp.ab_fileManagerStart.getVideo("aa_activity_StartUp");




            if(zz_Config.TEST)
            {


              //  Log.d("_###_SPLASH_THREAD", " Hash Inside SplashScreenThread :   aa_activity_StartUp C"+ Arrays.toString(aa_activity_StartUp.aaStartUp.ab_fileManagerStart.map.entrySet().toArray()));
                Log.d("_###_SPLASH_THREAD", " Hash Inside SplashScreenThread :   aa_activity_StartUp C"+ Arrays.toString(aa_activity_StartUp.aaStartUp.ab_fileManagerStart.names_fav.toArray()));

            }


            try
            {
                aa_activity_StartUp.aaStartUp.ab_fileManagerStart.semaphore.acquire();
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }

                if(aa_activity_StartUp.aaStartUp.ab_fileManagerStart.TOKEN_SCENE_CHANGE!=2)
                {
                    if (zz_Config.TEST)
                        Log.d("_###_SPLASH_THREAD", " CS :   aa_activity_StartUp  :: "+ aa_activity_StartUp.aaStartUp.ab_fileManagerStart.TOKEN_SCENE_CHANGE);

                    aa_activity_StartUp.aaStartUp.ab_fileManagerStart.TOKEN_SCENE_CHANGE = 1;

                    redirectToNewScreen_splash();
                }

            aa_activity_StartUp.aaStartUp.ab_fileManagerStart.semaphore.release();

            if(zz_Config.TEST)
                Log.d("_###SPLASH_THREAD", " Inside SplashScreenThread :   aa_activity_StartUp D");


        }
    }


///////////  Video File reading //////////////////////////////////////////////////////
    public void getfile(File dir) throws InterruptedException
    {



        ab_FileManager_Start.video_flag_fav =false;
        ab_FileManager_Start.count_fav=0;




        File[] directories = dir.listFiles(new FileFilter()
        {



            @Override
            public boolean accept(File pathname)
            {

                if(zz_Config.TEST){

                    Log.d("_#_DIR_SEARCH_1", "onCreate: MyApplication make mem A :: " +pathname);

                }



                if (
                        pathname.getName().endsWith(".3gp")
                                || pathname.getName().endsWith(".avi")
                                || pathname.getName().endsWith(".flv")
                                || pathname.getName().endsWith(".m4v")
                                || pathname.getName().endsWith(".mkv")
                                || pathname.getName().endsWith(".mov")

                                || pathname.getName().endsWith(".mp4")
                                || pathname.getName().endsWith(".mpeg")
                                || pathname.getName().endsWith(".mpg")
                                || pathname.getName().endsWith(".mts")
                                || pathname.getName().endsWith(".vob")



                                || pathname.getName().endsWith(".webm")
                                || pathname.getName().endsWith(".wmv")


                )

                // if (listFile[i].getName().endsWith(".webm"))

                {


                    if(zz_Config.TEST){

                        Log.d("_#_DIR_SEARCH_2", "onCreate: MyApplication make mem B :: " +pathname);

                    }


                    ab_FileManager_Start.video_flag_fav =true;
                    ab_FileManager_Start.count_fav++;

                    //check_vid =true;

                    // names_fav.add(listFile[i].toString());
                    //  mAttachmentList.add(new AttachmentModel(listFile[i].getName()));
                }




                return pathname.isDirectory() ;
            }
        });


        if(ab_FileManager_Start.video_flag_fav)
        {
            String string_path = dir.toString();
            String[] parts = string_path.split("/");

            List<String> values = new ArrayList<String>();
            values.add(parts[parts.length - 1]);
            values.add(dir.toString());

            values.add(count_fav.toString());


          //  names_fav.add(parts[parts.length - 1]);
            names_fav.add(values);
            // names_fav.add("WMV");
          //  map.put(parts[parts.length - 1], values);


        }


        for (int i = 0; !THREAD_STOPPER &&(i < directories.length) ; i++) {

            getfile(directories[i]);

        }





    }


    public void getfolder_Content(File dir) throws InterruptedException
    {


        ab_FileManager_Start.count__folder=0;

         names_folder.clear();



        File[] directories = dir.listFiles(new FileFilter()
        {



            @Override
            public boolean accept(File pathname)
            {

                if(zz_Config.TEST){

                    Log.d("_#_FILE_SEARCH_1", "onCreate: MyApplication make mem A :: " +pathname);

                }

                Boolean flag_video = false;

                if (
                        pathname.getName().endsWith(".3gp")
                                || pathname.getName().endsWith(".avi")
                                || pathname.getName().endsWith(".flv")
                                || pathname.getName().endsWith(".m4v")
                                || pathname.getName().endsWith(".mkv")
                                || pathname.getName().endsWith(".mov")

                                || pathname.getName().endsWith(".mp4")
                                || pathname.getName().endsWith(".mpeg")
                                || pathname.getName().endsWith(".mpg")
                                || pathname.getName().endsWith(".mts")
                                || pathname.getName().endsWith(".vob")



                                || pathname.getName().endsWith(".webm")
                                || pathname.getName().endsWith(".wmv")


                )

                // if (listFile[i].getName().endsWith(".webm"))

                {


                    if(zz_Config.TEST){

                        Log.d("_#_FILE_SEARCH_2", "onCreate: MyApplication make mem B :: " +pathname);

                    }


                    flag_video =true;
                    ab_FileManager_Start.count__folder++;



                    // Get length of file in bytes
                    long fileSizeInBytes = pathname.length();
                    // Convert the bytes to Kilobytes (1 KB = 1024 Bytes)
                    long fileSizeInKB = fileSizeInBytes / 1024;
                    //  Convert the KB to MegaBytes (1 MB = 1024 KBytes)
                    long fileSizeInMB = fileSizeInKB / 1024;




                    String string_path = pathname.toString();
                    String[] parts = string_path.split("/");

                    List<String> values = new ArrayList<String>();
                    values.add(string_path);
                    values.add(parts[parts.length - 1]);
                    values.add(fileSizeInMB+ "MB");



                    //  names_fav.add(parts[parts.length - 1]);
                    names_folder.add(values);


                }




                return flag_video ;
            }
        });











    }

    private void getInbox()
    {
        // holder.text.setTextColor(Color.RED);



        List<String> values_1 = new ArrayList<String>();
        values_1.add("Inbox");
        values_1.add("/Inbox");
        values_1.add("12");

        names_fav.add(values_1);
      //  map.put("Inbox", values_1);

        List<String> values_2 = new ArrayList<String>();
        values_2.add("Private Folder");
        values_2.add("/Private Folder");
        values_2.add("12");

        names_fav.add(values_2);

    //    map.put("Private Folder", values_2);

    }
    public void getVideo(String activity_name)
    {
        names_fav.clear();

        File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath());

        if(zz_Config.TEST)
        {
            Log.d("_#__GET_VID_ROOT_DIR A", "Root Dir: "+dir.toString()+"_::_"+activity_name);
        }


        getInbox();


        try
        {
            getfile(dir );
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        if(zz_Config.TEST)
        {
            Log.d("_#__GET_VID_ROOT_DIR B", "Root Dir: "+dir.toString()+"_::_"+activity_name);
        }


    }
///////////  Video File reading //////////////////////////////////////////////////////















}
