package com.leak.solver.Mond.ab_home_page.aa_tabs.ac_folderview.aa_Recycler;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.leak.solver.Mond.R;


public class ViewHolder_folder extends RecyclerView.ViewHolder {


    public TextView file_name;
    public ImageView video_ref;
    public LinearLayout slice;



    public ViewHolder_folder(@NonNull View itemView)
    {
        super(itemView);

        file_name = (TextView)itemView.findViewById(R.id.file_name_f);
        video_ref = itemView.findViewById(R.id.video_thumb);
        slice  = itemView.findViewById(R.id.folderSlice);
      //  file_name.setText("lolo");
    }
}
